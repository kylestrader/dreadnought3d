cls
@set /p name=What is the name of your project?(My Dreadnought Project):%=% 
@set "install=%CD%"

mkdir "%CD%\projects\%name%\build"
xcopy "%CD%\INSTALL\_template" "%CD%\projects\%name%" /E 
mkdir "%CD%\projects\%name%\build\res"
xcopy "%CD%\res" "%CD%\projects\%name%\build\res" /E 
xcopy "%CD%\INSTALL\lib_3rdParty\_bin" "%CD%\projects\%name%\build" /E

cd /d "%CD%\projects\%name%"

@set /p vsver=Visual Studio version:%=% 

mkdir build
cd build

cmake -G "Visual Studio %vsver%" ../ -DSOLUTION_NAME="%name%" -DDREADNOUGHT_INSTALL="%install%\INSTALL"
devenv %name%.sln