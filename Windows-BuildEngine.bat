devenv "%cd%/build/Dreadnought3D.sln" /build Debug
devenv "%cd%/build/Dreadnought3D.sln" /build Release
devenv "%cd%/build/Dreadnought3D.sln" /build RelWithDebInfo

cls

@set /p ans=Would you like to create a new project now?(y/n):%=% 

If /I "%ans%"=="y" goto yes
else goto no

:yes
call Windows-CreateNewProject.bat

:no
