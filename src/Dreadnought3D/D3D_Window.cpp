#include <3rdParty/Glew/glew.h>
#include <3rdParty/SDL2/SDL.h>
#include "D3D_Window.h"
#include "D3D_Input.h"

D3D_Window::D3D_Window(){}

D3D_Window::~D3D_Window()
{
	m_Window.reset();
}

uint32_t D3D_Window::Initialize(int width, int height, const char * title, uint32_t winFlags)
{
	m_Width = width;
	m_Height = height;
	m_Title = title;

	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	//	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	//	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

	m_Window = std::unique_ptr<SDL_Window, SDL_WindowDestructor> (SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		winFlags
		));

	if (m_Window == nullptr) {
		printf("%s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	m_glContext = SDL_GL_CreateContext(m_Window.get());
	SDL_GL_SetSwapInterval(1);

	glewExperimental = GL_TRUE;

	GLenum res = glewInit();
	if (res != GLEW_OK)
	{
		fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
	}

	return 0;
}

void D3D_Window::WarpMouseToPosition(const D3D_Types::Vector2<int>& pos)
{
	WarpMouseToPosition(pos[0], pos[1]);
}

void D3D_Window::WarpMouseToPosition(uint16_t xPos, uint16_t yPos)
{
	SDL_WarpMouseInWindow(m_Window.get(), xPos, yPos);
}

uint32_t D3D_Window::GetWidth()
{
	return m_Width;
}

uint32_t D3D_Window::GetHeight()
{
	return m_Height;
}

std::string D3D_Window::GetTitle()
{
	return m_Title;
}

uint32_t D3D_Window::SwapBuffers()
{
	SDL_GL_SwapWindow(m_Window.get());
	return 0;
}

void D3D_Window::Update()
{
	for (uint32_t i = 0; i < D3D_Input::NUM_KEYS; i++)
	{
		D3D_Input::SetKeyDown(i, false);
		D3D_Input::SetKeyUp(i, false);
	}

	for (uint32_t i = 0; i < D3D_Input::NUM_MOUSE_BUTTONS; i++)
	{
		D3D_Input::SetMouseDown(i, false);
		D3D_Input::SetMouseUp(i, false);
	}

	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
		{
			D3D_Input::SetIsExitRequested(true);
		}

		if (e.type == SDL_MOUSEMOTION)
		{
			D3D_Input::SetMouseX(e.motion.x);
			D3D_Input::SetMouseY(e.motion.y);
		}

		if (e.type == SDL_KEYDOWN)
		{
			int value = e.key.keysym.scancode;

			D3D_Input::SetKey(value, true);
			D3D_Input::SetKeyDown(value, true);
		}
		if (e.type == SDL_KEYUP)
		{
			int value = e.key.keysym.scancode;

			D3D_Input::SetKey(value, false);
			D3D_Input::SetKeyUp(value, true);
		}
		if (e.type == SDL_MOUSEBUTTONDOWN)
		{
			int value = e.button.button;

			D3D_Input::SetMouse(value, true);
		    D3D_Input::SetMouseDown(value, true);
		}
		if (e.type == SDL_MOUSEBUTTONUP)
		{
			int value = e.button.button;

			D3D_Input::SetMouse(value, false);
			D3D_Input::SetMouseUp(value, true);
		}
	}
}
