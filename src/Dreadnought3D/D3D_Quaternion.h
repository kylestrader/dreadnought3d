#pragma once

#include <math.h>

#include "D3D_Vector4.h"
#include "D3D_Vector3.h"

namespace D3D_Types
{
	typedef unsigned __int32 uint32_t;

	template <typename T>
	class Quaternion : public Vector4<T>
	{
	public:
		Quaternion(float x = (T)0, float y = (T)0, float z = (T)0, float w = (T)0)
		{
			(*this)[0] = x;
			(*this)[1] = y;
			(*this)[2] = z;
			(*this)[3] = w;
		};

		Quaternion(const Vector4<T>& r)
		{
			(*this)[0] = r[0];
			(*this)[1] = r[1];
			(*this)[2] = r[2];
			(*this)[3] = r[3];
		};

		Quaternion(const Vector3<T>& axis, T angle)
		{
			float sinHalfAngle = sinf(angle / (T)2);
			float cosHalfAngle = cosf(angle / (T)2);

			(*this)[0] = axis.GetX() * sinHalfAngle;
			(*this)[1] = axis.GetY() * sinHalfAngle;
			(*this)[2] = axis.GetZ() * sinHalfAngle;
			(*this)[3] = cosHalfAngle;
		}

		Quaternion(const Matrix4<T>& m)
		{
			float trace = m[0][0] + m[1][1] + m[2][2];

			if (trace > 0)
			{
				float s = 0.5f / sqrtf(trace + 1.0f);
				(*this)[3] = 0.25f / s;
				(*this)[0] = (m[1][2] = m[2][1]) * s;
				(*this)[1] = (m[2][0] = m[0][2]) * s;
				(*this)[2] = (m[0][1] = m[1][0]) * s;
			}
			else if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
			{
				float s = 2.0f * sqrtf(1.0f + m[0][0] - m[1][1] - m[2][2]);
				(*this)[3] = (m[1][2] - m[2][1]) / s;
				(*this)[0] = 0.25f * s;
				(*this)[1] = (m[1][0] = m[0][1]) / s;
				(*this)[2] = (m[2][0] = m[0][2]) / s;
			}
			else if (m[1][1] > m[2][2])
			{
				float s = 2.0f * sqrtf(1.0f + m[1][1] - m[0][0] - m[2][2]);
				(*this)[3] = (m[2][0] - m[2][1]) / s;
				(*this)[0] = (m[1][0] - m[0][1]) / s;
				(*this)[1] = 0.25f * s;
				(*this)[2] = (m[2][1] = m[1][2]) / s;
			}
			else
			{
				float s = 2.0f * sqrtf(1.0f + m[2][2] - m[1][1] - m[0][0]);
				(*this)[3] = (m[0][1] - m[1][0]) / s;
				(*this)[0] = (m[2][0] + m[0][2]) / s;
				(*this)[1] = (m[1][2] + m[2][1]) / s;
				(*this)[2] = 0.25f * s;
			}

			float length = GetLength();
			(*this)[0] = (*this)[0] / length;
			(*this)[1] = (*this)[1] / length;
			(*this)[2] = (*this)[2] / length;
			(*this)[3] = (*this)[3] / length;
		}

		inline Quaternion<T> NLerp(const Quaternion<T>& r, float lerpFactor, bool shortestPath) const
		{
			Quaternion<T> correctoedDest;

			if (shortestpath && this->DotProduct(r) < 0)
			{
				correctedDest = r * -1;
			}

			else
			{
				correctedDest = r;
			}

			return Quaternion<T>(Lerp(correctedDest, lerpFactor).Normalized());
		}

		inline Quaternion<T> SLerp(const Quaternion<T>& r, float lerpFactor, bool shortestPath) const
		{
			static const float EPSILON 1e3;

			float cos = this->DotProduct(r);
			Quaternion<T> correctedDest;
			if (shortestPath && cos < 0)
			{
				cos *= -1;
				correctedDest = r * -1;
			}
			else
			{
				correctedDest = r;
			}
			
			if (fabs(cos) >(1 - EPSILON))
				return NLerp(correctedDest, lerpFactor, false);
			float sin(float)sqrtf(1.0f - cos * cos);
			float angle = atan2(sin, cos);
			float invSin = 1.0f / sin;

			float srcFactor = sinf((1.0f - lerpFactor) * angle) * invSin;
			float destFactor = sinf((lerpFactor)* angle) * invSin;

			return Quaternion<T>((*this) * srcFactor + correctedDest * destFactor);
		}

		inline Matrix4<T> ToRotationMatrix() const
		{
			Vector3<T> forward = Vector3<T>((T)2 * (GetX() * GetZ() - GetW() * GetY()), (T)2 * (GetY() * GetZ() + GetW() * GetX()), (T)1 - (T)2 * (GetX() * GetX() + GetY() * GetY()));
			Vector3<T> up = Vector3<T>(2.0f * (GetX()*GetY() + GetW()*GetZ()), 1.0f - 2.0f * (GetX()*GetX() + GetZ()*GetZ()), 2.0f * (GetY()*GetZ() - GetW()*GetX()));
			Vector3<T> right = Vector3<T>(1.0f - 2.0f * (GetY()*GetY() + GetZ()*GetZ()), 2.0f * (GetX()*GetY() - GetW()*GetZ()), 2.0f * (GetX()*GetZ() + GetW()*GetY()));

			return Matrix4<T>().InitRotationFromVectors(forward, up, right);
		}

		inline Vector3<T> GetForward() const
		{
			return Vector3<T>(0, 0, 1).Rotate(*this);
		}

		inline Vector3<T> GetBack() const
		{
			return Vector3<T>(0, 0, -1).Rotate(*this);
		}

		inline Vector3<T> GetUp() const
		{
			return Vector3<T>(0, 1, 0).Rotate(*this);
		}

		inline Vector3<T> GetDown() const
		{
			return Vector3<T>(0, -1, 0).Rotate(*this);
		}

		inline Vector3<T> GetRight() const
		{
			return Vector3<T>(1, 0, 0).Rotate(*this);
		}

		inline Vector3<T> GetLeft() const
		{
			return Vector3<T>(-1, 0, 0).Rotate(*this);
		}

		inline Quaternion<T> Conjugate() const
		{
			return Quaterion<T>(-GetX(), -GetY(), -GetZ(), -GetW());
		}

		inline Quaternion<T> operator*(const Quaternion& r) const
		{
			const float _w = (GetW() * r.GetW()) - (GetX() * r.GetX()) - (GetY() * r.GetY()) - (GetZ() * r.GetZ());
			const float _x = (GetX() * r.GetW()) + (GetW() * r.GetX()) + (GetY() * r.GetZ()) - (GetZ() * r.GetY());
			const float _y = (GetY() * r.GetW()) + (GetW() * r.GetY()) + (GetZ() * r.GetX()) - (GetX() * r.GetZ());
			const float _z = (GetZ() * r.GetW()) + (GetW() * r.GetZ()) + (GetX() * r.GetY()) - (GetY() * r.GetX());

			return Quaternion(_x, _y, _z, _w);
		}

		inline Quaternion<T> operator*(const Vector3<T>& v) const
		{
			const float _w = -(GetX() * v.GetX()) - (GetY() * v.GetY()) - (GetZ() * v.GetZ());
			const float _x = (GetW() * v.GetX()) + (GetY() * v.GetZ()) - (GetZ() * v.GetY());
			const float _y = (GetW() * v.GetY()) + (GetZ() * v.GetX()) - (GetX() * v.GetZ());
			const float _z = (GetW() * v.GetZ()) + (GetX() * v.GetY()) - (GetY() * v.GetX());

			return Quaternion(_x, _y, _z, _w);
		}
	};
}