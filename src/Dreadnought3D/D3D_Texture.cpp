#include "D3D_Texture.h"
#include "D3D_Utils.h"

#include<3rdParty\Staticlibs\stb_image.h>

#define assert_error(expression, error) D3D_Utils::Assertion::AssertError(expression, error)

std::map<std::string, std::shared_ptr<D3D_TextureTypes::TextureData>> D3D_TextureTypes::Texture::s_ResourceMap;

D3D_TextureTypes::Texture::Texture(const std::string & fileName, GLenum textureTarget, GLfloat filter, GLenum internalFormat, GLenum format, bool clamp, GLenum attachment)
{
	m_FileName = fileName;

	std::map<std::string, std::shared_ptr<TextureData>>::const_iterator it = s_ResourceMap.find(fileName);
	if (it != s_ResourceMap.end())
	{
		m_TextureData = it->second;
	}
	else
	{
		int x, y, bytesPerPixel;
		unsigned char* data = stbi_load(("./res/textures/" + fileName).c_str(), &x, &y, &bytesPerPixel, 4);

		assert_error(data == NULL, "Unable to load textures: " + fileName + "\n");

		m_TextureData = std::make_shared<TextureData>(textureTarget, x, y, 1, &data, &filter, &internalFormat, &format, clamp, &attachment);
		stbi_image_free(data);

		s_ResourceMap.insert(std::pair<std::string, std::shared_ptr<TextureData>>(fileName, m_TextureData));
	}
}

D3D_TextureTypes::Texture::Texture(int width, int height, unsigned char * data, GLenum textureTarget, GLfloat filter, GLenum internalFormat, GLenum format, bool clamp, GLenum attachment)
{
	m_FileName = "";
	m_TextureData = std::make_shared<TextureData>(textureTarget, width, height, 1, &data, &filter, &internalFormat, &format, clamp, &attachment);
}

D3D_TextureTypes::Texture::Texture(const Texture & texture) :
	m_TextureData(texture.m_TextureData), 
	m_FileName(texture.m_FileName)
{
}

void D3D_TextureTypes::Texture::operator=(Texture & texture)
{
	char* temp[sizeof(Texture) / sizeof(char)];
	memcpy(temp, this, sizeof(Texture));
	memcpy(this, &texture, sizeof(Texture));
	memcpy(&texture, temp, sizeof(Texture));
}

D3D_TextureTypes::Texture::~Texture()
{
	if (m_TextureData)
	{
		if (m_FileName.length() > 0)
		{
			s_ResourceMap.erase(m_FileName);
		}

		m_TextureData.reset();
	}
}

void D3D_TextureTypes::Texture::Bind(uint32_t unit) const
{
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0 + unit);
	m_TextureData->Bind(0);
}

void D3D_TextureTypes::Texture::BindAsRenderTarget() const
{
	m_TextureData->BindAsRenderTarget();
}

D3D_TextureTypes::TextureData::TextureData(GLenum textureTarget, int width, int height, int numTextures, unsigned char ** data, GLfloat * filters, GLenum * internalformat, GLenum * format, bool clamp, GLenum * attachments)
{
	m_TextureID = new GLuint[numTextures];
	m_TextureTarget = textureTarget;
	m_NumTextures = numTextures;

	m_Width = width;
	m_Height = height;

	m_FrameBuffer = 0;
	m_RenderBuffer = 0;

	InitTextures(data, filters, internalformat, format, clamp);
	InitRenderTargets(attachments);
}

D3D_TextureTypes::TextureData::~TextureData()
{
	if (*m_TextureID) glDeleteTextures(m_NumTextures, m_TextureID);
	if (m_FrameBuffer) glDeleteFramebuffers(1, &m_FrameBuffer);
	if (m_RenderBuffer) glDeleteRenderbuffers(1, &m_RenderBuffer);
	if (m_TextureID) delete[] m_TextureID;
}

void D3D_TextureTypes::TextureData::Bind(int textureNum) const
{
	glBindTexture(m_TextureTarget, m_TextureID[textureNum]);
}

void D3D_TextureTypes::TextureData::BindAsRenderTarget() const
{
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBuffer);
	glViewport(0, 0, m_Width, m_Height);
}

void D3D_TextureTypes::TextureData::InitTextures(unsigned char ** data, GLfloat * filters, GLenum * internalFormat, GLenum * format, bool clamp)
{
	glGenTextures(m_NumTextures, m_TextureID);
	for (uint32_t i = 0; i < m_NumTextures; ++i)
	{
		glBindTexture(m_TextureTarget, m_TextureID[i]);

		glTexParameterf(m_TextureTarget, GL_TEXTURE_MIN_FILTER, filters[i]);
		glTexParameterf(m_TextureTarget, GL_TEXTURE_MAG_FILTER, filters[i]);

		if (clamp)
		{
			glTexParameterf(m_TextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(m_TextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}

		glTexImage2D(m_TextureTarget, 0, internalFormat[i], m_Width, m_Height, 0, format[i], GL_UNSIGNED_BYTE, data[i]);

		if (filters[i] == GL_NEAREST_MIPMAP_NEAREST
			|| filters[i] == GL_NEAREST_MIPMAP_LINEAR
			|| filters[i] == GL_LINEAR_MIPMAP_NEAREST
			|| filters[i] == GL_LINEAR_MIPMAP_LINEAR)
		{
			glGenerateMipmap(m_TextureTarget);
			GLfloat maxAnisotropy;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, & maxAnisotropy);
			glTexParameterf(m_TextureTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, D3D_Utils::Math::Clamp(0.0f, 8.0f, maxAnisotropy));
		}
		else
		{
			glTexParameteri(m_TextureTarget, GL_TEXTURE_BASE_LEVEL, 0);
			glTexParameteri(m_TextureTarget, GL_TEXTURE_MAX_LEVEL, 0);
		}
	}
}

void D3D_TextureTypes::TextureData::InitRenderTargets(GLenum * attachments)
{
	if (attachments == 0)
		return;

	GLenum drawBuffers[32];
	assert_error(m_NumTextures >= 32, "Num textures is greater than 32");

	bool hasDepth = false;
	for (uint32_t i = 0; i < m_NumTextures; ++i)
	{
		if (attachments[i] == GL_DEPTH_ATTACHMENT)
		{
			drawBuffers[i] = GL_NONE;
			hasDepth = true;
		}
		else
			drawBuffers[i] = attachments[i];

		if (attachments[i] == GL_NONE)
			continue;

		if (m_FrameBuffer == 0)
		{
			glGenFramebuffers(1, &m_FrameBuffer);
			glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBuffer);
		}

		glFramebufferTexture2D(GL_FRAMEBUFFER, attachments[i], m_TextureTarget, m_TextureID[i], 0);
	}

	if (m_FrameBuffer == 0)
		return;

	if (!hasDepth)
	{
		glGenRenderbuffers(1, &m_RenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, m_RenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_Width, m_Height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_RenderBuffer);
	}

	glDrawBuffers(m_NumTextures, drawBuffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		assert_error(false, "Framebuffer creation failed!");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
