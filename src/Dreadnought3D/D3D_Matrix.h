#pragma once

#include "D3D_Vector.h"

/// Declaration
namespace D3D_Types
{

	template <typename T, uint32_t D>
	class Matrix
	{
	public:
		typedef unsigned __int32 uint32_t;

		Matrix() {};

		Matrix<T, D> InitIdentity();
		Matrix<T, D> InitScale(const Vector<T, D - 1>& r);
		Matrix<T, D> InitTranslation(const Vector<T, D - 1>& r);
		Matrix<T, D> Transpose() const;
		Matrix<T, D> Inverse() const;
		Matrix<T, D> operator*(const Matrix<T, D>& r) const;
		Vector<T, D> Transform(const Matrix<T, D>& r) const;
		Vector<T, D - 1> Transform(const Matrix<T, D - 1>& r) const;
		void Set(uint32_t x, uint32_t y, T val);
		const T* operator[](int index) const;
		T* operator[](int index);

	private:
		T m_Matrix[D][D];
	};
};

/// Definition
namespace D3D_Types
{
	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::InitIdentity()
	{
		for (uint32_t i = 0; i < D; i++)
		{
			for (uint32_t j = 0; j < D; j++)
			{
				if (i == j)
					m_Matrix[i][j] = T(1);
				else
					m_Matrix[i][j] = T(0);
			}
		}
		return (*this);
	}

	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::InitScale(const Vector<T, D - 1>& r)
	{
		for (uint32_t i = 0; i < D; i++)
		{
			for (uint32_t j = 0; j < D; j++)
			{
				if (i == j && i != D - 1)
					m_Matrix[i][j] = r[i];
				else
					m_Matrix[i][j] = T(0);
			}
		}
		m_Matrix[D - 1][D - 1] = T(1);
		return *this;
	}

	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::InitTranslation(const Vector<T, D - 1>& r)
	{
		for (uint32_t i = 0; i < D; i++)
		{
			for (uint32_t j = 0; j < D; j++)
			{
				if (i == D - 1 && j != D - 1)
					m_Matrix[i][j] = r[j];
				else if (i == j)
					m_Matrix[i][j] = T(1);
				else
					m_Matrix[i][j] = T(0);
			}
		}
		m_Matrix[D - 1][D - 1] = T(1);
		return *this;
	}

	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::Transpose() const
	{
		Matrix<T, D> t;
		for (uint32_t i = 0; i < D; i++)
		{
			for (uint32_t j = 0; j < D; j++)
			{
				t[i][j] = m_Matrix[i][j];
			}
		}
		return t;
	}

	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::Inverse() const
	{
		//TODO: Implement this function!
		return (*this);
	}

	template<typename T, uint32_t D>
	Matrix<T, D> Matrix<T, D>::operator*(const Matrix<T, D>& r) const
	{
		Matrix<T, D> ret;
		for (unsigned int i = 0; i < D; i++)
		{
			for (unsigned int j = 0; j < D; j++)
			{
				ret.m_Matrix[i][j] = T(0);
				for (unsigned int k = 0; k < D; k++)
					ret.m_Matrix[i][j] += m_Matrix[k][j] * r.m_Matrix[i][k];
			}
		}
		return ret;
	}

	template<typename T, uint32_t D>
	Vector<T, D - 1> Matrix<T, D>::Transform(const Matrix<T, D - 1>& r) const
	{
		Vector<T, D> r2;

		for (uint32_t i = 0; i < D - 1; i++)
		{
			r2[i] = r[i];
		}

		r2[D - 1] = T(1);

		Vector<T, D> ret2 = Transform(r2);
		Vector<T, D - 1> ret;

		for (uint32_t i = 0; i < D - 1; i++)
		{
			ret[i] = ret2[i];
		}
		return ret;
	}

	template<typename T, uint32_t D>
	void Matrix<T, D>::Set(uint32_t x, uint32_t y, T val)
	{
		(*this)[x][y] = val;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Matrix<T, D>::Transform(const Matrix<T, D>& r) const
	{
		Vector<T, D> ret;
		for (uint32_t i = 0; i < D; i++)
		{
			ret[i] = 0;
			for (uint32_t j = 0; j < D; j++)
			{
				ret[i] += m_Matrix[j][i] * r[j]
			}
		}
		return ret;
	}

	template<typename T, uint32_t D>
	const T* Matrix<T, D>::operator[](int index) const
	{
		return m_Matrix[index];
	}

	template<typename T, uint32_t D>
	T* Matrix<T, D>::operator[](int index)
	{
		return m_Matrix[index];
	}
}