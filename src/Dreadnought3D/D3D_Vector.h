#pragma once

#include "D3D_Utils.h"

#include <string> 

/// Declaration
namespace D3D_Types
{
	typedef unsigned __int32 uint32_t;

	template <typename T, uint32_t D> 
	class Vector
	{
	public:
		Vector() {};

		// Utils
		std::string ToString();

		T GetLengthSq() const;
		T GetLength() const;
		T DotProduct(const Vector<T, D> & r) const;


		Vector<T, D> Normalize();
		Vector<T, D> GetAsNormalizeD();
		Vector<T, D> Lerp(const Vector<T, D>& r, T lerpFactor) const;
		Vector<T, D> Max(const Vector <T, D>& r) const;
		Vector<T, D> Reflect(const Vector<T, D>& normal) const;

		// Operations
		Vector<T, D>& operator-(const Vector<T, D>& right) const;
		Vector<T, D>& operator-=(const Vector<T, D>& right);
		Vector<T, D> operator+(const Vector<T, D>& right) const;
		Vector<T, D>& operator+=(const Vector<T, D>& right);
		Vector<T, D> operator*(const T& right) const;
		Vector<T, D>& operator*=(const T& right);
		Vector<T, D> operator/(const T& right) const;
		Vector<T, D>& operator/=(const T& right);

		bool operator ==(const Vector<T, D>& right) const;
		bool operator !=(const Vector<T, D>& right) const;

		// Accessors
		T& operator [] (unsigned int i);
		T operator [] (unsigned int i) const;

	protected:
	private:

		// Core data
		T m_Values[D];
	};
}

/// Definition
namespace D3D_Types
{
	template<typename T, uint32_t D>
	std::string Vector<T, D>::ToString()
	{
		std::string ret = "(";
		for (uint32_t i = 0; i < D - 1; i++)
			ret += std::to_string(m_Values[i]) + ", ";
		ret += std::to_string(m_Values[D - 1]);
		ret += ")";
		return ret;
	}

	template<typename T, uint32_t D>
	T Vector<T, D>::GetLengthSq() const
	{
		return this->DotProduct(*this);
	}

	template<typename T, uint32_t D>
	T Vector<T, D>::GetLength() const
	{
		return sqrt(GetLengthSq());
	}

	template<typename T, uint32_t D>
	T Vector<T, D>::DotProduct(const Vector<T, D>& r) const
	{
		float dot = 0.f;
		for (uint32_t i = 0; i < D; i++)
		{
			dot += (*this)[i] * r[i];
		}
		return dot;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::Normalize()
	{
		*this /= GetLength();
		return *this;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::GetAsNormalizeD()
	{
		Vector<T, D> vec = *this;
		vec /= GetLength();
		return vec;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::Lerp(const Vector<T, D>& r, T lerpFactor) const
	{
		return (r - *this) * lerpFactor + *this;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::Max(const Vector<T, D>& r) const
	{
		Vector<T, D> result;
		for (uint32_t i = 0; i < D; i++)
		{
			result[i] = m_Values[i] > r[i] ? m_Values[i] : r[i];
		}
		return result;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::Reflect(const Vector<T, D>& normal) const
	{
		return *this - (normal * (this->DotProduct(normal) * 2))
	}

	template<typename T, uint32_t D>
	Vector<T, D>& Vector<T, D>::operator-(const Vector<T, D>& right) const
	{
		Vector<T, D> result;
		for (uint32_t i = 0; i < D; i++)
			result[i] = m_Values[i] - right[i];

		return result;
	}

	template<typename T, uint32_t D>
	Vector<T, D>& Vector<T, D>::operator-=(const Vector<T, D>& right)
	{
		for (uint32_t i = 0; i < D; i++)
			(*this)[i] = m_Values[i] - right[i];

		return *this;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::operator+(const Vector<T, D>& right) const
	{
		Vector<T, D> result;
		for (uint32_t i = 0; i < D; i++)
			result[i] = m_Values[i] + right[i];

		return result;
	}

	template<typename T, uint32_t D>
	Vector<T, D>& Vector<T, D>::operator+=(const Vector<T, D>& right)
	{
		for (uint32_t i = 0; i < D; i++)
			(*this)[i] = m_Values[i] + right[i];

		return *this;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::operator*(const T& right) const
	{
		Vector<T, D> result;
		for (uint32_t i = 0; i < D; i++)
			result[i] = m_Values[i] * right;

		return result;
	}

	template<typename T, uint32_t D>
	Vector<T, D>& Vector<T, D>::operator*=(const T& right)
	{
		for (uint32_t i = 0; i < D; i++)
			(*this)[i] = m_Values[i] * right;

		return *this;
	}

	template<typename T, uint32_t D>
	Vector<T, D> Vector<T, D>::operator/(const T& right) const
	{
		Vector<T, D> result;
		for (uint32_t i = 0; i < D; i++)
			result[i] = m_Values[i] / right;

		return result;
	}

	template<typename T, uint32_t D>
	Vector<T, D>& Vector<T, D>::operator/=(const T& right)
	{
		for (uint32_t i = 0; i < D; i++)
			(*this)[i] = m_Values[i] / right;

		return *this;
	}

	template<typename T, uint32_t D>
	bool Vector<T, D>::operator ==(const Vector<T, D>& right) const
	{
		for (uint32_t i = 0; i < D; i++)
			if (*(this)[i] != right[i])
				return false;
		return true;
	}

	template<typename T, uint32_t D>
	bool Vector<T, D>::operator !=(const Vector<T, D>& right) const
	{

		return !operator==(right);
	}

	template<typename T, uint32_t D>
	T& Vector<T, D>::operator [](unsigned int i)
	{
		return m_Values[i];
	}

	template<typename T, uint32_t D>
	T Vector<T, D>::operator [](unsigned int i) const
	{
		return m_Values[i];
	}
}