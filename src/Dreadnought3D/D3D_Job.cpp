#include "D3D_Job.h"

#include <cassert>

std::shared_ptr<D3D_JobBatch> D3D_Job::GetMyJobBatch()
{
	return m_JobBatch;
}

void D3D_Job::SetMyJobBatch(std::shared_ptr<D3D_JobBatch> jobbatch)
{
	m_JobBatch = jobbatch;
}

void D3D_Job::ResetMyJobBatch()
{
	m_JobBatch.reset();
}