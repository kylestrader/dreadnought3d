#include "D3D_Camera.h"

typedef D3D_Types::Vector3<float> Vector3f;

Vector3f D3D_Camera::s_YAxis = Vector3f(0, 1, 0);

D3D_Camera::D3D_Camera(Vector3f pos, Vector3f forward, Vector3f up) : 
	m_Pos(pos),
	m_Forward(forward),
	m_Up(up)
{
	m_Forward.Normalize();
	m_Up.Normalize();
}

D3D_Camera::D3D_Camera() :
	m_Pos(Vector3f(0, 0, 0)),
	m_Forward(Vector3f(0, 0, 1)),
	m_Up(Vector3f(0, 1, 0))
{
}

D3D_Camera::~D3D_Camera()
{
}

Vector3f D3D_Camera::GetPos()
{
	return m_Pos;
}

Vector3f D3D_Camera::GetForward()
{
	return m_Forward;
}

Vector3f D3D_Camera::GetUp()
{
	return m_Up;
}

void D3D_Camera::SetPos(Vector3f pos)
{
	m_Pos = pos;
}

void D3D_Camera::SetForward(Vector3f forward)
{
	m_Forward = forward;
}

void D3D_Camera::SetUp(Vector3f up)
{
	m_Up = up;
}

void D3D_Camera::Move(Vector3f dir, float amt)
{
	m_Pos += dir * amt;
}

Vector3f D3D_Camera::GetLeft()
{
	Vector3f left = m_Forward.CrossProduct(m_Up);
	left.Normalize();
	return left;
}

Vector3f D3D_Camera::GetRight()
{
	Vector3f right = m_Up.CrossProduct(m_Forward);
	right.Normalize();
	return right;
}

void D3D_Camera::RotateX(float angle)
{
	Vector3f Haxis = s_YAxis.CrossProduct(m_Forward);
	Haxis.Normalize();

	m_Forward = m_Forward.Rotate(angle, Haxis);
	m_Forward.Normalize();

	m_Up = m_Forward.CrossProduct(Haxis);
	m_Up.Normalize();
}

void D3D_Camera::RotateY(float angle)
{
	Vector3f Haxis = s_YAxis.CrossProduct(m_Forward);
	Haxis.Normalize();

	m_Forward = m_Forward.Rotate(angle, s_YAxis);
	m_Forward.Normalize();

	m_Up = m_Forward.CrossProduct(Haxis);
	m_Up.Normalize();
}
