#pragma once

#include "D3D_Vector.h"

/// Declaration
namespace D3D_Types
{
	class Vector3f_AVX: public Vector<float, 4>
	{
	public:
		Vector3f_AVX(const Vector<float, 3>& r);
		Vector3f_AVX(const Vector<float, 4>& r);
		Vector3f_AVX(float x = 0.f, float y = 0.f, float z = 0.f);
		~Vector3f_AVX();;

		uint32_t GetSize();

		float GetX() const;
		float GetY() const;
		float GetZ() const;
		void SetX(float x);
		void SetY(float y);
		void SetZ(float z);

		Vector3f_AVX Rotate(float angle, const Vector3f_AVX& axis);
		Vector3f_AVX CrossProduct(const Vector3f_AVX& r);
		float DotProduct(const Vector3f_AVX& r);
		Vector3f_AVX operator*(const float& right) const;
	};
};

/// Definition
namespace D3D_Types
{
	Vector3f_AVX::Vector3f_AVX(const Vector<float, 3>& r)
	{
		(*this)[0] = r[0];
		(*this)[1] = r[1];
		(*this)[2] = r[2];
		(*this)[3] = 0;
	}

	Vector3f_AVX::Vector3f_AVX(const Vector<float, 4>& r)
	{
		(*this)[0] = r[0];
		(*this)[1] = r[1];
		(*this)[2] = r[2];
		(*this)[3] = 0;
	}

	Vector3f_AVX::Vector3f_AVX(float x, float y, float z)
	{
		(*this)[0] = x;
		(*this)[1] = y;
		(*this)[2] = z;
		(*this)[3] = 0;
	}

	Vector3f_AVX::~Vector3f_AVX() {}

	uint32_t Vector3f_AVX::GetSize()
	{
		return 3;
	}

	float Vector3f_AVX::GetX() const
	{
		return (*this)[0];
	}

	float Vector3f_AVX::GetY() const
	{
		return (*this)[1];
	}

	float Vector3f_AVX::GetZ() const
	{
		return (*this)[2];
	}

	void Vector3f_AVX::SetX(float x)
	{
		(*this)[0] = x;
	}

	void Vector3f_AVX::SetY(float y)
	{
		(*this)[1] = y;
	}

	void Vector3f_AVX::SetZ(float z)
	{
		(*this)[2] = z;
	}

	Vector3f_AVX Vector3f_AVX::Rotate(float angle, const Vector3f_AVX & axis)
	{
		if (false)
		{
			float two_right_angles = 180.0f, cos_compliment_helper = 1.f, radians, pi, sin_angle, cos_angle;
			Vector3f_AVX a, b, c;
			__asm
			{
				// Get radians from degrees
				FLDPI							// load pi from fpu
				FSTP pi							// store pi in local variable
				MOV EAX, angle					// move angle into the eax register
				MOV EBX, pi						// move pi into the ebx register
				MUL EBX							// multiply eax register by ebx register
				MOV EBX, two_right_angles		// move 180.f degrees into ebx
				DIV EBX							// divide eax register by ebx
				MOV radians, EAX				// move eax register into radians variable

				// Get sin of angle
				FLD radians						// load radians variable
				FSIN							// run sin operation on radians variable
				FSTP sin_angle					// push result into sin_angle

												// Get cos of angle
				FLD radians						// load radians variable
				FCOS							// run cos operation on radians variable
				FSTP cos_angle					// push result int cos_angle

				// Begin Cross Product of this and the axis * sin_angle
				MOV EAX, this					// Load pointers into CPU regs
				MOV EBX, axis

				MOVUPS	XMM0, [EAX]				// We will save a clean copy of this vector for later
				MOVUPS	XMM1, [EBX]

				LEA		EBX, sin_angle			// move sin_angle variablke into eax register
				VBROADCASTSS	XMM2, [EBX]		// broadcast ebx into xmm2 register

				MULPS	XMM1, XMM2				// multiply xmm1 and xmm2 registers

				MOVAPS	XMM2, XMM0				// Make a copy of vector A
				MOVAPS	XMM3, XMM1				// Make a copy of vector B

				SHUFPS	XMM0, XMM0, 0xD8		// 11 01 10 00 Flip the middle elements of A
				SHUFPS	XMM1, XMM1, 0xE1		// 11 10 00 01 Flip first two elements of B
				MULPS	XMM0, XMM1				// Multiply the modified register vectors

				SHUFPS	XMM2, XMM2, 0xE1		// 11 10 00 01 Flip first two elements of the A copy
				SHUFPS	XMM3, XMM3, 0xD8		// 11 01 10 00  Flip the middle elements of the B copy
				MULPS	XMM2, XMM3				// Multiply the modified register vectors

				SUBPS	XMM0, XMM2				// Subtract the two resulting register vectors
				SHUFPS	XMM0, XMM0, 0xC6		// 10 01 00 11 Flip the first and third elements
				MOVUPS[a], XMM0				// Save the return vector (A)

				// begin multiply this vector by the cos-angle
				LEA		ECX, cos_angle			// move sin_angle variablke into eax register
				VBROADCASTSS	XMM0, [ECX]		// broadcast ebx into xmm2 register

				MOVUPS	XMM1, [EAX]

				MULPS	XMM0, XMM1				// multiply xmm4 and xmm0 registers
				MOVUPS[b], XMM0				// Save the return vector (B)

				// axis * this dot-product(axis * (1 - cosAngle))
				MOVSS	XMM0, cos_angle
				MOVSS	XMM1, cos_compliment_helper
				SUBSS	XMM1, XMM0
			}
			_ReadWriteBarrier();
		}
		else
		{
			const float sinAngle = sin(-angle);
			const float cosAngle = cos(-angle);
			Vector3f_AVX a = this->CrossProduct(axis * sinAngle);
			Vector3f_AVX b = *this * cosAngle;
			Vector3f_AVX c = axis * this->DotProduct(axis * (1.f - cosAngle));
			return a + b + c;
		} // _SSE2	
	}
	Vector3f_AVX Vector3f_AVX::CrossProduct(const Vector3f_AVX & r)
	{
		if (false)
		{
			Vector3f_AVX result;
			__asm
			{
				MOV EAX, this					// Load pointers into CPU regs
				MOV EBX, r

				MOVUPS XMM0, [EAX]              // Move unaligned vectors to SSE regs
				MOVUPS XMM1, [EBX]
				MOVAPS XMM2, XMM0               // Make a copy of vector A
				MOVAPS XMM3, XMM1               // Make a copy of vector B

				SHUFPS XMM0, XMM0, 0xD8			// 11 01 10 00  Flip the middle elements of A
				SHUFPS XMM1, XMM1, 0xE1			// 11 10 00 01 Flip first two elements of B
				MULPS  XMM0, XMM1               // Multiply the modified register vectors

				SHUFPS XMM2, XMM2, 0xE1			// 11 10 00 01 Flip first two elements of the A copy
				SHUFPS XMM3, XMM3, 0xD8			// 11 01 10 00  Flip the middle elements of the B copy
				MULPS XMM2, XMM3                // Multiply the modified register vectors

				SUBPS  XMM0, XMM2			    // Subtract the two resulting register vectors
				SHUFPS XMM0, XMM0, 0xC6			// 10 01 00 11
				MOVUPS[result], XMM0			// Save the return vector
			}
			_ReadWriteBarrier();
			return result;
		}
		else
		{
			float x = 0.f;
			float y = 0.f;
			float z = 0.f;
			x = (*this)[1] * r[2] - (*this)[2] * r[1];
			y = (*this)[2] * r[0] - (*this)[0] * r[2];
			z = (*this)[0] * r[1] - (*this)[1] * r[0];
			return Vector3f_AVX(x, y, z);
		} // _WIN32
	}
	float Vector3f_AVX::DotProduct(const Vector3f_AVX & r)
	{
		bool b = _AVX;
		if (false)
		{
			Vector3f_AVX carrier;
			_asm
			{
				MOV EAX, this
				MOV EBX, r

				MOVUPS	XMM0, [EAX]
				MOVUPS	XMM1, [EBX]
				MULPS	XMM0, XMM1
				MOVAPS	XMM2, XMM0
				SHUFPS	XMM0, XMM0, 0x4E
				ADDPS	XMM0, XMM2
				SHUFPS	XMM2, XMM2, 0x11
				ADDPS	XMM0, XMM2
				MOVUPS[carrier], XMM0			// Save the return vector
			}
			_ReadWriteBarrier();
			return carrier[0];
		}
		else
		{
			return (GetX() * r.GetX()
				+ GetY() * r.GetY()
				+ GetZ() * r.GetZ());
		}
	}

	Vector3f_AVX Vector3f_AVX::operator*(const float& right) const
	{
		Vector3f_AVX result;
		if (false)
		{
			__asm
			{
				MOV EAX, this				// Load pointer into CPU reg

				MOVUPS XMM0, [EAX]			// Move the vector to an SSE reg   
				MOV EBX, right
				VBROADCASTSS XMM1, [EBX]
				MULPS XMM0, XMM1			// Multiply vectors
				MOVUPS[result], XMM0		// Save the return vector
			}
			_ReadWriteBarrier();
		}
		else
		{
			result[0] = (*this)[0] * right;
			result[1] = (*this)[1] * right;
			result[2] = (*this)[2] * right;
		}// _WIN32
		return result;
	};
}