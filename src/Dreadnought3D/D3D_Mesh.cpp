#include "D3D_Mesh.h"

#include <cassert>

#include <3rdParty\Assimp\Importer.hpp>
#include <3rdParty\Assimp\scene.h>
#include <3rdParty\Assimp\postprocess.h>

std::map<std::string, std::shared_ptr<D3D_MeshTypes::MeshData>> D3D_MeshTypes::Mesh::s_ResourceMap;

namespace D3D_MeshTypes
{
#pragma region IndexedModel
	IndexedModel::IndexedModel()
	{
	}

	IndexedModel::IndexedModel(const std::vector<uint32_t>& indices,
		const std::vector<D3D_Types::Vector3<float>>& positions,
		const std::vector<D3D_Types::Vector2<float>>& texCoords,
		const std::vector<D3D_Types::Vector3<float>>& normals,
		const std::vector<D3D_Types::Vector3<float>>& tangents) :
		m_Indices(indices),
		m_Positions(positions),
		m_TexCoords(texCoords),
		m_Normals(normals),
		m_Tangents(tangents)
	{
	}

	bool IndexedModel::IsValid() const
	{
		return m_Positions.size() == m_TexCoords.size()
			&& m_TexCoords.size() == m_Normals.size()
			&& m_Normals.size() == m_Tangents.size();
	}

	void IndexedModel::CalcNormals()
	{
	}

	void IndexedModel::CalcTangents()
	{
	}

	IndexedModel IndexedModel::Finalize()
	{
		return IndexedModel();
	}

	void IndexedModel::AddVertex(const D3D_Types::Vector3<float>& vert)
	{
		m_Positions.push_back(vert);
	}

	void IndexedModel::AddVertex(float x, float y, float z)
	{
		AddVertex(D3D_Types::Vector3<float>(x, y, z));
	}

	void IndexedModel::AddTexCoord(const D3D_Types::Vector2<float>& coord)
	{
		m_TexCoords.push_back(coord);
	}

	void IndexedModel::AddTexCoord(float x, float y)
	{
		AddTexCoord(D3D_Types::Vector2<float>(x, y));
	}

	void IndexedModel::AddNormal(const D3D_Types::Vector3<float>& normal)
	{
		m_Normals.push_back(normal);
	}

	void IndexedModel::AddNormal(float x, float y, float z)
	{
		AddNormal(D3D_Types::Vector3<float>(x, y, z));
	}

	void IndexedModel::AddTangent(const D3D_Types::Vector3<float>& tangent)
	{
		m_Tangents.push_back(tangent);
	}

	void IndexedModel::AddTangent(float x, float y, float z)
	{
		AddTangent(D3D_Types::Vector3<float>(x, y, z));
	}

	void IndexedModel::AddFace(uint32_t vertIndex0, uint32_t vertIndex1, uint32_t vertIndex2)
	{
	}
	
	const std::vector<uint32_t>& IndexedModel::GetIndices() const
	{
		return m_Indices;
	}

	const std::vector<D3D_Types::Vector3<float>>& IndexedModel::GetPositions() const
	{
		return m_Positions;
	}

	const std::vector<D3D_Types::Vector2<float>>& IndexedModel::GetTexCoords() const
	{
		return m_TexCoords;
	}

	const std::vector<D3D_Types::Vector3<float>>& IndexedModel::GetNormals() const
	{
		return m_Normals;
	}

	const std::vector<D3D_Types::Vector3<float>>& IndexedModel::GetTangents() const
	{
		return m_Tangents;
	}
#pragma endregion

#pragma region Mesh
	Mesh::Mesh(const std::string & fileName)
	{
		std::map<std::string, std::shared_ptr<MeshData>>::const_iterator it = s_ResourceMap.find(fileName);
		if (it != s_ResourceMap.end())
		{
			m_meshData = it->second;
		}
		else
		{
			Assimp::Importer importer;

			const aiScene* scene = importer.ReadFile(("./res/models/" + fileName).c_str(),
				aiProcess_Triangulate |
				aiProcess_GenSmoothNormals |
				aiProcess_FlipUVs |
				aiProcess_CalcTangentSpace);

			if (!scene)
			{
				printf("Mesh load failed!: %s\n", fileName.c_str());
				assert(0 == 0);
			}

			const aiMesh* model = scene->mMeshes[0];

			std::vector<Vector3f> positions;
			std::vector<Vector2f> texCoords;
			std::vector<Vector3f> normals;
			std::vector<Vector3f> tangents;
			std::vector<unsigned int> indices;

			const aiVector3D aiZeroVector(0.0f, 0.0f, 0.0f);
			for (unsigned int i = 0; i < model->mNumVertices; i++)
			{
				const aiVector3D pos = model->mVertices[i];
				const aiVector3D normal = model->mNormals[i];
				const aiVector3D texCoord = model->HasTextureCoords(0) ? model->mTextureCoords[0][i] : aiZeroVector;
				const aiVector3D tangent = model->mTangents[i];

				positions.push_back(Vector3f(pos.x, pos.y, pos.z));
				texCoords.push_back(Vector2f(texCoord.x, texCoord.y));
				normals.push_back(Vector3f(normal.x, normal.y, normal.z));
				tangents.push_back(Vector3f(tangent.x, tangent.y, tangent.z));
			}

			for (unsigned int i = 0; i < model->mNumFaces; i++)
			{
				const aiFace& face = model->mFaces[i];
				assert(face.mNumIndices == 3);
				indices.push_back(face.mIndices[0]);
				indices.push_back(face.mIndices[1]);
				indices.push_back(face.mIndices[2]);
			}

			m_meshData = std::make_shared<MeshData>(IndexedModel(indices, positions, texCoords, normals, tangents));
			s_ResourceMap.insert(std::pair<std::string, std::shared_ptr<MeshData>>(fileName, m_meshData));
		}
	}

	Mesh::Mesh(const std::string & meshName, const IndexedModel & model)
	{

	}

	Mesh::Mesh(const Mesh & mesh)
	{
	}

	Mesh::~Mesh()
	{
	}

	void Mesh::Draw()
	{
		m_meshData->Draw();
	}

	void Mesh::operator=(Mesh & mesh)
	{
	}
#pragma endregion

#pragma region MeshData
	MeshData::MeshData(const IndexedModel & model) :
		m_DrawCount(model.GetIndices().size())
	{
		glGenVertexArrays(1, &m_VertexArrayObject);
		glBindVertexArray(m_VertexArrayObject);

		glGenBuffers(NUM_BUFFERS, m_VertexArrayBuffers);

		//Position vertex array buffer
		if (model.GetPositions().size() > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffers[POSITION_VB]);
			glBufferData(GL_ARRAY_BUFFER, model.GetPositions().size() * sizeof(model.GetPositions()[0]), &model.GetPositions()[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		//TexCoord vertex array buffer
		if (model.GetTexCoords().size() > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffers[TEXCOORD_VB]);
			glBufferData(GL_ARRAY_BUFFER, model.GetTexCoords().size() * sizeof(model.GetTexCoords()[0]), &model.GetTexCoords()[0], GL_STATIC_DRAW);
		
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
		}
		
		//Normal vertex array buffer
		if (model.GetNormals().size() > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffers[NORMAL_VB]);
			glBufferData(GL_ARRAY_BUFFER, model.GetNormals().size() * sizeof(model.GetNormals()[0]), &model.GetNormals()[0], GL_STATIC_DRAW);
		
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}
		
		//Tangent vertex array buffer
		if (model.GetTangents().size() > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_VertexArrayBuffers[TANGENT_VB]);
			glBufferData(GL_ARRAY_BUFFER, model.GetTangents().size() * sizeof(model.GetTangents()[0]), &model.GetTangents()[0], GL_STATIC_DRAW);
		
			glEnableVertexAttribArray(3);
			glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		//Index vertex array buffer
		if (model.GetIndices().size() > 0)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_VertexArrayBuffers[INDEX_VB]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, model.GetIndices().size() * sizeof(model.GetIndices()[0]), &model.GetIndices()[0], GL_STATIC_DRAW);
		}
	}

	MeshData::~MeshData()
	{
		glDeleteBuffers(NUM_BUFFERS, m_VertexArrayBuffers);
		glDeleteVertexArrays(1, &m_VertexArrayObject);
	}

	void MeshData::Draw() const
	{
		glBindVertexArray(m_VertexArrayObject);
		glDrawElements(GL_TRIANGLES, m_DrawCount, GL_UNSIGNED_INT, 0);
	}

	void MeshData::operator=(MeshData & other)
	{
	}
#pragma endregion
}
