#pragma once

#include <string>

/// ShaderType
namespace D3D_Types
{
	struct ShaderType
	{
	public:

		// Common GLSL shader program types.
		enum class Type
		{
			VERTEX_SHADER,
			FRAGMENT_SHADER,
			COMPUTE_SHADER,
			GEOMETRY_SHADER
		};

		// Static helper instances.
		static const ShaderType Vertex;
		static const ShaderType Fragment;
		static const ShaderType Compute;
		static const ShaderType Geometry;

		// The instance's associated type.
		Type type;

		std::string extension();

	private:

		// Static file ext names.
		static const std::string VS;
		static const std::string FS;
		static const std::string CS;
		static const std::string GS;

		// Designated initializer.
		ShaderType(ShaderType::Type type);
	};
}