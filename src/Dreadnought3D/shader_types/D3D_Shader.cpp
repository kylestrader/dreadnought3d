#include "D3D_Shader.h"
#include "D3D_ShaderType.h"
#include "Dreadnought3D\D3D_Utils.h"

#include <string>
#include <sstream>
#include <map>
#include <vector>

#include<3rdParty\Glew\glew.h>

#define assert_error(expression, error) D3D_Utils::Assertion::AssertError(expression, error)

namespace D3D_Types
{
	std::map<std::string, std::shared_ptr<ShaderData>> Shader::s_ResourceMap;
	int ShaderData::s_SupportedOpenGLLevel = 0;
	std::string ShaderData::s_GlslVersion = "";

	ShaderData::ShaderData(std::string fileName)
	{
		m_Program = glCreateProgram();
		assert_error(!m_Program, "There was an error creating the shader program\n");
		ImportShaderSource(fileName);
	}

	ShaderData::~ShaderData()
	{
		glDeleteProgram(m_Program);
	}

	void ShaderData::AddShader(ShaderType shaderType, const std::string& source)
	{
		switch (shaderType.type)
		{
		case ShaderType::Type::FRAGMENT_SHADER:
			AddProgram(source, GL_FRAGMENT_SHADER);
			break;
		case ShaderType::Type::VERTEX_SHADER:
			AddProgram(source, GL_VERTEX_SHADER);
			break;
		case ShaderType::Type::COMPUTE_SHADER:
			AddProgram(source, GL_COMPUTE_SHADER);
			break;
		case ShaderType::Type::GEOMETRY_SHADER:
			AddProgram(source, GL_GEOMETRY_SHADER);
			break;
		}
	}

	std::string ShaderData::ComposeShaderHeader(std::string shaderSource, const std::string& glslVersion)
	{
		std::string comp = "#version " + s_GlslVersion + "\n";
		comp += "#define GLSL_VERSION " + s_GlslVersion + "\n";
		comp += shaderSource;

		return comp;
	}

	void ShaderData::CompileShader()
	{
		glLinkProgram(m_Program);
		CheckShaderError(m_Program, GL_LINK_STATUS, true, "Error linking shader program");

		glValidateProgram(m_Program);
		CheckShaderError(m_Program, GL_LINK_STATUS, true, "Invalid shader program");
	}

	int ShaderData::GetProgram() const
	{
		return m_Program;
	}

	void ShaderData::ImportShaderSource(const std::string& file)
	{
		if (s_SupportedOpenGLLevel == 0)
		{
			int majorVersion = 0;
			int minorVersion = 0;

			glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
			glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

			s_SupportedOpenGLLevel = majorVersion * 100 + minorVersion * 10;

			if (s_SupportedOpenGLLevel >= 330)
			{
				std::ostringstream convert;
				convert << s_SupportedOpenGLLevel;

				s_GlslVersion = convert.str();
			}
			else if (s_SupportedOpenGLLevel >= 320)
			{
				s_GlslVersion = "150";
			}
			else if (s_SupportedOpenGLLevel >= 310)
			{
				s_GlslVersion = "140";
			}
			else if (s_SupportedOpenGLLevel >= 300)
			{
				s_GlslVersion = "130";
			}
			else if (s_SupportedOpenGLLevel >= 210)
			{
				s_GlslVersion = "120";
			}
			else if (s_SupportedOpenGLLevel >= 200)
			{
				s_GlslVersion = "110";
			}
			else
			{
				assert_error(true, "Error: OpenGL Version " + std::to_string(majorVersion) + "." + std::to_string(minorVersion) + " does not support shaders.\n");
			}

			// TODO: Find way to check what shader types are available.
			std::vector<ShaderType> fileExtensions = { ShaderType::Vertex, ShaderType::Fragment };

			std::string shaderSource = "";
			for (uint8_t i = 0; i < fileExtensions.size(); ++i)
			{
				// Throw the shader source into a string that we can use to parse for uniforms later.
				std::string src = D3D_Utils::ResourceLoading::LoadShader(file + fileExtensions[i].extension());

				shaderSource += src;

				// Create header content for shader source file.
				src = ComposeShaderHeader(src, s_GlslVersion);
				// Link the shader source to the current program.
				AddShader(fileExtensions[i], src);
			}

			CompileShader();

			// Parse shader source for uniforms.
			AddShaderUniforms(shaderSource);
		}
	}

	std::map<std::string, unsigned int> ShaderData::GetUniformMap() const
	{
		return m_UniformMap;
	}

	void ShaderData::AddProgram(const std::string& text, int type)
	{
		int shader = glCreateShader(type);

		assert_error(!shader, "There was an error creating the shader type\n");

		const GLchar* newStr[1];
		newStr[0] = text.c_str();
		GLint lengths[1];
		lengths[0] = text.length();

		glShaderSource(shader, 1, newStr, lengths);
		glCompileShader(shader);

		CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error compiling shader type");

		glAttachShader(m_Program, shader);
	}

	void ShaderData::CheckShaderError(int shader, int flag, bool isProgram, const std::string & errorMessage)
	{
		GLint success = 0;
		GLchar error[1024] = { 0 };

		if (isProgram)
			glGetProgramiv(shader, flag, &success);
		else
			glGetShaderiv(shader, flag, &success);

		if (!success)
		{
			if (isProgram)
				glGetProgramInfoLog(shader, sizeof(error), NULL, error);
			else
				glGetShaderInfoLog(shader, sizeof(error), NULL, error);

			assert_error(!success, errorMessage + ": " + error + "\n");
		}
	}

	void ShaderData::AddAllAttributes(const std::string& vertexShaderText, const std::string& attributeKeyword)
	{
	}

	void ShaderData::AddShaderUniforms(const std::string& shaderText)
	{
		AddShaderUniform("transform");
		AddShaderUniform("color");
	}

	void ShaderData::AddShaderUniform(const std::string& uniform)
	{
		int uniformLocation = glGetUniformLocation(m_Program, uniform.c_str());

		assert_error(uniformLocation == INVALID_VALUE, "Error: could not find uniform: " + uniform);

		m_UniformMap.insert(std::pair<std::string, uint32_t>(uniform, uniformLocation));
	}

	Shader::Shader(const std::string& fileName)
	{
		m_FileName = fileName;

		std::map<std::string, std::shared_ptr<ShaderData>>::const_iterator it = s_ResourceMap.find(fileName);
		if (it != s_ResourceMap.end())
		{
			m_ShaderData = it->second;
		}
		else
		{
			m_ShaderData = std::make_shared<D3D_Types::ShaderData>(fileName);
			s_ResourceMap.insert(std::pair<std::string, std::shared_ptr<D3D_Types::ShaderData>>(fileName, m_ShaderData));
		}
	}

	Shader::Shader(const Shader & other) :
		m_FileName(other.m_FileName),
		m_ShaderData(other.m_ShaderData)
	{
	}

	Shader::~Shader()
	{
		if (m_ShaderData)
		{
			if (m_FileName.length() > 0)
				s_ResourceMap.erase(m_FileName);

			m_ShaderData.reset();
		}
	}

	void Shader::Bind() const
	{
		glUseProgram(m_ShaderData->GetProgram());
	}

	void Shader::SetUniform(const std::string & uniformName, int value) const
	{
		glUniform1i(m_ShaderData->GetUniformMap().at(uniformName), value);
	}

	void Shader::SetUniform(const std::string & uniformName, float value) const
	{
		glUniform1f(m_ShaderData->GetUniformMap().at(uniformName), value);
	}

	void Shader::SetUniform(const std::string & uniformName, const Matrix4f & value) const
	{
		glUniformMatrix4fv(m_ShaderData->GetUniformMap().at(uniformName), 1, GL_FALSE, &(value[0][0]));
	}

	void Shader::SetUniform(const std::string & uniformName, const Vector3f & value) const
	{
		glUniform3f(m_ShaderData->GetUniformMap().at(uniformName), value.GetX(), value.GetY(), value.GetZ());
	}

	void Shader::SetUniform(const std::string & uniformName, const Vector4f & value) const
	{
		glUniform4f(m_ShaderData->GetUniformMap().at(uniformName), value.GetX(), value.GetY(), value.GetZ(), value.GetW());
	}

	void Shader::AddShaderUniform(std::string uniform)
	{
		m_ShaderData->AddShaderUniform(uniform);
	}
}
