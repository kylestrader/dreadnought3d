#pragma once 

#include <string>
#include <memory>
#include <map>

#include "Dreadnought3D\D3D_Matrix4.h"
#include "Dreadnought3D\D3D_Vector3.h"
#include "Dreadnought3D\D3D_Vector4.h"

#include "D3D_ShaderType.h"

namespace D3D_Types
{
	class ShaderData
	{
	public:

		ShaderData(std::string fileName);
		~ShaderData();

		void CompileShader();
		int GetProgram() const;
		void AddShaderUniform(const std::string& uniform);
		void AddShaderUniforms(const std::string& shaderText);
		void ImportShaderSource(const std::string& file);

		std::map<std::string, unsigned int> GetUniformMap() const;
	
	private:

		void AddProgram(const std::string &text, int type);
		void AddShader(ShaderType shaderType, const std::string& text);
		std::string ComposeShaderHeader(std::string shaderSource, const std::string& glslVersion);
		void CheckShaderError(int shader, int flag, bool isProgram, const std::string& errorMessage);
		void AddAllAttributes(const std::string& vertexShaderText, const std::string& attributeKeyword);

		int m_Program;
		static std::string s_GlslVersion;
		static int s_SupportedOpenGLLevel;

		std::map<std::string, unsigned int> m_UniformMap;
	};

	class Shader
	{
	public:
		typedef Matrix4<float> Matrix4f;
		typedef Vector3<float> Vector3f;
		typedef Vector4<float> Vector4f;

		Shader(const std::string& fileName = "basicShader");
		Shader(const Shader& other);
		~Shader();

		void Bind() const;

		void SetUniform(const std::string& uniformName, int value) const;
		void SetUniform(const std::string& uniformName, float value) const;
		void SetUniform(const std::string& uniformName, const Matrix4f& value) const;
		void SetUniform(const std::string& uniformName, const Vector3f& value) const;
		void SetUniform(const std::string& uniformName, const Vector4f& value) const;
	private:
		static std::map<std::string, std::shared_ptr<ShaderData>> s_ResourceMap;

		std::shared_ptr<ShaderData> m_ShaderData;
		std::string m_FileName;

		void operator=(const Shader& other) {}

		void AddShaderUniform(std::string uniform);
	};
}