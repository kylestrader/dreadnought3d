#include "D3D_ShaderType.h"

namespace D3D_Types
{
	const std::string ShaderType::VS = ".vert";
	const std::string ShaderType::FS = ".frag";
	const std::string ShaderType::CS = ".comp";
	const std::string ShaderType::GS = ".geom";

	const ShaderType ShaderType::Vertex = ShaderType(ShaderType::Type::VERTEX_SHADER);
	const ShaderType ShaderType::Fragment = ShaderType(ShaderType::Type::FRAGMENT_SHADER);
	const ShaderType ShaderType::Compute = ShaderType(ShaderType::Type::COMPUTE_SHADER);
	const ShaderType ShaderType::Geometry = ShaderType(ShaderType::Type::GEOMETRY_SHADER);

	// Designated initializer.
	ShaderType::ShaderType(ShaderType::Type type)
	{
		this->type = type;
	}

	// Get the file extension for this type of shader file.
	std::string ShaderType::extension()
	{
		switch (this->type)
		{
		case ShaderType::Type::VERTEX_SHADER:
			return ShaderType::VS;
		case ShaderType::Type::FRAGMENT_SHADER:
			return ShaderType::FS;
		case ShaderType::Type::COMPUTE_SHADER:
			return ShaderType::CS;
		case ShaderType::Type::GEOMETRY_SHADER:
			return ShaderType::GS;
		default:
			return ShaderType::VS;
		}
	}
}