#pragma once

#include "D3D_Input.h"

bool D3D_Input::m_Keys[] = { false };
bool D3D_Input::m_DownKeys[] = { false };
bool D3D_Input::m_UpKeys[] = { false };
bool D3D_Input::m_MouseInputs[] = { false };
bool D3D_Input::m_DownMouse[] = { false };
bool D3D_Input::m_UpMouse[] = { false };

bool D3D_Input::m_IsExitRequested = false;

int D3D_Input::m_MouseX = 0;
int D3D_Input::m_MouseY = 0;
