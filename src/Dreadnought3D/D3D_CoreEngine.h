#pragma once

#include <memory>

#include <chrono>
#include <thread>

#include "D3D_Job.h"
#include "D3D_JobBatch.h"
#include "D3D_Utils.h"

class D3D_GameApp;
class D3D_RenderingEngine;

class TestJob2 : public D3D_Job
{
	void Execute() override
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
		printf("FINSHED 2\n");
	}
};

class TestJob1 : public D3D_Job
{
	void Execute() override
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(2500));
		printf("FINSHED 1\n");
	}
};

class TestJob3 : public D3D_Job
{
	void Execute() override
	{
		for (int i = 0; i < 100; ++i)
		{
			printf("entry: %d\n", i);
		}
		printf("FINSHED 3\n");
	}
};

class D3D_CoreEngine
{
public:
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;

	//Constructor/Destructor
	D3D_CoreEngine();
	~D3D_CoreEngine();

	void Initialize(const std::shared_ptr<D3D_GameApp>& game, const std::shared_ptr<D3D_RenderingEngine>& renderer);
	void Start();
	void Quit();

protected:

private:
	void Update();
	void Render();
	void Cleanup();
	void Run();

	//Private vars
	const double DEFAULT_FRAME_CAP = 60;

	std::shared_ptr<D3D_GameApp> m_GameApp;
	std::shared_ptr<D3D_RenderingEngine> m_Renderer;
	bool m_IsRunning;
};