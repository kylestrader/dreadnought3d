#include <cassert>

#include "D3D_JobBatch.h"

D3D_JobBatch::D3D_JobBatch()
:m_JobsCompleted(false)
{
}

D3D_JobBatch::~D3D_JobBatch()
{
}

void D3D_JobBatch::AddBatchJob(std::shared_ptr<D3D_Job> job)
{
	m_Jobs.push_back(job);
}

void D3D_JobBatch::CompletedJob(std::shared_ptr<D3D_Job> job)
{
	for (uint32_t i = 0; i < m_Jobs.size(); ++i)
	{
		if (m_Jobs[i] == job)
		{
			m_Jobs.erase(m_Jobs.begin() + i);
			return;
		}
	}

	bool couldNotFind = false;
	assert(couldNotFind);
}

void D3D_JobBatch::SetCompletedAllJobs()
{
	m_JobsCompleted = true;
}

uint32_t D3D_JobBatch::GetNumJobs()
{
	return m_Jobs.size();
}

std::shared_ptr<D3D_Job> D3D_JobBatch::GetBatchJob(uint32_t index)
{
	return m_Jobs[index];
}

void D3D_JobBatch::WaitForBatch()
{
	while (!m_JobsCompleted) {}
}