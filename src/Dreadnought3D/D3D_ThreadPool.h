#pragma once

#include <thread>
#include <mutex>
#include <deque>
#include <atomic>
#include <vector>
#include <memory>
#include <condition_variable>

class D3D_Job;
class D3D_JobBatch;

class D3D_ThreadPool
{
public:
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;

	class WorkerThread
	{
	public:
		WorkerThread(D3D_ThreadPool& threadPool);
		void operator()();
	private:
		D3D_ThreadPool& m_Pool;
	};

	D3D_ThreadPool(uint32_t numThreads);
	~D3D_ThreadPool();

	void WaitForAll();

	void AddSingleJob(std::shared_ptr<D3D_Job> job);
	void AddJobBatch(std::shared_ptr<D3D_JobBatch> jobBatch);

	std::vector<std::thread> workers;
	std::deque<std::shared_ptr<D3D_Job>> jobs;

	std::mutex queue_mutex;
	std::mutex vector_mutex;
	std::condition_variable condition;
	std::atomic<bool> stop;
};