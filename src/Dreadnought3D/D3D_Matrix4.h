#pragma once
#include "D3D_Matrix.h"
#include "D3D_Vector3.h"
#include "D3D_Utils.h"

namespace D3D_Types
{

	template <typename T>
	class Matrix4 : public Matrix<T, 4>
	{
	public:
		typedef unsigned __int32 uint32_t;

		Matrix4() {};

		template<uint32_t D>
		Matrix4(const Matrix<T, D>& r)
		{
			if (D < 4)
			{
				this->InitIdentity();

				for (uint32_t i = 0; i < D; i++)
				{
					for (uint32_t j = 0; j < D; j++)
					{
						(*this)[i][j] = r[i][j];
					}
				}
			}

			else
			{
				for (uint32_t i = 0; i < 4; i++) // Magic numbers are toxic... excpet 4 right here
				{
					for (uint32_t j = 0; j < 4; j++) // and right here...
					{
						(*this)[i][j] = r[i][j];
					}
				}
			}
		}

		inline Matrix4<T> InitRotationEuler(const Vector3<T>& rot)
		{
			return InitRotationEuler(rot.GetX(), rot.GetY(), rot.GetZ());
		}

		inline Matrix4<T> InitRotationEuler(T rotateX, T rotateY, T rotateZ)
		{
			Matrix4<T> rx, ry, rz;

			const T x = D3D_Utils::Math::Deg2Rad(rotateX);
			const T y = D3D_Utils::Math::Deg2Rad(rotateY);
			const T z = D3D_Utils::Math::Deg2Rad(rotateZ);

			rx[0][0] = T(1);	rx[1][0] = T(0);		rx[2][0] = T(0);		rx[3][0] = T(0);
			rx[0][1] = T(0);	rx[1][1] = cos(x);		rx[2][1] = -sin(x);		rx[3][1] = T(0);
			rx[0][2] = T(0);	rx[1][2] = sin(x);		rx[2][2] = cos(x);		rx[3][2] = T(0);
			rx[0][3] = T(0);	rx[1][3] = T(0);		rx[2][3] = T(0);		rx[3][3] = T(1);

			ry[0][0] = cos(y);	ry[1][0] = T(0);		ry[2][0] = -sin(y);		ry[3][0] = T(0);
			ry[0][1] = T(0);	ry[1][1] = T(1);		ry[2][1] = T(0);		ry[3][1] = T(0);
			ry[0][2] = sin(y);	ry[1][2] = T(0);		ry[2][2] = cos(y);		ry[3][2] = T(0);
			ry[0][3] = T(0);	ry[1][3] = T(0);		ry[2][3] = T(0);		ry[3][3] = T(1);

			rz[0][0] = cos(z);	rz[1][0] = -sin(z);		rz[2][0] = T(0);		rz[3][0] = T(0);
			rz[0][1] = sin(z);	rz[1][1] = cos(z);		rz[2][1] = T(0);		rz[3][1] = T(0);
			rz[0][2] = T(0);	rz[1][2] = T(0);		rz[2][2] = T(1);		rz[3][2] = T(0);
			rz[0][3] = T(0);	rz[1][3] = T(0);		rz[2][3] = T(0);		rz[3][3] = T(1);

			*this = rz * ry * rx;

			return *this;
		}

		inline Matrix4<T> InitRotationFromVectors(const Vector3<T>& n, const Vector3<T>& v, const Vector3<T>& u)
		{
			(*this)[0][0] = u.GetX();	(*this)[1][0] = u.GetY();	(*this)[2][0] = u.GetZ();	(*this)[3][0] = T(0);
			(*this)[0][1] = v.GetX();	(*this)[1][1] = v.GetY();	(*this)[2][1] = v.GetZ();	(*this)[3][1] = T(0);
			(*this)[0][2] = n.GetX();	(*this)[1][2] = n.GetY();	(*this)[2][2] = n.GetZ();	(*this)[3][2] = T(0);
			(*this)[0][3] = T(0);		(*this)[1][3] = T(0);		(*this)[2][3] = T(0);		(*this)[3][3] = T(1);

			return *this;
		}

		inline Matrix4<T> InitRotationFromDirection(const Vector3<T>& forward, const Vector3<T>& up) 
		{
			Vector3<T> _f = forward;
			Vector3<T> _u = up;

			Vector3<T> n = _f.Normalize();
			Vector3<T> u = Vector3<T>(_u.Normalize()).CrossProduct(n);
			Vector3<T> v = n.CrossProduct(u);

			return InitRotationFromVectors(n, v, u);
		}

		inline Matrix4<T> InitPerspective(T fov, T aspectRatio, T zNear, T zFar)
		{
			const T zRange = zNear - zFar;
			const T tanHalfFOV = tanf(fov / T(2));

			(*this)[0][0] = T(1) / (tanHalfFOV * aspectRatio); (*this)[1][0] = T(0);   (*this)[2][0] = T(0);            (*this)[3][0] = T(0);
			(*this)[0][1] = T(0);                   (*this)[1][1] = T(1) / tanHalfFOV; (*this)[2][1] = T(0);            (*this)[3][1] = T(0);
			(*this)[0][2] = T(0);                   (*this)[1][2] = T(0);            (*this)[2][2] = (-zNear - zFar) / zRange; (*this)[3][2] = T(2)*zFar*zNear / zRange;
			(*this)[0][3] = T(0);                   (*this)[1][3] = T(0);            (*this)[2][3] = T(1);            (*this)[3][3] = T(0);

			return *this;
		}

		inline Matrix4<T> InitOrthographic(T left, T right, T bottom, T top, T near, T far)
		{
			const T width = (right - left);
			const T height = (top - bottom);
			const T depth = (far - near);

			(*this)[0][0] = T(2) / width; (*this)[1][0] = T(0);        (*this)[2][0] = T(0);        (*this)[3][0] = -(right + left) / width;
			(*this)[0][1] = T(0);       (*this)[1][1] = T(2) / height; (*this)[2][1] = T(0);        (*this)[3][1] = -(top + bottom) / height;
			(*this)[0][2] = T(0);       (*this)[1][2] = T(0);        (*this)[2][2] = T(-2) / depth; (*this)[3][2] = -(far + near) / depth;
			(*this)[0][3] = T(0);       (*this)[1][3] = T(0);        (*this)[2][3] = T(0);			(*this)[3][3] = T(1);

			return (*this);
		}
	};
};