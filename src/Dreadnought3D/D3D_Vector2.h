#pragma once

#include "D3D_Vector.h"

/// Declaration
namespace D3D_Types
{
	template <typename T> 
	class Vector2 : public Vector<T, 2>
	{
	public:
		Vector2();
		Vector2(const Vector<T, 2>& r);
		Vector2(T x, T y);

		~Vector2();

		uint32_t GetSize();

		T GetX();
		T GetY();
		void SetX(T x);
		void SetY(T y);
		Vector2<T> Rotate(T angle);
	};
}

/// Definition
namespace D3D_Types
{
	template<typename T>
	D3D_Types::Vector2<T>::Vector2() {}

	template<typename T>
	D3D_Types::Vector2<T>::Vector2(const Vector<T, 2>& r)
	{
		(*this)[0] = r[0];
		(*this)[1] = r[1];
	}

	template<typename T>
	D3D_Types::Vector2<T>::Vector2(T x, T y)
	{
		(*this)[0] = x;
		(*this)[1] = y;
	}

	template<typename T>
	D3D_Types::Vector2<T>::~Vector2() {}

	template<typename T>
	uint32_t D3D_Types::Vector2<T>::GetSize()
	{
		return 2;
	}

	template<typename T>
	T D3D_Types::Vector2<T>::GetX()
	{
		return (*this)[0];
	}

	template<typename T>
	T D3D_Types::Vector2<T>::GetY()
	{
		return (*this)[1];
	}

	template<typename T>
	void D3D_Types::Vector2<T>::SetX(T x)
	{
		(*this)[0] = x;
	}

	template<typename T>
	void D3D_Types::Vector2<T>::SetY(T y)
	{
		(*this)[1] = y;
	}

	template<typename T>
	D3D_Types::Vector2<T> D3D_Types::Vector2<T>::Rotate(T angle)
	{
		T rad = D3D_Utils::Math::Deg2Rad<T>(angle);
		T cos = cos(rad);
		T sin = sin(rad);
		return Vector2<T>((T)((*this)[0] * cos - (*this)[1] * sin),
			(T)((*this)[0] * sin + (*this)[1] * cos));
	}
}