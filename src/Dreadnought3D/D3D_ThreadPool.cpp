#include "D3D_ThreadPool.h"
#include "D3D_JobBatch.h"
#include "D3D_Job.h"

D3D_ThreadPool::WorkerThread::WorkerThread(D3D_ThreadPool& threadPool)
	: m_Pool(threadPool)
{
}

void D3D_ThreadPool::WorkerThread::operator()()
{
	std::shared_ptr<D3D_Job> job = nullptr;

	while (true)
	{
		{
			std::unique_lock<std::mutex> lock(m_Pool.queue_mutex);

			while (!m_Pool.stop && m_Pool.jobs.empty())
			{
				m_Pool.condition.wait(lock);
			}

			if (m_Pool.stop && m_Pool.jobs.empty())
			{
				return;
			}

			job = m_Pool.jobs.front();
			m_Pool.jobs.pop_front();
		}

		job->Execute();

		if (job->GetMyJobBatch() != nullptr)
		{
			std::unique_lock<std::mutex> lock(m_Pool.vector_mutex);

   			std::shared_ptr<D3D_JobBatch> jobBatch = job->GetMyJobBatch();
			jobBatch->CompletedJob(job);

			if (jobBatch->GetNumJobs() == 0)
			{
				job->ResetMyJobBatch();
				jobBatch->SetCompletedAllJobs();
			}
		}
	}
}

D3D_ThreadPool::D3D_ThreadPool(uint32_t numThreads)
	: stop(false)
{
	for (uint32_t i = 0; i < numThreads; ++i)
	{
		workers.push_back(std::thread(WorkerThread(*this)));
	}
}

D3D_ThreadPool::~D3D_ThreadPool()
{
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		stop = true;
	}

	condition.notify_all();

	for (size_t i = 0; i < workers.size(); ++i)
	{
		workers[i].join();
	}
}

void D3D_ThreadPool::WaitForAll()
{
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		stop = true;
	}

	condition.notify_all();

	for (size_t i = 0; i < workers.size(); ++i)
	{
		workers[i].join();
	}
}

void D3D_ThreadPool::AddSingleJob(std::shared_ptr<D3D_Job> job)
{
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		jobs.push_back(job);
		printf("%d\n", jobs.size());
	}

	condition.notify_one();
}

void D3D_ThreadPool::AddJobBatch(std::shared_ptr<D3D_JobBatch> jobBatch)
{
	if (jobBatch->GetNumJobs() == 0)
	{
		jobBatch->SetCompletedAllJobs();
	}

	{
		std::unique_lock<std::mutex> batchLock(vector_mutex);
		uint32_t jobCount = jobBatch->GetNumJobs();

		for (uint32_t i = 0; i < jobCount; i++)
		{
			std::shared_ptr<D3D_Job> job = jobBatch->GetBatchJob(i);
			job->SetMyJobBatch(jobBatch);
			AddSingleJob(job);
		}
	}
}
