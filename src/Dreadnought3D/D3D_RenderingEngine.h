#pragma once 

#include "D3D_Vector4.h"
#include "D3D_Vector2.h"
#include "D3D_Window.h"

class D3D_RenderingEngine
{
public:
	friend class D3D_CoreEngine;

	D3D_RenderingEngine();
	~D3D_RenderingEngine();
	void CreateWindow(int width, int height, const char* title, uint32_t winFlags);
	void ClearScreen();
	void InitGraphics(const D3D_Types::Vector4<float>& clearColor);
	inline void SetClearColor(const D3D_Types::Vector4<float>& clearColor) { m_BufferClearColor = clearColor; }
	void WarpMouseToPosition(const D3D_Types::Vector2<int> pos);
	void Cleanup();

private:
	void Update();
	void SwapBuffers();
	void DestroyWindow();

	D3D_Types::Vector4<float> m_BufferClearColor;
	std::unique_ptr<D3D_Window> m_Window;
};