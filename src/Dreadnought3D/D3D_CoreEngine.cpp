#include <string>

#include <3rdParty/SDL2/SDL.h>
#include <memory>

#include "D3D_CoreEngine.h"
#include "D3D_Time.h"
#include "D3D_Utils.h"
#include "D3D_GameApp.h"
#include "D3D_Input.h"
#include "D3D_ThreadPool.h"
#include "D3D_ThreadPoolManager.h"
#include "D3D_Types.h"
#include "D3D_Vector4.h"
#include "D3D_Vector3.h"
#include "D3D_RenderingEngine.h"

#define MAX_ACTIVE_THREADS 8

D3D_CoreEngine::D3D_CoreEngine()
{
	m_IsRunning = false;
}

D3D_CoreEngine::~D3D_CoreEngine()
{
	Cleanup();
}

void D3D_CoreEngine::Initialize(const std::shared_ptr<D3D_GameApp>& game, const std::shared_ptr<D3D_RenderingEngine>& renderer)
{
	D3D_Utils::Cpu::GetCpu_AVX_Support();
	D3D_ThreadPoolManager::Initialize(MAX_ACTIVE_THREADS);

	m_GameApp = game;
	m_Renderer = renderer;
}

void D3D_CoreEngine::Start()
{
	if (m_IsRunning)
		return;
	Run();
}

void D3D_CoreEngine::Run()
{
	m_IsRunning = true;

	double frameCounter = 0;
	int frames = 0;
	double frameTime = 1.0 / DEFAULT_FRAME_CAP;

	D3D_Time::Timer* timer = new D3D_Time::Timer();
	timer->Start();

	double lastTime = timer->GetTotalTime_ns();
	double unprocessedTime = 0; //how much time still needs to be processed

	while (m_IsRunning)
	{
		bool shouldRender = false;

		double startTime = timer->GetTotalTime_ns();

		double passedTime = startTime - lastTime;
		lastTime = startTime;

		unprocessedTime += passedTime / (double)D3D_Time::Helper::NS_PER_SEC;

		frameCounter += passedTime;
		while (unprocessedTime > frameTime)
		{
			D3D_Time::Helper::DELTA_T = frameTime;
			shouldRender = true;
			unprocessedTime -= frameTime;

			if (D3D_Input::IsExitRequested())
				Quit();

			Update();

			if (frameCounter >= D3D_Time::Helper::NS_PER_SEC)
			{
				printf("Frames per sec: %d\n", frames);
				frames = 0;
				frameCounter = 0;
			}
		}

		if (shouldRender)
		{
			frames++;
			Render();
		}
		else
		{
			D3D_Time::SDL_Time::Delay(1);
		}
	}
}

void D3D_CoreEngine::Update()
{
	m_Renderer->Update();

	D3D_Types::Vector2<int> mousePos(D3D_Input::GetMousePos());

	m_GameApp->Update();
}

void D3D_CoreEngine::Render()
{
	// Begin Render
	m_Renderer->ClearScreen();

	m_GameApp->Render();

	// End Render
	m_Renderer->SwapBuffers();
}

void D3D_CoreEngine::Cleanup()
{
	m_GameApp->Cleanup();
	m_Renderer->Cleanup();
}

void D3D_CoreEngine::Quit()
{
	if (!m_IsRunning)
		return;

	SDL_Quit();
}

