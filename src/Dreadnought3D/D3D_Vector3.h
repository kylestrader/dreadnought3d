#pragma once

#include "D3D_Vector.h"

/// Declaration
namespace D3D_Types
{
	class Vector3f_AVX;

	template <typename T>
	class Vector3 : public Vector<T, 3>
	{
	public:
		Vector3(const Vector<T, 3>& r);
		Vector3(const Vector3f_AVX& r);
		Vector3(T x = (T)0, T y = (T)0, T z = (T)0);
		~Vector3();

		uint32_t GetSize();

		T GetX() const;
		T GetY() const;
		T GetZ() const;
		void SetX(T x);
		void SetY(T y);
		void SetZ(T z);
		Vector3<T> Rotate(T angle, const Vector3<T>& axis);
		Vector3<T> CrossProduct(const Vector3<T>& r) const;
		Vector3 operator=(const Vector3f_AVX& right) const;
	};
}

/// Definition
namespace D3D_Types
{
	template<typename T>
	Vector3<T>::Vector3(const Vector<T, 3>& r)
	{
		(*this)[0] = r[0];
		(*this)[1] = r[1];
		(*this)[2] = r[2];
	}

	template<typename T>
	Vector3<T>::Vector3(const Vector3f_AVX& r)
	{
		(*this)[0] = r[0];
		(*this)[1] = r[1];
		(*this)[2] = r[2];
	}

	template<typename T>
	Vector3<T>::Vector3(T x = (T)0, T y = (T)0, T z = (T)0)
	{
		(*this)[0] = x;
		(*this)[1] = y;
		(*this)[2] = z;
	}

	template<typename T>
	Vector3<T>::~Vector3() {}

	template<typename T>
	uint32_t Vector3<T>::GetSize()
	{
		return 3;
	}

	template<typename T>
	T Vector3<T>::GetX() const
	{
		return (*this)[0];
	}

	template<typename T>
	T Vector3<T>::GetY() const
	{
		return (*this)[1];
	}

	template<typename T>
	T Vector3<T>::GetZ() const
	{
		return (*this)[2];
	}

	template<typename T>
	void Vector3<T>::SetX(T x)
	{
		(*this)[0] = x;
	}

	template<typename T>
	void Vector3<T>::SetY(T y)
	{
		(*this)[1] = y;
	}

	template<typename T>
	void Vector3<T>::SetZ(T z)
	{
		(*this)[2] = z;
	}

	template<typename T>
	Vector3<T> Vector3<T>::Rotate(T angle, const Vector3<T>& axis)
	{
		const T sinAngle = sin(-angle);
		const T cosAngle = cos(-angle);

		Vector3<T> a = this->CrossProduct(axis * sinAngle);
		Vector3<T> b = *this * cosAngle;
		Vector3<T> c = axis * this->DotProduct(axis * ((T)1 - cosAngle));
		return a + b + c;
	}

	template<typename T>
	Vector3<T> Vector3<T>::CrossProduct(const Vector3<T>& r) const
	{
		T x = (*this)[1] * r[2] - (*this)[2] * r[1];
		T y = (*this)[2] * r[0] - (*this)[0] * r[2];
		T z = (*this)[0] * r[1] - (*this)[1] * r[0];

		return Vector3<T>(x, y, z);
	}

	template<typename T>
	Vector3<T> Vector3<T>::operator=(const Vector3f_AVX & right) const
	{
		(*this)[0] = right[0];
		(*this)[1] = right[1];
		(*this)[2] = right[2];
		return *this;
	}
}