#include "D3D_ThreadPoolManager.h"
#include "D3D_ThreadPool.h"

std::shared_ptr<D3D_ThreadPool> D3D_ThreadPoolManager::m_ThreadPool = nullptr;

D3D_ThreadPoolManager::D3D_ThreadPoolManager()
{
}

D3D_ThreadPoolManager::~D3D_ThreadPoolManager()
{
	m_ThreadPool.reset();
}

void D3D_ThreadPoolManager::Initialize(uint32_t numWorkerThreads)
{
	m_ThreadPool = std::make_shared<D3D_ThreadPool>(numWorkerThreads);
}

std::shared_ptr<D3D_ThreadPool> D3D_ThreadPoolManager::GetThreadPool()
{
	return m_ThreadPool;
}
