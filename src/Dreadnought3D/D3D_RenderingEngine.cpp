#include "D3D_RenderingEngine.h"
#include "D3D_Input.h"

#include <string>
#include <memory>

#include <3rdParty\Glew\glew.h>
#include <3rdParty\SDL2\SDL.h>

D3D_RenderingEngine::D3D_RenderingEngine()
{
	m_BufferClearColor = D3D_Types::Vector4<float>(0.f, 0.f, 0.f, 0.f);
}

D3D_RenderingEngine::~D3D_RenderingEngine()
{
	Cleanup();
}

void D3D_RenderingEngine::CreateWindow(int width, int height, const char* title, uint32_t winFlags)
{
	m_Window = std::make_unique<D3D_Window>();
	m_Window->Initialize(width, height, title, winFlags);
}

void D3D_RenderingEngine::ClearScreen()
{
	glClearColor(
		m_BufferClearColor.GetX(),
		m_BufferClearColor.GetY(),
		m_BufferClearColor.GetZ(),
		m_BufferClearColor.GetW()
	);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void D3D_RenderingEngine::InitGraphics(const D3D_Types::Vector4<float>& clearColor)
{
	m_BufferClearColor = clearColor;

	glClearColor(
		m_BufferClearColor.GetX(),
		m_BufferClearColor.GetY(),
		m_BufferClearColor.GetZ(),
		m_BufferClearColor.GetW()
	);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	//TODO: Set toggle for this
	glEnable(GL_TEXTURE_2D);

	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);
}

void D3D_RenderingEngine::WarpMouseToPosition(const D3D_Types::Vector2<int> pos)
{
	m_Window->WarpMouseToPosition(pos);
}

void D3D_RenderingEngine::Cleanup()
{
	DestroyWindow();
}

void D3D_RenderingEngine::Update()
{
	m_Window->Update();
}

void D3D_RenderingEngine::SwapBuffers()
{
	m_Window->SwapBuffers();
}

void D3D_RenderingEngine::DestroyWindow()
{
	if (m_Window != nullptr)
		m_Window.reset();
}
