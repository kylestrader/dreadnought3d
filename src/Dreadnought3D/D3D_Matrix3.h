#pragma once
#include "D3D_Matrix.h"
#include "D3D_Vector2.h"

namespace D3D_Types
{

	template <typename T>
	class Matrix3 : public Matrix<T, 3>
	{
	public:
		typedef unsigned __int32 uint32_t;

		Matrix3() {};

		template <uint32_t D>
		Matrix3(const Matrix<T, D>& r)
		{
			if (D < 3)
			{
				this->InitIdentity();

				for (uint32_t i = 0; i < D; i++)
				{
					for (uint32_t j = 0; j < D; j++)
					{
						(*this)[i][j] = r[i][j];
					}
				}
			}
			else
			{
				for (uint32_t i = 0; i < 3; i++)
				{
					for (uint32_t j = 0; j < 3; j++)
					{
						(*this)[i][j] = r[i][j];
					}
				}
			}
		}
	};
};