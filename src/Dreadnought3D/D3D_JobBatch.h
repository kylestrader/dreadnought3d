#pragma once

#include <memory>
#include <vector>

class D3D_Job;
class D3D_JobBatch
{
public:
	D3D_JobBatch();
	~D3D_JobBatch();

	void AddBatchJob(std::shared_ptr<D3D_Job> job);
	void CompletedJob(std::shared_ptr<D3D_Job> job);
	void SetCompletedAllJobs();

	uint32_t GetNumJobs();
	std::shared_ptr<D3D_Job> GetBatchJob(uint32_t index);

	void WaitForBatch();

private:
	std::vector<std::shared_ptr<D3D_Job>> m_Jobs;
	volatile bool m_JobsCompleted;
};