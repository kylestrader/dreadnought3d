#pragma once

#include <memory>

class D3D_RenderingEngine;

class D3D_GameApp
{
public:
	friend class D3D_CoreEngine;

	D3D_GameApp();
	~D3D_GameApp();

protected:
	//We dont want 3rd party users incorrectly calling these
	virtual void Initialize();
	virtual void Update();
	virtual void Render();
	virtual void Cleanup();
};