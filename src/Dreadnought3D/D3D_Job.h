#pragma once

#include <memory>
#pragma once

#include <vector>

typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;

class D3D_JobBatch;

class D3D_Job
{
public:
	friend class D3D_ThreadPool;
	virtual void Execute() = 0;

	std::shared_ptr<D3D_JobBatch> GetMyJobBatch();

private:
	std::shared_ptr<D3D_JobBatch> m_JobBatch;

	void SetMyJobBatch(std::shared_ptr<D3D_JobBatch> jobbatch);
	void ResetMyJobBatch();
};