#pragma once

#include "D3D_Vector.h"

namespace D3D_Types
{
	template <typename T>
	class Vector4 : public Vector<T, 4>
	{
	public:
		typedef unsigned __int32 uint32_t;
		inline Vector4(const Vector<T, 4>& r)
		{
			(*this)[0] = r[0];
			(*this)[1] = r[1];
			(*this)[2] = r[2];
			(*this)[3] = r[3];
		}
		inline Vector4(T x = (T)0, T y = (T)0, T z = (T)0, T w = (T)0)
		{
			(*this)[0] = x;
			(*this)[1] = y;
			(*this)[2] = z;
			(*this)[3] = w;
		}
		inline ~Vector4() {}

		inline uint32_t GetSize() { return 4; }

		inline T GetX() const { return (*this)[0]; }
		inline T GetY() const { return (*this)[1]; }
		inline T GetZ() const { return (*this)[2]; }
		inline T GetW() const { return (*this)[3]; }
		inline void SetX(T x) { (*this)[0] = x; }
		inline void SetY(T y) { (*this)[1] = y; }
		inline void SetZ(T z) { (*this)[2] = z; }
		inline void SetW(T w) { (*this)[3] = w; }
	};
};