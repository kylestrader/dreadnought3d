#include "D3D_Transform.h"

float D3D_Transform::s_ZNear = 0.1f;
float D3D_Transform::s_ZFar = 1000.f;
float D3D_Transform::s_Width = 800.f;
float D3D_Transform::s_Height = 600.f;
float D3D_Transform::s_FOV = 70.f;

std::shared_ptr<D3D_Camera> D3D_Transform::m_Camera = nullptr;

typedef D3D_Types::Vector3<float> Vector3f;
typedef D3D_Types::Matrix4<float> Matrix4f;

D3D_Transform::D3D_Transform()
{
	m_Translation = Vector3f(0.0f, 0.0f, 0.0f);
	m_EulerRotation = Vector3f(0.0f, 0.0f, 0.0f);
	m_Scale = Vector3f(1.0f, 1.0f, 1.0f);
}

D3D_Transform::~D3D_Transform()
{
}

Vector3f D3D_Transform::GetTranslation() const
{
	return m_Translation;
}

Vector3f D3D_Transform::GetEulerRotation() const
{
	return m_EulerRotation;
}

Vector3f D3D_Transform::GetScale() const
{
	return m_Scale;
}

Matrix4f D3D_Transform::InitTranslationMatrix()
{
	Matrix4f translation;
	return translation.InitTranslation(Vector3f(m_Translation.GetX(), m_Translation.GetY(), m_Translation.GetZ()));
}

Matrix4f D3D_Transform::InitEulerRotationMatrix()
{
	Matrix4f eulerRotation;
	return eulerRotation.InitRotationEuler(Vector3f(m_EulerRotation.GetX(), m_EulerRotation.GetY(), m_EulerRotation.GetZ()));
}

Matrix4f D3D_Transform::InitScaleMatrix()
{
	Matrix4f scale;
	return scale.InitScale(Vector3f(m_Scale.GetX(), m_Scale.GetY(), m_Scale.GetZ()));
}

Matrix4f D3D_Transform::InitCameraPerspectiveMatrix()
{
	Matrix4f perspective;
	return perspective.InitPerspective(s_FOV, s_Width / s_Height, s_ZNear, s_ZFar);
}

Matrix4f D3D_Transform::InitCameraRotationFromDirectionMatrix()
{
	Matrix4f cameraRotationMatrix;
	return cameraRotationMatrix.InitRotationFromDirection(m_Camera->GetForward(), m_Camera->GetUp());
}

Matrix4f D3D_Transform::InitCamerTranslationMatrix()
{
	Matrix4f cameraTranslationMatrix;
	return cameraTranslationMatrix.InitTranslation(m_Camera->GetPos() * -1.0f);
}

std::shared_ptr<D3D_Camera> D3D_Transform::GetCamera()
{
	return m_Camera;
}

void D3D_Transform::SetTranslation(const Vector3f& translation)
{
	m_Translation = translation;
}

void D3D_Transform::SetTranslation(float x, float y, float z)
{
	m_Translation = Vector3f(x, y, z);
}

void D3D_Transform::SetEulerRotation(const Vector3f& eulerRotation)
{
	m_EulerRotation = eulerRotation;
}

void D3D_Transform::SetEulerRotation(float x, float y, float z)
{
	m_EulerRotation = Vector3f(x, y, z);
}

void D3D_Transform::SetScale(const Vector3f & scale)
{
	m_Scale = scale;
}

void D3D_Transform::SetScale(float x, float y, float z)
{
	m_Scale = Vector3f(x, y, z);
}

void D3D_Transform::SetProjection(float near, float far, float width, float height, float fov)
{
	s_ZNear = near;
	s_ZFar = far;
	s_Width = width;
	s_Height = height;
	s_FOV = fov;
}

void D3D_Transform::SetCamera(std::shared_ptr<D3D_Camera> cam)
{
	m_Camera = cam;
}

Matrix4f D3D_Transform::GetTransformation()
{
	// translation
	Matrix4f translationMatrix;
	translationMatrix.InitTranslation(Vector3f(m_Translation.GetX(), m_Translation.GetY(), m_Translation.GetZ()));
	
	// euler rotation
	Matrix4f eulerRotationMatrix;
	eulerRotationMatrix.InitRotationEuler(Vector3f(m_EulerRotation.GetX(), m_EulerRotation.GetY(), m_EulerRotation.GetZ()));
	
	// scale
	Matrix4f scaleMatrix;
	scaleMatrix.InitScale(Vector3f(m_Scale.GetX(), m_Scale.GetY(), m_Scale.GetZ()));

	return translationMatrix * eulerRotationMatrix * scaleMatrix;
	//return eulerRotationMatrix;
}

Matrix4f D3D_Transform::GetProjectedTransformation()
{
	//Transformation Matrix
	Matrix4f worldMatrix = GetTransformation();

	//Projection Matrix
	Matrix4f projectionMatrix;
	projectionMatrix.InitPerspective(s_FOV, s_Width/s_Height, s_ZNear, s_ZFar);

	// Camera Rotation Matrix
	Matrix4f cameraRotationMatrix;
	cameraRotationMatrix.InitRotationFromDirection(m_Camera->GetForward(), m_Camera->GetUp());

	// Camera Translation Matrix
	Matrix4f cameraTranslationMatrix;
	cameraTranslationMatrix.InitTranslation(m_Camera->GetPos() * -1.0f);

	Matrix4f viewProjection = projectionMatrix * cameraRotationMatrix * cameraTranslationMatrix;

	return viewProjection * worldMatrix;
}
