#pragma once

#include "D3D_Vector3.h"
class D3D_Camera
{
public:
	typedef D3D_Types::Vector3<float> Vector3f;

	static Vector3f s_YAxis;

	D3D_Camera(Vector3f pos, Vector3f forward, Vector3f up);
	D3D_Camera();
	~D3D_Camera();

	Vector3f GetPos();
	Vector3f GetForward();
	Vector3f GetUp();

	void SetPos(Vector3f pos);
	void SetForward(Vector3f forward);
	void SetUp(Vector3f up);

	void Move(Vector3f dir, float amt);

	Vector3f GetLeft();
	Vector3f GetRight();

	void RotateX(float angle);
	void RotateY(float angle);

private:
	Vector3f m_Pos;
	Vector3f m_Forward;
	Vector3f m_Up;
};