#pragma once

#include <string>
#include <memory>

#include "D3D_Vector2.h"

#include <3rdParty/SDL2/SDL.h>

enum D3D_WindowFlags
{
	D3D_WINDOW_FULLSCREEN = SDL_WindowFlags::SDL_WINDOW_FULLSCREEN,						/**< full screen window */
	D3D_WINDOW_OPENGL = SDL_WindowFlags::SDL_WINDOW_OPENGL,								/**< window usable with OpenGL context */
	D3D_WINDOW_SHOWN = SDL_WindowFlags::SDL_WINDOW_SHOWN,								/**< window is visible */
	D3D_WINDOW_HIDDEN = SDL_WindowFlags::SDL_WINDOW_HIDDEN,								/**< window is not visible */
	D3D_WINDOW_BORDERLESS = SDL_WindowFlags::SDL_WINDOW_BORDERLESS,						/**< no window decoration */
	D3D_WINDOW_RESIZABLE = SDL_WindowFlags::SDL_WINDOW_RESIZABLE,						/**< window can be resized */
	D3D_WINDOW_MINIMIZED = SDL_WindowFlags::SDL_WINDOW_MINIMIZED,						/**< window is minimized */
	D3D_WINDOW_MAXIMIZED = SDL_WindowFlags::SDL_WINDOW_MAXIMIZED,						/**< window is maximized */
	D3D_WINDOW_INPUT_GRABBED = SDL_WindowFlags::SDL_WINDOW_INPUT_GRABBED,				/**< window has grabbed input focus */
	D3D_WINDOW_INPUT_FOCUS = SDL_WindowFlags::SDL_WINDOW_INPUT_FOCUS,					/**< window has input focus */
	D3D_WINDOW_MOUSE_FOCUS = SDL_WindowFlags::SDL_WINDOW_MOUSE_FOCUS,					/**< window has mouse focus */
	D3D_WINDOW_FULLSCREEN_DESKTOP = SDL_WindowFlags::SDL_WINDOW_FULLSCREEN_DESKTOP,
	D3D_WINDOW_FOREIGN = SDL_WindowFlags::SDL_WINDOW_FOREIGN,							/**< window not created by SDL */
	D3D_WINDOW_ALLOW_HIGHDPI = SDL_WindowFlags::SDL_WINDOW_ALLOW_HIGHDPI				/**< window should be created in high-DPI mode if supported */
};

struct SDL_WindowDestructor {
	void operator()(SDL_Window*window) {
		SDL_DestroyWindow(window);
	}
};

class D3D_Window
{
public:
	friend class D3D_CoreEngine;

	D3D_Window();
	~D3D_Window();

	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;

	uint32_t Initialize(int width, int height, const char* title, uint32_t winFlags);
	void WarpMouseToPosition(const D3D_Types::Vector2<int>& pos);
	void WarpMouseToPosition(uint16_t xPos, uint16_t yPos);
	uint32_t GetWidth();
	uint32_t GetHeight();
	std::string GetTitle();
	uint32_t SwapBuffers();
	void Update();

private:

	std::unique_ptr<SDL_Window, SDL_WindowDestructor> m_Window;

	SDL_GLContext m_glContext;

	uint32_t m_Width;
	uint32_t m_Height;
	std::string m_Title;
};