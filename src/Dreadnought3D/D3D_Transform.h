#pragma once

#include <memory>

#include "D3D_Vector3.h"
#include "D3D_Matrix4.h"
#include "D3D_Camera.h"

class D3D_Transform
{
public:
	typedef D3D_Types::Vector3<float> Vector3f;
	typedef D3D_Types::Matrix4<float> Matrix4f;

	D3D_Transform();
	~D3D_Transform();

	// Getters
	Vector3f GetTranslation() const;
	Vector3f GetEulerRotation() const;
	Vector3f GetScale() const;
	Matrix4f InitTranslationMatrix();
	Matrix4f InitEulerRotationMatrix();
	Matrix4f InitScaleMatrix();
	Matrix4f InitCameraPerspectiveMatrix();
	Matrix4f InitCameraRotationFromDirectionMatrix();
	Matrix4f InitCamerTranslationMatrix();

	std::shared_ptr<D3D_Camera> GetCamera();
	
	// Setters
	void SetTranslation(const Vector3f& translation);
	void SetTranslation(float x, float y, float z);
	void SetEulerRotation(const Vector3f& eulerRotation);
	void SetEulerRotation(float x, float y, float z);
	void SetScale(const Vector3f& scale);
	void SetScale(float x, float y, float z);
	void SetProjection(float near, float far, float width, float height, float fov);
	void SetCamera(std::shared_ptr<D3D_Camera> cam);

	Matrix4f GetTransformation();
	Matrix4f GetProjectedTransformation();

private:
	Vector3f m_Translation;
	Vector3f m_EulerRotation;
	Vector3f m_Scale;

	static std::shared_ptr<D3D_Camera> m_Camera;

	static float s_ZNear;
	static float s_ZFar;
	static float s_Width;
	static float s_Height;
	static float s_FOV;
};