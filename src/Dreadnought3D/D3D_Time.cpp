#include "D3D_Time.h"

double D3D_Time::Helper::DELTA_T = 0.0;

double D3D_Time::Helper::MSToSec(double ms)
{
	return ms / (double)1000.0;
}

double D3D_Time::Helper::SecToMS(double sec)
{
	return sec * (double)1000.0;
}

double D3D_Time::Helper::MCSToMS(double mcs)
{
	return mcs / (double)1000.0;
}

double D3D_Time::Helper::MSToMCS(double ms)
{
	return ms * (double)1000.0;
}

void D3D_Time::Timer::Start()
{
	m_StartTime = ch_timer::now();
	m_CurrentTime = m_StartTime;
	m_PreviousTime = m_StartTime;
}

double D3D_Time::Timer::Sleep_s(double timeInSec)
{
	return D3D_Time::Helper::MSToSec(sleep<ch_milliseconds>(D3D_Time::Helper::SecToMS(timeInSec)));
}

double D3D_Time::Timer::Sleep_ms(double timeInMs)
{
	return sleep<ch_milliseconds>(timeInMs);
}

void D3D_Time::Timer::Stop()
{
	m_PreviousTime = m_CurrentTime;
	m_CurrentTime = ch_timer::now();
}

double D3D_Time::Timer::GetElapsedTime_ms()
{
	return getElapsedTime<ch_milliseconds>();
}

double D3D_Time::Timer::GetElapsedTime_mcs()
{
	return getElapsedTime<ch_microseconds>();
}

double D3D_Time::Timer::GetElapsedTime_ns()
{
	return getElapsedTime<ch_nanoseconds>();
}

double D3D_Time::Timer::GetElapsedTime_s()
{
	return D3D_Time::Helper::MSToSec(getElapsedTime<ch_milliseconds>());
}

double D3D_Time::Timer::GetTotalTime_ms()
{
	return getTotalTime<ch_milliseconds>();
}

double D3D_Time::Timer::GetTotalTime_mcs()
{
	return getTotalTime<ch_microseconds>();
}

double D3D_Time::Timer::GetTotalTime_ns()
{
	return getTotalTime<ch_nanoseconds>();
}

double D3D_Time::Timer::GetTotalTime_s()
{
	return D3D_Time::Helper::MSToSec(getTotalTime<ch_milliseconds>());
}
