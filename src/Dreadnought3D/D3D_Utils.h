#pragma once

#include <cassert>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include <3rdParty\Glew\glew.h>

// Misc.
#define INVALID_VALUE 0xFFFFFFFF

// Math
#define MATH_PI 3.14159265359

// Resources
#define RESOURCE_URL "./res/shaders/"

#define _AVX D3D_Utils::Cpu::AVX_READY

namespace D3D_Utils
{
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;

	namespace Cpu
	{
		static bool AVX_READY = true;

		inline unsigned int GetCpuFeatureFlags()
		{
			unsigned int features;

			__asm
			{
				// Save registers
				push    eax
				push    ebx
				push    ecx
				push    edx

				// Get the feature flags (eax=1) from edx
				mov     eax, 1
				cpuid
				mov     features, edx

				// Restore registers
				pop     edx
				pop     ecx
				pop     ebx
				pop     eax
			}
			return features;
		}

		inline bool GetCpu_AVX_Support()
		{
			unsigned int hex = GetCpuFeatureFlags();
			AVX_READY = ((hex & (int)1 << 28) != 0);
			return AVX_READY;
		}
	}

	namespace Math
	{
		inline float Rad2Deg(float rad)
		{
			return (float)(rad * ((float)180.0 / (float)MATH_PI));
		}

		inline float Deg2Rad(float deg)
		{
			return (float)(deg * ((float)MATH_PI / (float)180.0));
		}

		template <typename T>
		inline T Clamp(const T& value, const T& low, const T& high)
		{
			return value < low ? low : (value > high ? high : value);
		}
	};

	namespace StringOps
	{
		inline std::vector<std::string> Split(const std::string& s, char delim)
		{
			std::vector<std::string> elems;

			const char* cstr = s.c_str();
			uint32_t strLength = (uint32_t)s.length();
			uint32_t start = 0;
			uint32_t end = 0;

			while (end <= strLength)
			{
				while (end <= strLength)
				{
					if (cstr[end] == delim)
						break;
					end++;
				}

				elems.push_back(s.substr(start, end - start));
				start = end + 1;
				end = start;
			}

			return elems;
		}

		inline void print_string(std::string str)
		{
			printf(str.c_str());
		}
	};

	namespace Rendering
	{
		inline const char* GetOpenGLVersion()
		{
			return (char*)(glGetString(GL_VERSION));
		}
	};

	namespace ResourceLoading
	{
		inline std::string LoadShader(const std::string& fileName)
		{
			std::ifstream file;
			file.open((RESOURCE_URL + fileName).c_str());

			std::string output;
			std::string line;

			if (file.good())
			{
				while (file.good())
				{
					getline(file, line);

					if (line.find("#include") == std::string::npos)
						output.append(line + "\n");
					else
					{
						std::string includeFileName = D3D_Utils::StringOps::Split(line, ' ')[1];
						includeFileName = includeFileName.substr(1, includeFileName.length() - 2);

						std::string toAppend = LoadShader(includeFileName);
						output.append(toAppend + "\n");
					}
				}
			}
			else
			{
				fprintf(stderr, "Unable to load shader: %s\n", fileName.c_str());
			}

			return output;
		}
	};

	namespace Assertion
	{
		inline void AssertError(bool expr, const char* error)
		{
			if (expr)
			{
				fprintf(stderr, error);
				assert(expr);
			}
		}

		inline void AssertError(bool expr, std::string error)
		{
			AssertError(expr, error.c_str());
		}
	};
};