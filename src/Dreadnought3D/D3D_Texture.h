#pragma once

#include <string>
#include <map>
#include <memory>

#include<3rdParty\Glew\glew.h>

namespace D3D_TextureTypes
{
	typedef unsigned __int32 uint32_t;
	typedef __int32 int32_t;

	class TextureData
	{
	public:
		TextureData(GLenum textureTarget,
			int width,
			int height,
			int numTextures,
			unsigned char** data,
			GLfloat* filters,
			GLenum* internalformat,
			GLenum* format, 
			bool clamp, 
			GLenum* attachments);

		~TextureData();

		void Bind(int textureNum) const;
		void BindAsRenderTarget() const;

		inline int GetWidth() const {return m_Width; };
		inline int GetHeight() const {return m_Height; };

	private:
		TextureData(TextureData& other) {}
		void operator=(TextureData& other) {}

		void InitTextures(unsigned char** data, GLfloat* filters, GLenum* internalFormat, GLenum* format, bool clamp);
		void InitRenderTargets(GLenum* attachments);

		GLuint* m_TextureID;
		GLenum m_TextureTarget;
		GLuint m_FrameBuffer;
		GLuint m_RenderBuffer;
		
		uint32_t m_NumTextures;
		uint32_t m_Width;
		uint32_t m_Height;
	};

	class Texture
	{
	public:
		Texture(const std::string& fileName, 
			GLenum textureTarget = GL_TEXTURE_2D, 
			GLfloat filter = GL_LINEAR_MIPMAP_LINEAR, 
			GLenum internalFormat = GL_RGBA, 
			GLenum format = GL_RGBA, 
			bool clamp = false, 
			GLenum attachment = GL_NONE);

		Texture(int width = 0,
			int height = 0,
			unsigned char* data = 0,
			GLenum textureTarget = GL_TEXTURE_2D,
			GLfloat filter = GL_LINEAR_MIPMAP_LINEAR,
			GLenum internalFormat = GL_RGBA,
			GLenum format = GL_RGBA,
			bool clamp = false,
			GLenum attachment = GL_NONE);

		Texture(const Texture& texture);
		void operator=(Texture& texture);
		~Texture();

		void Bind(uint32_t unit = 0) const;
		void BindAsRenderTarget() const;

		inline int32_t GetWidth() const { return m_TextureData->GetWidth(); }
		inline int32_t Getheight() const { return m_TextureData->GetHeight(); }

		inline bool operator==(const Texture& texture) const { return m_TextureData == texture.m_TextureData; }
		inline bool operator!=(const Texture& texture) const { return !operator==(texture); }

	private:
		static std::map<std::string, std::shared_ptr<TextureData>> s_ResourceMap;

		std::shared_ptr<TextureData> m_TextureData;
		std::string m_FileName;
	};
}