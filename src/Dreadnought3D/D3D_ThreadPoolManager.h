#pragma once

#include <memory>

class D3D_ThreadPool;

class D3D_ThreadPoolManager
{
public:
	typedef __int32 int32_t;
	typedef unsigned __int32 uint32_t;

	D3D_ThreadPoolManager();
	~D3D_ThreadPoolManager();

	static void Initialize(uint32_t numWorkerThreads);
	static std::shared_ptr<D3D_ThreadPool> GetThreadPool();

private:
	static std::shared_ptr<D3D_ThreadPool> m_ThreadPool;
};