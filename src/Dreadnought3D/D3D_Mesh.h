#pragma once 

#include "D3D_Vector3.h"
#include "D3D_Vector2.h"

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <3rdParty\Glew\glew.h>

namespace D3D_MeshTypes
{
	class IndexedModel
	{
	public:
		typedef unsigned __int32 uint32_t;

		IndexedModel();
		IndexedModel(const std::vector<uint32_t>& indices,
			const std::vector<D3D_Types::Vector3<float>>& positions,
			const std::vector<D3D_Types::Vector2<float>>& texCoords,
			const std::vector<D3D_Types::Vector3<float>>& normals = std::vector<D3D_Types::Vector3<float>>(),
			const std::vector<D3D_Types::Vector3<float>>& tangents = std::vector<D3D_Types::Vector3<float>>());

		bool IsValid() const;
		void CalcNormals();
		void CalcTangents();

		IndexedModel Finalize();

		void AddVertex(const D3D_Types::Vector3<float>& vert);
		void AddVertex(float x, float y, float z);

		void AddTexCoord(const D3D_Types::Vector2<float>& coord);
		void AddTexCoord(float x, float y);

		void AddNormal(const D3D_Types::Vector3<float>& normal);
		void AddNormal(float x, float y, float z);

		void AddTangent(const D3D_Types::Vector3<float>& tangent);
		void AddTangent(float x, float y, float z);

		void AddFace(uint32_t vertIndex0, uint32_t vertIndex1, uint32_t vertIndex2);

		const std::vector<uint32_t>& GetIndices() const;
		const std::vector<D3D_Types::Vector3<float>>& GetPositions() const;
		const std::vector<D3D_Types::Vector2<float>>& GetTexCoords() const;
		const std::vector<D3D_Types::Vector3<float>>& GetNormals() const;
		const std::vector<D3D_Types::Vector3<float>>& GetTangents() const;

	protected:

	private:
		std::vector<uint32_t> m_Indices;
		std::vector<D3D_Types::Vector3<float>> m_Positions;
		std::vector<D3D_Types::Vector2<float>> m_TexCoords;
		std::vector<D3D_Types::Vector3<float>> m_Normals;
		std::vector<D3D_Types::Vector3<float>> m_Tangents;
	};

	class MeshData
	{
	public:
		MeshData(const IndexedModel& model);
		~MeshData();

		void Draw() const;

	private:
		MeshData(MeshData& other);
		void operator=(MeshData& other);

		enum
		{
			POSITION_VB,
			TEXCOORD_VB,
			NORMAL_VB,
			TANGENT_VB,

			INDEX_VB,

			NUM_BUFFERS
		};

		GLuint m_VertexArrayObject;
		GLuint m_VertexArrayBuffers[NUM_BUFFERS];
		int m_DrawCount;
	};

	class Mesh
	{
	public:
		typedef D3D_Types::Vector3<float> Vector3f;
		typedef D3D_Types::Vector2<float> Vector2f;
		Mesh(const std::string& fileName);
		Mesh(const std::string& meshName, const IndexedModel& model);
		Mesh(const Mesh& mesh);
		~Mesh();

		void Draw();

	private:
		static std::map<std::string, std::shared_ptr<MeshData>> s_ResourceMap;

		std::string m_FileName;
		std::shared_ptr<MeshData> m_meshData;

		void operator=(Mesh& mesh);
	};
}