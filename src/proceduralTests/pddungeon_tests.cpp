#include <3rdParty/CATCH/catch.hpp>
#include <memory>
#include <procedural/pdroom.h>
#include <procedural/pddungeon.h>
#include <procedural/pdgenerator.h>
#include <procedural/pdconnection.h>
#include <procedural/pdlockeddoor.h>

namespace PDTests
{
    typedef std::unique_ptr<PDDungeon>      pd_dung_uptr;
    typedef std::shared_ptr<PDDungeon>      pd_dung_ptr;
    typedef std::shared_ptr<PDRoom>         pd_room_ptr;
    typedef std::shared_ptr<PDConnection>   pd_con_ptr;
    typedef std::shared_ptr<PDLockedDoor>   pd_door_ptr;
    typedef std::vector<int>                pd_vec;
    typedef std::vector<pd_room_ptr>        pd_room_ptr_list;
    typedef std::vector<pd_door_ptr>        pd_door_ptr_list;
    typedef std::vector<pd_con_ptr>         pd_con_ptr_list;
    typedef std::unordered_set<int>         pd_uset;
    typedef std::set<pd_room_ptr>           pd_room_ptr_set;

template <typename T>
bool findElement(const std::vector<T>& source, T value)
{
    std::vector<T>::const_iterator it = source.begin();
    for (; it != source.end(); it++)
    {
        if (*it == value)
            return true;
    }

    return false;
}

TEST_CASE("procedural/pddungeon.IsValidIndex", "Test to make sure dungeons can properly determine valid indexes")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    REQUIRE(dungeon->IsValidIndex(10));
    REQUIRE_FALSE(dungeon->IsValidIndex(-1));
    REQUIRE_FALSE(dungeon->IsValidIndex(25));
}

TEST_CASE("procedural/pddungeon.IsValid", "Test to make sure dungeons can properly determine valid grid positions")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    REQUIRE(dungeon->IsValid(0, 0));
    REQUIRE(dungeon->IsValid(4, 4));
    REQUIRE(dungeon->IsValid(0, 4));
    REQUIRE(dungeon->IsValid(4, 0));
    REQUIRE_FALSE(dungeon->IsValid(-1, 0));
    REQUIRE_FALSE(dungeon->IsValid(0, -1));
    REQUIRE_FALSE(dungeon->IsValid(5, 0));
    REQUIRE_FALSE(dungeon->IsValid(0, 5));
    REQUIRE_FALSE(dungeon->IsValid(-1, -1));
}

TEST_CASE("procedural/pddungeon.GetStartRoom", "Test to make sure dungeons can properly retrieve their start room")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr room1 = dungeon->GetRoom(0, 0);
    pd_room_ptr room2 = dungeon->GetRoom(1, 1);

    int gridX;
    int gridY;
    dungeon->SetStartRoom(0, 0);
    dungeon->GetStartRoom(gridX, gridY);
    CHECK(dungeon->GetStartRoom() == room1);
    CHECK(gridX == 0);
    CHECK(gridY == 0);

    dungeon->SetStartRoom(room2);
    dungeon->GetStartRoom(gridX, gridY);
    CHECK(dungeon->GetStartRoom() == room2);
    CHECK(gridX == 1);
    CHECK(gridY == 1);
}

TEST_CASE("procedural/pddungeon.To1DPosition", "Test to make sure the dungeon returns the proper 1D position given a 2D position")
{
    // I am using a 5x5 grid as an example
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);

    // Top Left
    CHECK(dungeon->To1DPos(0, 0) == 0);

    // Bottom Right
    CHECK(dungeon->To1DPos(4, 4) == 24);

    // Center
    CHECK(dungeon->To1DPos(2, 2) == 12);
}

TEST_CASE("procedural/pddungeon.To2DPosition", "Test to make sure the dungeon returns the proper 1D position given a 2D position")
{
    // I am using a 5x5 grid as an example
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    int gridX = -1;
    int gridY = -1;

    // Top Left
    dungeon->To2DPos(0, gridX, gridY);
    CHECK(gridX == 0);
    CHECK(gridY == 0);

    // Bottom Right
    dungeon->To2DPos(24, gridX, gridY);
    CHECK(gridX == 4);
    CHECK(gridY == 4);

    // Center
    dungeon->To2DPos(12, gridX, gridY);
    CHECK(gridX == 2);
    CHECK(gridY == 2);
}

TEST_CASE("procedural/pddungeon.GetMaxConnections", "Test to make sure the dungeon properly calculates the max number of connections allowed")
{
    // I am using a 5x5 grid as an example
    pd_dung_uptr dungeon1 = std::make_unique<PDDungeon>(5, 5);
    pd_dung_uptr dungeon2 = std::make_unique<PDDungeon>(2, 2);
    pd_dung_uptr dungeon3 = std::make_unique<PDDungeon>(8, 8);

    REQUIRE(dungeon1->GetMaxConnections() == 40);
    REQUIRE(dungeon2->GetMaxConnections() == 4);
    REQUIRE(dungeon3->GetMaxConnections() == 112);
}

TEST_CASE("procedural/pddungeon.AddLockedDoor", "Test to make sure dungeons can properly add a locked door")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_con_ptr con = PDGenerator::CreateConnection(room1, room2);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1, 1);
    dungeon->AddConnection(con);

    REQUIRE(dungeon->GetNumLockedDoors() == 0);
    REQUIRE_FALSE(con->IsLocked());
    dungeon->AddLockedDoor(con);
    CHECK(dungeon->GetNumLockedDoors() == 1);
    CHECK(dungeon->GetLockedDoor(0) == con->GetLockedDoor());
}

TEST_CASE("procedural/pddungeon.AddRoom", "Test to make sure dungeons can properly add a single room")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();

    REQUIRE(dungeon->GetNumRoomsAdded() == 0);

    dungeon->AddRoom(room1, 0);

    REQUIRE(dungeon->GetNumRoomsAdded() == 1);
    CHECK(room1->GetGridX() == 0);
    CHECK(room1->GetGridY() == 0);

    dungeon->AddRoom(room2, 1, 1);

    REQUIRE(dungeon->GetNumRoomsAdded() == 2);
    CHECK(room2->GetGridX() == 1);
    CHECK(room2->GetGridY() == 1);
}

TEST_CASE("procedural/pddungeon.GetRoom", "Test to make sure dungeons can properly retrieve a single room")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);

    REQUIRE(dungeon->GetRoom(0) == room1);
    REQUIRE(dungeon->GetRoom(0, 0) == room1);
    REQUIRE_FALSE(dungeon->GetRoom(1) == room1);
}

TEST_CASE("procedural/pddungeon.AddRooms", "Test to make sure dungeons can properly add multiple rooms")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    std::vector<pd_room_ptr> rooms1;
    std::vector<pd_room_ptr> rooms2;
    pd_vec indexes;
    std::vector<std::pair<int, int>> gridPositions;

    for (int i = 0; i < 5; i++)
    {
        rooms1.push_back(PDGenerator::CreateRoom());
        indexes.push_back(i);
    }

    for (int i = 0; i < 5; i++)
    {
        rooms2.push_back(PDGenerator::CreateRoom());
        gridPositions.push_back(std::pair<int, int>(i, 1));
    }

    REQUIRE(dungeon->GetNumRoomsAdded() == 0);

    dungeon->AddRooms(rooms1, indexes);

    REQUIRE(dungeon->GetNumRoomsAdded() == 5);

    dungeon->AddRooms(rooms2, gridPositions);

    REQUIRE(dungeon->GetNumRoomsAdded() == 10);
}

TEST_CASE("procedural/pddungeon.AddConnection", "Test to make sure dungeons can properly add a connection")
{
	pd_room_ptr room1 = PDGenerator::CreateRoom();
	pd_room_ptr room2 = PDGenerator::CreateRoom();
	pd_con_ptr con = std::make_shared<PDConnection>(room1, room2);
	pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);

	REQUIRE(dungeon->GetNumConnections() == 0);
	dungeon->AddConnection(con);
	REQUIRE(dungeon->GetNumConnections() == 1);
}

TEST_CASE("procedural/pddungeon.AddConnections", "Test to make sure dungeons can properly add connections")
{
	std::vector<pd_con_ptr> cons;
	pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);

	for (int i = 0; i < 5; i++)
	{
		pd_room_ptr room1 = PDGenerator::CreateRoom();
		pd_room_ptr room2 = PDGenerator::CreateRoom();
		pd_con_ptr con = std::make_shared<PDConnection>(room1, room2);

		cons.push_back(con);
	}

	REQUIRE(dungeon->GetNumConnections() == 0);
	dungeon->AddConnections(cons);
	REQUIRE(dungeon->GetNumConnections() == 5);
}

TEST_CASE("procedural/pddungeon.GetConnections", "Test to make sure dungeons can properly retrieve connections for a room")
{
	std::vector<pd_con_ptr> cons;
	pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();

	for (int i = 0; i < 5; i++)
	{
		pd_room_ptr room2 = PDGenerator::CreateRoom();
		pd_con_ptr con = std::make_shared<PDConnection>(room1, room2);

        dungeon->AddRoom(room2, i);
		cons.push_back(con);
	}

    dungeon->AddRoom(room1, 5);
	
	pd_vec indexes = dungeon->AddConnections(cons);
    CHECK(indexes.size() == 5);

    pd_uset in = dungeon->GetConnections(room1);
    pd_uset::iterator it;

    for (unsigned int i = 0; i < indexes.size(); i++)
    {
        it = in.find(indexes[i]);

        CHECK_FALSE(it == in.end()); // We should find every single index since this room is connected to all other rooms
    }
}

TEST_CASE("procedural/pddungeon.GetConnection", "Test to make sure dungeons can properly retrieve connections for a room")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_con_ptr con = std::make_shared<PDConnection>(room1, room2);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);

    int index = dungeon->AddConnection(con);

    REQUIRE(dungeon->GetConnection(index) == con);
    REQUIRE(dungeon->GetConnection(room1, room2) == con);
}

TEST_CASE("procedural/pddungeon.GetRoomNeighbors", "Test to make sure dungeons can properly retrieve neighbors for a room")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr room = dungeon->GetRoom(0);

    // Top Left
    pd_room_ptr_list neighbors = dungeon->GetRoomNeighbors(room);
    REQUIRE(neighbors.size() == 2);
    CHECK(neighbors[0]->GetGridX() == 1);
    CHECK(neighbors[0]->GetGridY() == 0);
    CHECK(neighbors[1]->GetGridX() == 0);
    CHECK(neighbors[1]->GetGridY() == 1);

    // Center
    neighbors = dungeon->GetRoomNeighbors(dungeon->To1DPos(2, 2));
    REQUIRE(neighbors.size() == 4);
    CHECK(neighbors[0]->GetGridX() == 2);
    CHECK(neighbors[0]->GetGridY() == 1);
    CHECK(neighbors[1]->GetGridX() == 3);
    CHECK(neighbors[1]->GetGridY() == 2);
    CHECK(neighbors[2]->GetGridX() == 2);
    CHECK(neighbors[2]->GetGridY() == 3);
    CHECK(neighbors[3]->GetGridX() == 1);
    CHECK(neighbors[3]->GetGridY() == 2);
}

TEST_CASE("procedural/pddungeon.GetRoomNeighborsIndexes", "Test to make sure dungeons can properly retrieve neighbor indexes for a room")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr room = dungeon->GetRoom(0);

    // Top Left
    pd_vec neighbors = dungeon->GetRoomNeighborsIndexes(room);
    REQUIRE(neighbors.size() == 2);
    CHECK(neighbors[0] == 1);
    CHECK(neighbors[1] == 5);

    // Center
    neighbors = dungeon->GetRoomNeighborsIndexes(dungeon->To1DPos(2, 2));
    REQUIRE(neighbors.size() == 4);
    CHECK(neighbors[0] == 7);
    CHECK(neighbors[1] == 13);
    CHECK(neighbors[2] == 17);
    CHECK(neighbors[3] == 11);
}

TEST_CASE("procedural/pddungeon.HasConnection", "Test to make sure dungeons can determine if a connection exists")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_room_ptr room3 = PDGenerator::CreateRoom();
    pd_con_ptr con = std::make_shared<PDConnection>(room1, room2);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddRoom(room3, 2);
    dungeon->AddConnection(con);

    // Make sure it works both ways
    CHECK(dungeon->HasConnection(room1, room2));
    CHECK(dungeon->HasConnection(room2, room1));

    CHECK_FALSE(dungeon->HasConnection(room1, room3));
    CHECK_FALSE(dungeon->HasConnection(room2, room3));
}

TEST_CASE("procedural/pddungeon.GetRoomUnlockedNeighbors", "Test to make sure the dungeon can properly get the unlocked neighbors for a room")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr roomCenter = dungeon->GetRoom(1, 1);
    pd_room_ptr roomNorth = dungeon->GetRoom(1, 0);
    pd_room_ptr roomEast = dungeon->GetRoom(2, 1);
    pd_room_ptr roomSouth = dungeon->GetRoom(1, 2);
    pd_room_ptr roomWest = dungeon->GetRoom(0, 1);
    int index = dungeon->To1DPos(1, 1);

    pd_con_ptr conNorth = PDGenerator::CreateConnection(roomCenter, roomNorth);
    pd_con_ptr conEast = PDGenerator::CreateConnection(roomCenter, roomEast);
    pd_con_ptr conSouth = PDGenerator::CreateConnection(roomCenter, roomSouth);
    pd_con_ptr conWest = PDGenerator::CreateConnection(roomCenter, roomWest);

    dungeon->AddConnection(conNorth);
    dungeon->AddConnection(conEast);
    dungeon->AddConnection(conSouth);
    dungeon->AddConnection(conWest);

    // Purposely lock these
    dungeon->AddLockedDoor(conNorth);
    dungeon->AddLockedDoor(conSouth);

    // Make sure it returns only the rooms without a lock
    pd_room_ptr_list unlockNeighbors = dungeon->GetRoomUnlockedNeighbors(roomCenter);
    CHECK(unlockNeighbors.size() == 2);
    CHECK(unlockNeighbors[0] == roomEast);
    CHECK(unlockNeighbors[1] == roomWest);

    // Now we give an exception to this
    pd_vec unlockedDoors;
    unlockedDoors.push_back(conNorth->GetID());

    unlockNeighbors = dungeon->GetRoomUnlockedNeighbors(roomCenter, unlockedDoors);
    CHECK(unlockNeighbors.size() == 3);
    CHECK(unlockNeighbors[0] == roomNorth);
    CHECK(unlockNeighbors[1] == roomEast);
    CHECK(unlockNeighbors[2] == roomWest);

    // Repeat above tests using the index of the center room instead of the reference

    // Make sure it returns only the rooms without a lock
    unlockNeighbors = dungeon->GetRoomUnlockedNeighbors(index);
    CHECK(unlockNeighbors.size() == 2);
    CHECK(unlockNeighbors[0] == roomEast);
    CHECK(unlockNeighbors[1] == roomWest);

    // Now we give an exception to this
    unlockedDoors.clear();
    unlockedDoors.push_back(conNorth->GetID());

    unlockNeighbors = dungeon->GetRoomUnlockedNeighbors(index, unlockedDoors);
    CHECK(unlockNeighbors.size() == 3);
    CHECK(unlockNeighbors[0] == roomNorth);
    CHECK(unlockNeighbors[1] == roomEast);
    CHECK(unlockNeighbors[2] == roomWest);
}

TEST_CASE("procedural/pddungeon.GetLockedDoor", "Test to make sure dungeons can properly get a locked door")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_room_ptr room3 = PDGenerator::CreateRoom();
    pd_room_ptr room4 = PDGenerator::CreateRoom();
    pd_con_ptr con1 = std::make_shared<PDConnection>(room1, room2);
    pd_con_ptr con2 = std::make_shared<PDConnection>(room3, room4);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddRoom(room3, 2);
    dungeon->AddRoom(room4, 3);
    dungeon->AddConnection(con1);
    dungeon->AddConnection(con2);
    dungeon->AddLockedDoor(con1);
    dungeon->AddLockedDoor(con2);

    CHECK(dungeon->GetLockedDoor(0) == con1->GetLockedDoor());
    CHECK(dungeon->GetLockedDoor(1) == con2->GetLockedDoor());
}

TEST_CASE("procedural/pddungeon.GetLockedConnection", "Test to make sure dungeons can properly get a locked connection")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_room_ptr room3 = PDGenerator::CreateRoom();
    pd_room_ptr room4 = PDGenerator::CreateRoom();
    pd_con_ptr con1 = std::make_shared<PDConnection>(room1, room2);
    pd_con_ptr con2 = std::make_shared<PDConnection>(room3, room4);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddRoom(room3, 2);
    dungeon->AddRoom(room4, 3);
    dungeon->AddConnection(con1);
    dungeon->AddConnection(con2);
    dungeon->AddLockedDoor(con1);
    dungeon->AddLockedDoor(con2);

    CHECK(dungeon->GetLockedConnection(0) == con1);
    CHECK(dungeon->GetLockedConnection(1) == con2);
}

TEST_CASE("procedural/pddungeon.GetLockedDoors", "Test to make sure dungeons can properly get all the locked doors")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_room_ptr room3 = PDGenerator::CreateRoom();
    pd_room_ptr room4 = PDGenerator::CreateRoom();
    pd_con_ptr con1 = std::make_shared<PDConnection>(room1, room2);
    pd_con_ptr con2 = std::make_shared<PDConnection>(room3, room4);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddRoom(room3, 2);
    dungeon->AddRoom(room4, 3);
    dungeon->AddConnection(con1);
    dungeon->AddConnection(con2);
    dungeon->AddLockedDoor(con1);
    dungeon->AddLockedDoor(con2);

    pd_door_ptr_list doors = dungeon->GetLockedDoors();

    CHECK(doors.size() == 2);
    CHECK(doors[0] == con1->GetLockedDoor());
    CHECK(doors[1] == con2->GetLockedDoor());
}

TEST_CASE("procedural/pddungeon.GetLockedConnections", "Test to make sure dungeons can properly get all the locked connections")
{
    pd_dung_uptr dungeon = std::make_unique<PDDungeon>(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_room_ptr room3 = PDGenerator::CreateRoom();
    pd_room_ptr room4 = PDGenerator::CreateRoom();
    pd_con_ptr con1 = std::make_shared<PDConnection>(room1, room2);
    pd_con_ptr con2 = std::make_shared<PDConnection>(room3, room4);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddRoom(room3, 2);
    dungeon->AddRoom(room4, 3);
    dungeon->AddConnection(con1);
    dungeon->AddConnection(con2);
    dungeon->AddLockedDoor(con1);
    dungeon->AddLockedDoor(con2);

    pd_con_ptr_list cons = dungeon->GetLockedConnections();

    CHECK(cons.size() == 2);
    CHECK(cons[0] == con1);
    CHECK(cons[1] == con2);
}

TEST_CASE("procedural/pddungeon.GetRoomFlood", "Test to make sure dungeons can properly get a room's flood")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr roomCenter = dungeon->GetRoom(1, 1);
    pd_room_ptr roomNorth  = dungeon->GetRoom(1, 0);
    pd_room_ptr roomEast   = dungeon->GetRoom(2, 1);
    pd_room_ptr roomSouth  = dungeon->GetRoom(1, 2);
    pd_room_ptr roomWest   = dungeon->GetRoom(0, 1);
    pd_room_ptr roomSouthEast  = dungeon->GetRoom(2, 2);
    pd_room_ptr roomSouthSouth = dungeon->GetRoom(1, 3);
    pd_room_ptr roomSouthWest  = dungeon->GetRoom(0, 2);
    pd_room_ptr roomSouthEastEast = dungeon->GetRoom(3, 2);
    pd_room_ptr roomSouthEastEastNorth = dungeon->GetRoom(3, 1);
    pd_room_ptr roomSouthEastEastSouth = dungeon->GetRoom(3, 3);
    int index = dungeon->To1DPos(1, 1);
    
    pd_con_ptr conNorth = PDGenerator::CreateConnection(roomCenter, roomNorth);
    pd_con_ptr conEast  = PDGenerator::CreateConnection(roomCenter, roomEast);
    pd_con_ptr conSouth = PDGenerator::CreateConnection(roomCenter, roomSouth);
    pd_con_ptr conWest  = PDGenerator::CreateConnection(roomCenter, roomWest);
    pd_con_ptr conSouthEast  = PDGenerator::CreateConnection(roomSouth, roomSouthEast);
    pd_con_ptr conSouthSouth = PDGenerator::CreateConnection(roomSouth, roomSouthSouth);
    pd_con_ptr conSouthWest  = PDGenerator::CreateConnection(roomSouth, roomSouthWest);
    pd_con_ptr conSouthEastEast = PDGenerator::CreateConnection(roomSouthEast, roomSouthEastEast);
    pd_con_ptr conSouthEastEastNorth = PDGenerator::CreateConnection(roomSouthEastEast, roomSouthEastEastNorth);
    pd_con_ptr conSouthEastEastSouth = PDGenerator::CreateConnection(roomSouthEastEast, roomSouthEastEastSouth);

    // Create the basic dungeon connection for testing
    dungeon->AddConnection(conNorth);
    dungeon->AddConnection(conEast);
    dungeon->AddConnection(conSouth);
    dungeon->AddConnection(conWest);
    dungeon->AddConnection(conSouthEast);
    dungeon->AddConnection(conSouthSouth);
    dungeon->AddConnection(conSouthWest);
    dungeon->AddConnection(conSouthEastEast);
    dungeon->AddConnection(conSouthEastEastNorth);
    dungeon->AddConnection(conSouthEastEastSouth);

    // Lock this door purposely
    dungeon->AddLockedDoor(conSouthEastEast);

    pd_vec unlockedDoors;
    unlockedDoors.push_back(conSouthEastEast->GetID());

    ///////////////////////// TEST A ROOM //////////////////////////
    pd_room_ptr_list roomFlood = dungeon->GetRoomFlood(roomCenter);

    REQUIRE(roomFlood.size() == 8);
    // Make sure these rooms are in the set
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));

    // Make sure this room IS NOT in the set
    CHECK(!findElement(roomFlood, roomSouthEastEast));

    ///////////////////////// TEST AN INDEX //////////////////////////
    roomFlood = dungeon->GetRoomFlood(index);

    REQUIRE(roomFlood.size() == 8);
    // Make sure these rooms are in the set
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));

    // Make sure this room IS NOT in the set
    CHECK(!findElement(roomFlood, roomSouthEastEast));

    ///////////////////////// TEST A ROOM WITH UNLOCKED DOOR //////////////////////////
    roomFlood = dungeon->GetRoomFlood(roomCenter, unlockedDoors);

    REQUIRE(roomFlood.size() == 11);
    // Make sure these rooms are in the set
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));

    ///////////////////////// TEST AN INDEX WITH UNLOCKED DOOR //////////////////////////
    roomFlood = dungeon->GetRoomFlood(index, unlockedDoors);

    REQUIRE(roomFlood.size() == 11);
    // Make sure these rooms are in the set
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));
}

TEST_CASE("procedural/pddungeon.AddToRoomFlood", "Test to make sure dungeons can properly add new rooms to an existing room flood")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr roomCenter = dungeon->GetRoom(1, 1);
    pd_room_ptr roomNorth = dungeon->GetRoom(1, 0);
    pd_room_ptr roomEast = dungeon->GetRoom(2, 1);
    pd_room_ptr roomSouth = dungeon->GetRoom(1, 2);
    pd_room_ptr roomWest = dungeon->GetRoom(0, 1);
    pd_room_ptr roomSouthEast = dungeon->GetRoom(2, 2);
    pd_room_ptr roomSouthSouth = dungeon->GetRoom(1, 3);
    pd_room_ptr roomSouthWest = dungeon->GetRoom(0, 2);
    pd_room_ptr roomSouthEastEast = dungeon->GetRoom(3, 2);
    pd_room_ptr roomSouthEastEastNorth = dungeon->GetRoom(3, 1);
    pd_room_ptr roomSouthEastEastSouth = dungeon->GetRoom(3, 3);
    pd_room_ptr roomSouthEastEastSouthSouth = dungeon->GetRoom(3, 4);
    int index = dungeon->To1DPos(3, 2);

    pd_con_ptr conNorth = PDGenerator::CreateConnection(roomCenter, roomNorth);
    pd_con_ptr conEast = PDGenerator::CreateConnection(roomCenter, roomEast);
    pd_con_ptr conSouth = PDGenerator::CreateConnection(roomCenter, roomSouth);
    pd_con_ptr conWest = PDGenerator::CreateConnection(roomCenter, roomWest);
    pd_con_ptr conSouthEast = PDGenerator::CreateConnection(roomSouth, roomSouthEast);
    pd_con_ptr conSouthSouth = PDGenerator::CreateConnection(roomSouth, roomSouthSouth);
    pd_con_ptr conSouthWest = PDGenerator::CreateConnection(roomSouth, roomSouthWest);
    pd_con_ptr conSouthEastEast = PDGenerator::CreateConnection(roomSouthEast, roomSouthEastEast);
    pd_con_ptr conSouthEastEastNorth = PDGenerator::CreateConnection(roomSouthEastEast, roomSouthEastEastNorth);
    pd_con_ptr conSouthEastEastSouth = PDGenerator::CreateConnection(roomSouthEastEast, roomSouthEastEastSouth);
    pd_con_ptr conSouthEastEastSouthSouth = PDGenerator::CreateConnection(roomSouthEastEastSouth, roomSouthEastEastSouthSouth);

    // Create the basic dungeon connection for testing
    dungeon->AddConnection(conNorth);
    dungeon->AddConnection(conEast);
    dungeon->AddConnection(conSouth);
    dungeon->AddConnection(conWest);
    dungeon->AddConnection(conSouthEast);
    dungeon->AddConnection(conSouthSouth);
    dungeon->AddConnection(conSouthWest);
    dungeon->AddConnection(conSouthEastEast);
    dungeon->AddConnection(conSouthEastEastNorth);
    dungeon->AddConnection(conSouthEastEastSouth);
    dungeon->AddConnection(conSouthEastEastSouthSouth);

    // Lock this door purposely
    dungeon->AddLockedDoor(conSouthEastEast);
    dungeon->AddLockedDoor(conSouthEastEastSouthSouth);

    pd_vec unlockedDoors;
    unlockedDoors.push_back(conSouthEastEast->GetID());

    ///////////////////////// TEST A ROOM //////////////////////////
    pd_room_ptr_list roomFlood = dungeon->GetRoomFlood(roomCenter);
    CHECK(roomFlood.size() == 8);
    dungeon->AddToRoomFlood(roomFlood, roomSouthEastEast, unlockedDoors);
    CHECK(roomFlood.size() == 11);
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));

    ///////////////////////// TEST AN INDEX //////////////////////////
    roomFlood = dungeon->GetRoomFlood(roomCenter);
    CHECK(roomFlood.size() == 8);
    dungeon->AddToRoomFlood(roomFlood, index, unlockedDoors);
    CHECK(roomFlood.size() == 11);
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));

    ///////////////////////// TEST A ROOM WITH UNLOCKED DOOR //////////////////////////
    unlockedDoors.push_back(conSouthEastEastSouthSouth->GetID());

    roomFlood = dungeon->GetRoomFlood(roomCenter);
    CHECK(roomFlood.size() == 8);
    dungeon->AddToRoomFlood(roomFlood, roomSouthEastEast, unlockedDoors);
    CHECK(roomFlood.size() == 12);
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouthSouth));

    ///////////////////////// TEST AN INDEX WITH UNLOCKED DOOR //////////////////////////
    unlockedDoors.push_back(conSouthEastEastSouthSouth->GetID());

    roomFlood = dungeon->GetRoomFlood(roomCenter);
    CHECK(roomFlood.size() == 8);
    dungeon->AddToRoomFlood(roomFlood, index, unlockedDoors);
    CHECK(roomFlood.size() == 12);
    CHECK(findElement(roomFlood, roomCenter));
    CHECK(findElement(roomFlood, roomNorth));
    CHECK(findElement(roomFlood, roomEast));
    CHECK(findElement(roomFlood, roomSouth));
    CHECK(findElement(roomFlood, roomWest));
    CHECK(findElement(roomFlood, roomSouthEast));
    CHECK(findElement(roomFlood, roomSouthSouth));
    CHECK(findElement(roomFlood, roomSouthWest));
    CHECK(findElement(roomFlood, roomSouthEastEast));
    CHECK(findElement(roomFlood, roomSouthEastEastNorth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouth));
    CHECK(findElement(roomFlood, roomSouthEastEastSouthSouth));
}
}