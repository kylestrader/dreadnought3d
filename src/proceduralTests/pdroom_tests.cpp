#include <3rdParty/CATCH/catch.hpp>
#include <memory>
#include <vector>
#include <procedural/pdroom.h>
#include <procedural/pdkey.h>

namespace PDTests
{
    typedef std::unique_ptr<PDRoom>     pd_room_uptr;
    typedef std::shared_ptr<PDKey>      pd_key_ptr;
    typedef std::vector<pd_key_ptr>     pd_key_ptr_list;

TEST_CASE("procedural/pdroom", "Test to make sure rooms properly generate their grid")
{
    pd_room_uptr room = std::make_unique<PDRoom>(10, 10);

    CHECK(room->GetGridSize() == 100);
    CHECK(room->GetWidth() == 10);
    CHECK(room->GetHeight() == 10);
}

TEST_CASE("procedural/pdroom.GetTag", "Test to make sure rooms properly generate their grid")
{
    pd_room_uptr room = std::make_unique<PDRoom>(10, 10);
    room->SetGridX(5);
    room->SetGridY(1);

    REQUIRE(room->GetTag() == "5,1");
}

TEST_CASE("procedural/pdroom.AddKey", "Test to make sure rooms properly add a key")
{
    pd_room_uptr room = std::make_unique<PDRoom>(10, 10);
    pd_key_ptr key = std::make_shared<PDKey>(0);
    
    REQUIRE(room->GetNumKeys() == 0);
    room->AddKey(key);
    REQUIRE(room->GetNumKeys() == 1);
    CHECK(room->GetKeyAt(0) == key);
}

TEST_CASE("procedural/pdroom.AddKeys", "Test to make sure rooms properly add keys")
{
    pd_room_uptr room = std::make_unique<PDRoom>(10, 10);
    pd_key_ptr key1 = std::make_shared<PDKey>(0);
    pd_key_ptr key2 = std::make_shared<PDKey>(1);
    pd_key_ptr_list keys;

    keys.push_back(key1);
    keys.push_back(key2);

    REQUIRE(room->GetNumKeys() == 0);
    room->AddKeys(keys);
    REQUIRE(room->GetNumKeys() == 2);
}
}