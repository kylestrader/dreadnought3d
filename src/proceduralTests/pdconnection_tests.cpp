#include <3rdParty/CATCH/catch.hpp>
#include <memory>
#include <procedural/pd_generator.h>

namespace PDTests
{
    typedef std::shared_ptr<PDDungeon>         pd_dung_ptr;
    typedef std::shared_ptr<PDRoom>         pd_room_ptr;
    typedef std::shared_ptr<PDConnection>   pd_con_ptr;
    typedef std::shared_ptr<PDLockedDoor>   pd_door_ptr;

TEST_CASE("procedural/pdconnection.IsLocked", "Test to make sure connections properly determine if they are locked")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon();
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    pd_con_ptr con = PDGenerator::CreateConnection(room1, room2);

    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);
    dungeon->AddConnection(con);

    REQUIRE_FALSE(con->IsLocked());
    dungeon->AddLockedDoor(con);
    REQUIRE(con->IsLocked());
}

}