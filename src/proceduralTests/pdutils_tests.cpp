#include <3rdParty/CATCH/catch.hpp>
#include <procedural/pdutils.h>

namespace PDTests
{

TEST_CASE("procedural/pdutils.to1DPosition", "Test to make sure the function returns the proper 1D position given a 2D position")
{
    // I am using a 5x5 grid as an example

    // Top Left
    CHECK(PDUtils::to1DPosition(0, 0, 5) == 0);
    
    // Bottom Right
    CHECK(PDUtils::to1DPosition(4, 4, 5) == 24);

    // Center
    CHECK(PDUtils::to1DPosition(2, 2, 5) == 12);
}

TEST_CASE("procedural/pdutils.to2DPosition", "Test to make sure the function returns the proper 1D position given a 2D position")
{
    // I am using a 5x5 grid as an example
    int gridX = -1;
    int gridY = -1;

    // Top Left
    PDUtils::to2DPosition(0, 5, gridX, gridY);
    CHECK(gridX == 0);
    CHECK(gridY == 0);

    // Bottom Right
    PDUtils::to2DPosition(24, 5, gridX, gridY);
    CHECK(gridX == 4);
    CHECK(gridY == 4);

    // Center
    PDUtils::to2DPosition(12, 5, gridX, gridY);
    CHECK(gridX == 2);
    CHECK(gridY == 2);
}

TEST_CASE("procedural/pdutils.length", "Test to make sure the proper length is returned")
{
    CHECK(PDUtils::length<double>(0.0, 0.0) == Approx(0.0));
    CHECK(PDUtils::length<double>(5.0, 3.0) == Approx(5.831));
    CHECK(PDUtils::length<double>(8.0, 12.0) == Approx(14.422).epsilon(.0001));
}

TEST_CASE("procedural/pdutils.normalize", "Test to make sure the value is being normalized correctly")
{
    // Check the version which has output variables first
    double outX = -1;
    double outY = -1;
    PDUtils::normalize<double>(1.0, 1.0, outX, outY);
    CHECK(outX == Approx(0.707).epsilon(.001));
    CHECK(outY == Approx(0.707).epsilon(.001));

    PDUtils::normalize<double>(8.0, 3.0, outX, outY);
    CHECK(outX == Approx(0.936).epsilon(.001));
    CHECK(outY == Approx(0.351).epsilon(.001));

    PDUtils::normalize<double>(8.253, 15.981, outX, outY);
    CHECK(outX == Approx(0.458).epsilon(.001));
    CHECK(outY == Approx(0.888).epsilon(.001));

    // Check the version which overrides the values second
    outX = 1.0;
    outY = 1.0;
    PDUtils::normalize<double>(outX, outY);
    CHECK(outX == Approx(0.707).epsilon(.001));
    CHECK(outY == Approx(0.707).epsilon(.001));

    outX = 8.0;
    outY = 3.0;
    PDUtils::normalize<double>(outX, outY);
    CHECK(outX == Approx(0.936).epsilon(.001));
    CHECK(outY == Approx(0.351).epsilon(.001));

    outX = 8.253;
    outY = 15.981;
    PDUtils::normalize<double>(outX, outY);
    CHECK(outX == Approx(0.458).epsilon(.001));
    CHECK(outY == Approx(0.888).epsilon(.001));
}

TEST_CASE("procedural/pdutils.clamp", "Test to make sure the values are being clamped properly")
{
    // Test Integers
    CHECK(PDUtils::clamp<int>(-2, 0, 10) == 0);
    CHECK(PDUtils::clamp<int>(23, 0, 10) == 10);
    CHECK(PDUtils::clamp<int>(5, 0, 10) == 5);

    // Test Doubles
    CHECK(PDUtils::clamp<double>(-2.12, 0.53, 10.25) == Approx(0.53));
    CHECK(PDUtils::clamp<double>(23.65, 0, 10.25) == Approx(10.25));
    CHECK(PDUtils::clamp<double>(5.71, 0, 10.25) == Approx(5.71));
}

TEST_CASE("procedural/pdutils.lerp", "Test to make sure the values are being lerped properly")
{
    CHECK(PDUtils::lerp<double>(0.0, 1.0, 0.5) == Approx(0.5));
    CHECK(PDUtils::lerp<double>(-12.32, 432.10, 0.81) == Approx(347.66));
}

TEST_CASE("procedural/pdutils.map", "Test to make sure the values are being mapped properly")
{
    CHECK(PDUtils::map<double>(0.52, 0.0, 1.0, 0.0, 100.0) == Approx(52.0));
    CHECK(PDUtils::map<double>(12.52, -50.0, 50.0, 0.0, 1.0) == Approx(0.625).epsilon(0.001));
}

TEST_CASE("procedural/pdutils.distance", "Test to make sure the distance is being properly calculated")
{
    // Check Integers
    CHECK(PDUtils::distance<int>(0, 0, 100, 100) == 141);
    CHECK(PDUtils::distance<int>(-23, 15, 94, -83) == 152);

    // Check Doubles
    CHECK(PDUtils::distance<double>(0.0, 0.0, 100.0, 100.0) == Approx(141.421));
    CHECK(PDUtils::distance<double>(-23.56, 15.15, 94.48, -83.77) == Approx(154).epsilon(0.0001));
}

TEST_CASE("procedural/pdutils.toRadians", "Test to make sure the conversion from degrees to radians is correct")
{
    // Check Floats
    CHECK(PDUtils::toRadiansf(180.0f) == Approx(3.1415).epsilon(0.001));
    CHECK(PDUtils::toRadiansf(-128.14f) == Approx(-2.236).epsilon(0.001));

    // Check Doubles
    CHECK(PDUtils::toRadians(270.0) == Approx(4.712).epsilon(0.001));
    CHECK(PDUtils::toRadians(-328.37) == Approx(-5.731).epsilon(0.001));
}

TEST_CASE("procedural/pdutils.toDegrees", "Test to make sure the conversion from radians to degrees is correct")
{
    // Check Floats
    CHECK(PDUtils::toDegreesf(1.0f) == Approx(57.295f).epsilon(0.001));
    CHECK(PDUtils::toDegreesf(-5.48f) == Approx(-313.98f).epsilon(0.001));

    // Check Doubles
    CHECK(PDUtils::toDegrees(2.183) == Approx(125.076).epsilon(0.001));
    CHECK(PDUtils::toDegrees(-3.187) == Approx(-182.601).epsilon(0.001));
}
}