#include <3rdParty/CATCH/catch.hpp>
#include <memory>
#include <procedural/pddungeon.h>
#include <procedural/pdgenerator.h>
#include <procedural/pdconnection.h>
#include <procedural/pdroom.h>

namespace PDTests
{
    typedef std::shared_ptr<PDDungeon>      pd_dung_ptr;
    typedef std::shared_ptr<PDRoom>         pd_room_ptr;
    typedef std::shared_ptr<PDConnection>   pd_con_ptr;

TEST_CASE("procedural/pdgenerator.CreateRoom", "Test to make sure the generator properly creates rooms")
{
    PDGenerator::SetMinRoomSize(4);
    CHECK(PDGenerator::GetMinRoomSize() == 4);

    PDGenerator::SetMaxRoomSize(8);
    CHECK(PDGenerator::GetMaxRoomSize() == 8);

    // Check the default parameters (which should make it default to the min room size)
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    REQUIRE(room1->GetWidth() == 4);
    REQUIRE(room1->GetHeight() == 4);

    // Check to make sure it doesn't go above the max room size
    pd_room_ptr room2 = PDGenerator::CreateRoom(10, 10);
    REQUIRE(room2->GetWidth() == 8);
    REQUIRE(room2->GetHeight() == 8);

    // Check to make sure that if it is within the min and max, it stays the same
    pd_room_ptr room3 = PDGenerator::CreateRoom(5, 6);
    REQUIRE(room3->GetWidth() == 5);
    REQUIRE(room3->GetHeight() == 6);

    // Check to make sure min works
    pd_room_ptr room4 = PDGenerator::CreateRoom(0, 4, 0, 4);
    REQUIRE(room4->GetWidth() == 4);
    REQUIRE(room4->GetHeight() == 4);

    // Check to make sure max works
    pd_room_ptr room5 = PDGenerator::CreateRoom(10, 8, 10, 8);
    REQUIRE(room5->GetWidth() == 8);
    REQUIRE(room5->GetHeight() == 8);
}

TEST_CASE("procedural/pdgenerator.CreateDungeon", "Test to make sure the generator properly creates dungeons")
{
    PDGenerator::SetMinNumRooms(5);
    CHECK(PDGenerator::GetMinNumRooms() == 5);

    PDGenerator::SetMaxNumRooms(10);
    CHECK(PDGenerator::GetMaxNumRooms() == 10);

    //-----------------------------------------------------------
    // 2 PARAMS
    
    // Check to make sure it defaults it to the minimum
    pd_dung_ptr dungeon1_2 = PDGenerator::CreateDungeon(-1, -1);
    REQUIRE(dungeon1_2->GetSize() == 25);

    // Check to make sure it defaults it to the minimum
    pd_dung_ptr dungeon2_2 = PDGenerator::CreateDungeon();
    REQUIRE(dungeon2_2->GetSize() == 25);

    // Check to make sure it defaults it to the maximum
    pd_dung_ptr dungeon3_2 = PDGenerator::CreateDungeon(12, 12);
    REQUIRE(dungeon3_2->GetSize() == 100);

    //----------------------------------------------------------- 
    // 8 PARAMS
    
    // Check to make sure it defaults it to the minimum
    pd_dung_ptr dungeon1_8 = PDGenerator::CreateDungeon(-1, -1, -1, -1, -1, -1, -1, -1);
    REQUIRE(dungeon1_8->GetSize() == 25);

    // Check to make sure it defaults it to the maximum
    pd_dung_ptr dungeon2_8 = PDGenerator::CreateDungeon(12, 12, 12, 12, 12, 12, 12, 12);
    REQUIRE(dungeon2_8->GetSize() == 100);
}

TEST_CASE("procedural/pdgenerator.CreateConnection", "Test to make sure the generator properly creates connections")
{
    pd_dung_ptr dungeon = PDGenerator::CreateDungeon(5, 5);
    pd_room_ptr room1 = PDGenerator::CreateRoom();
    pd_room_ptr room2 = PDGenerator::CreateRoom();
    dungeon->AddRoom(room1, 0);
    dungeon->AddRoom(room2, 1);

    pd_con_ptr con = PDGenerator::CreateConnection(room1, room2);
    REQUIRE(con->GetRoom1() == room1);
    REQUIRE(con->GetRoom2() == room2);
}

}