#include "pddungeon.h"
#include <cassert>
#include <string>
#include "pdutils.h"
#include "pdroom.h"
#include "pdconnection.h"
#include "pdlockeddoor.h"

PDDungeon::PDDungeon(int width, int height)
{
    m_Width = width;
    m_Height = height;
    m_StartX = -1;
    m_StartY = -1;
    m_NumRoomsAdded = 0;

    m_Rooms.reserve(GetSize());

    for (int i = 0; i < GetSize(); i++)
    {
        m_Rooms.push_back(nullptr);
    }

    // Calculates the maximum number of connections a dungeon is able to have
    m_MaxConnections = 2 * ((width - 1) * (height - 1)) + ((width - 1) + (height - 1));
}

PDDungeon::~PDDungeon()
{

}

int PDDungeon::AddConnection(pd_con_ptr con)
{
    int index = m_Connections.size();
    
    con->m_ID = index;
    m_Connections.push_back(con);

    // Store the index in the lookup table for easy and fast lookup later
    // We store it in both room's keys to make it so either room can find each other
    storeIndex(con->GetRoom1()->GetTag(), index);
    storeIndex(con->GetRoom2()->GetTag(), index);

	return index;
}

PDDungeon::pd_vec PDDungeon::AddConnections(pd_con_ptr_list cons)
{
	pd_vec indexes;

    for (unsigned int i = 0; i < cons.size(); i++)
    {
        indexes.push_back(AddConnection(cons[i]));
    }

	return indexes;
}

void PDDungeon::AddRoom(pd_room_ptr room, int index)
{
    int gridX = 0;
    int gridY = 0;

    To2DPos(index, gridX, gridY);

    room->SetGridX(gridX).SetGridY(gridY);
    room->SetIndex(index);

    m_Rooms[index] = room;
    m_NumRoomsAdded++;
}

void PDDungeon::AddRoom(pd_room_ptr room, int gridX, int gridY)
{
    AddRoom(room, To1DPos(gridX, gridY));
}

void PDDungeon::AddRooms(pd_room_ptr_list rooms, pd_vec indexes)
{
    assert(rooms.size() == indexes.size());

    for (unsigned int i = 0; i < rooms.size(); i++)
    {
        AddRoom(rooms[i], indexes[i]);
    }
}

void PDDungeon::AddRooms(pd_room_ptr_list rooms, std::vector<std::pair<int, int>> gridPositions)
{
    assert(rooms.size() == gridPositions.size());

    for (unsigned int i = 0; i < rooms.size(); i++)
    {
        AddRoom(rooms[i], gridPositions[i].first, gridPositions[i].second);
    }
}

int PDDungeon::AddLockedDoor(pd_con_ptr con)
{
    con->m_Door = std::make_shared<PDLockedDoor>(m_LockedDoorsLookup.size());

    // Find the index of the connection so we can store it in the lookup for locks
    int index = con->GetID();

    assert(index != -1); // If we can't find it, the connection doesn't exist in the dungeon
    m_LockedDoorsLookup.push_back(index);

    return m_LockedDoorsLookup.size() - 1;
}

void PDDungeon::SetStartRoom(int gridX, int gridY)
{
    SetStartRoom(GetRoom(gridX, gridY));
}

void PDDungeon::SetStartRoom(pd_room_ptr room)
{
    if (m_StartX != -1 && m_StartY != -1)
    {
        GetRoom(m_StartX, m_StartY)->SetIsStart(false);
    }

    m_StartX = room->GetGridX();
    m_StartY = room->GetGridY();
    room->SetIsStart(true);
}

void PDDungeon::GetStartRoom(int& gridX, int& gridY) const
{
    gridX = m_StartX;
    gridY = m_StartY;
}

PDDungeon::pd_room_ptr PDDungeon::GetStartRoom() const
{
    return GetRoom(m_StartX, m_StartY);
}

int PDDungeon::GetWidth() const
{
    return m_Width;
}

int PDDungeon::GetHeight() const
{
    return m_Height;
}

int PDDungeon::GetSize() const
{
	return m_Width * m_Height;
}

int PDDungeon::GetMaxConnections() const
{
    return m_MaxConnections;
}

int PDDungeon::GetMaxLockedDoors() const
{
    return m_Connections.size();
}

int PDDungeon::GetNumRoomsAdded() const
{
    return m_NumRoomsAdded;
}

int PDDungeon::GetNumConnections() const
{
    return m_Connections.size();
}

int PDDungeon::GetNumLockedDoors() const
{
    return m_LockedDoorsLookup.size();
}

const PDDungeon::pd_room_ptr_list& PDDungeon::GetRooms() const
{
    return m_Rooms;
}

PDDungeon::pd_room_ptr PDDungeon::GetRoom(int gridX, int gridY) const
{
    return GetRoom(To1DPos(gridX, gridY));
}

PDDungeon::pd_room_ptr PDDungeon::GetRoom(int index) const
{
    assert(index >= 0 && index < (int)m_Rooms.size());
    return m_Rooms[index];
}

const PDDungeon::pd_con_ptr_list& PDDungeon::GetConnections() const
{
    return m_Connections;
}

PDDungeon::pd_door_ptr_list PDDungeon::GetLockedDoors() const
{
    pd_door_ptr_list doors;

    for (int i = 0; i < GetNumLockedDoors(); i++)
    {
        doors.push_back(GetLockedDoor(i));
    }

    return doors;
}

PDDungeon::pd_con_ptr_list PDDungeon::GetLockedConnections() const
{
    pd_con_ptr_list cons;

    for (int i = 0; i < GetNumLockedDoors(); i++)
    {
        cons.push_back(GetLockedConnection(i));
    }

    return cons;
}

PDDungeon::pd_uset PDDungeon::GetConnections(pd_room_ptr room) const
{
    return findConnections(room);
}

PDDungeon::pd_con_ptr PDDungeon::GetConnection(int index) const
{
    assert(index >= 0 && index < (int)m_Connections.size());
    return m_Connections[index];
}

PDDungeon::pd_con_ptr PDDungeon::GetConnection(pd_room_ptr room1, pd_room_ptr room2) const
{
    pd_uset indexes = findConnections(room1);
    pd_uset::iterator it = indexes.begin();

    for (; it != indexes.end(); it++)
    {
        pd_con_ptr con = GetConnection(*it);
        
        // Overloaded operator on connection to make it easier to see if a room is part of a connection
        if (con == room1 && con == room2)
            return con;
    }

    return nullptr;
}

PDDungeon::pd_door_ptr PDDungeon::GetLockedDoor(int index) const
{
    assert(index >= 0 && index < (int)m_LockedDoorsLookup.size());
    return m_Connections[m_LockedDoorsLookup[index]]->GetLockedDoor();
}

PDDungeon::pd_con_ptr PDDungeon::GetLockedConnection(int index) const
{
    assert(index >= 0 && index < (int)m_LockedDoorsLookup.size());
    return m_Connections[m_LockedDoorsLookup[index]];
}

PDDungeon::pd_uset PDDungeon::findConnections(pd_room_ptr room) const
{
    pd_con_lookup::const_iterator it = m_ConnectionLookup.find(room->GetTag());

    if (it != m_ConnectionLookup.end())
        return it->second;
    else
        return pd_uset();
}

void PDDungeon::storeIndex(std::string key, int index)
{
    m_ConnectionLookup[key].insert(index);
}

int PDDungeon::To1DPos(int gridX, int gridY) const
{
    return PDUtils::to1DPosition(gridX, gridY, m_Width);
}

void PDDungeon::To2DPos(int index, int& gridX, int& gridY) const
{
    PDUtils::to2DPosition(index, m_Width, gridX, gridY);
}

bool PDDungeon::IsValidIndex(int index) const
{
    if (index >= 0 && (unsigned int)index < m_Rooms.size())
        return true;

    return false;
}

bool PDDungeon::IsValid(int gridX, int gridY) const
{
	if (gridX >= 0 &&
		gridX < m_Width &&
		gridY >= 0 &&
		gridY < m_Height)
	{
		return IsValidIndex(To1DPos(gridX, gridY));
	}


	return false;
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomNeighbors(int index) const
{
	return getNeighbors(GetRoom(index), true);
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomNeighbors(pd_room_ptr room) const
{
	return getNeighbors(room, true);
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomUnlockedNeighbors(pd_room_ptr room, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
	if (unlockedDoors.size() > 0)
		return getNeighbors(room, false, true, unlockedDoors);
	else
		return getNeighbors(room, false);
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomUnlockedNeighbors(int index, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
	if (unlockedDoors.size() > 0)
		return getNeighbors(GetRoom(index), false, true, unlockedDoors);
	else
		return getNeighbors(GetRoom(index), false);
}

PDDungeon::pd_room_ptr_list PDDungeon::getNeighbors(pd_room_ptr room, bool allUnlocked, bool useVec /* = false */, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
	pd_room_ptr_list neighbors;
	int index = -1;

	// Check north
	index = To1DPos(room->GetGridX(), room->GetGridY() - 1);
	if (IsValid(room->GetGridX(), room->GetGridY() - 1))
	{
		pd_room_ptr room2 = GetRoom(index);
		bool isLocked = checkIfLocked(room, room2, allUnlocked, useVec, unlockedDoors);

		if (!isLocked)
			neighbors.push_back(room2);
	}

	// Check east
	index = To1DPos(room->GetGridX() + 1, room->GetGridY());
	if (IsValid(room->GetGridX() + 1, room->GetGridY()))
	{
		pd_room_ptr room2 = GetRoom(index);
		bool isLocked = checkIfLocked(room, room2, allUnlocked, useVec, unlockedDoors);

		if (!isLocked)
			neighbors.push_back(room2);
	}

	// Check south
	index = To1DPos(room->GetGridX(), room->GetGridY() + 1);
	if (IsValid(room->GetGridX(), room->GetGridY() + 1))
	{
		pd_room_ptr room2 = GetRoom(index);
		bool isLocked = checkIfLocked(room, room2, allUnlocked, useVec, unlockedDoors);

		if (!isLocked)
			neighbors.push_back(room2);
	}

	// Check west
	index = To1DPos(room->GetGridX() - 1, room->GetGridY());
	if (IsValid(room->GetGridX() - 1, room->GetGridY()))
	{
		pd_room_ptr room2 = GetRoom(index);
		bool isLocked = checkIfLocked(room, room2, allUnlocked, useVec, unlockedDoors);

		if (!isLocked)
			neighbors.push_back(room2);
	}

	return neighbors;
}

bool PDDungeon::checkIfLocked(pd_room_ptr room1, pd_room_ptr room2, bool allUnlocked, bool useVec, const pd_vec& unlockedDoors) const
{
	if (!allUnlocked)
	{
		pd_con_ptr con = GetConnection(room1, room2);

        if (!con)
            return true;

		if (useVec && con->IsLocked())
		{
			pd_vec::const_iterator iter = unlockedDoors.begin();
			for (; iter != unlockedDoors.end(); iter++)
			{
				if (*iter == con->GetID())
				{
					return false;
				}
			}
		}
		else if (!con->IsLocked())
			return false;

	}
	else
		return false;

	return true;
}

PDDungeon::pd_vec PDDungeon::GetRoomNeighborsIndexes(int index) const
{
    return GetRoomNeighborsIndexes(GetRoom(index));
}

PDDungeon::pd_vec PDDungeon::GetRoomNeighborsIndexes(pd_room_ptr room) const
{
    pd_room_ptr_list rooms = GetRoomNeighbors(room);
    pd_vec indexes;

    for (unsigned int i = 0; i < rooms.size(); i++)
    {
        indexes.push_back(To1DPos(rooms[i]->GetGridX(), rooms[i]->GetGridY()));
    }

    return indexes;
}

bool PDDungeon::HasConnection(pd_room_ptr room1, pd_room_ptr room2) const
{
    pd_con_ptr con = GetConnection(room1, room2);
    
    return (con == nullptr) ? false : true;
}

std::string PDDungeon::ToString() const
{
    std::string str = "{";
    str += "\"name\":\"Dungeon\",";
    str += "\"width\":\"" + std::to_string(m_Width) + "\",";
    str += "\"height\":\"" + std::to_string(m_Height) + "\",";
    str += "\"start_x\":\"" + std::to_string(m_StartX) + "\",";
    str += "\"start_y\":\"" + std::to_string(m_StartY) + "\",";
    str += "\"num_rooms\":\"" + std::to_string(m_Rooms.size()) + "\",";
    str += "\"rooms\":[";

    for (unsigned int i = 0; i < m_Rooms.size(); i++)
    {
        str += m_Rooms[i]->ToString();

        if (i + 1 < m_Rooms.size())
            str += ",";
    }
    
    str += "],";
    str += "\"num_connections\":\"" + std::to_string(m_Connections.size()) + "\",";
    str += "\"num_locked_doors\":\"" + std::to_string(m_LockedDoorsLookup.size()) + "\",";
    str += "\"connections\":[";
    
    for (unsigned int i = 0; i < m_Connections.size(); i++)
    {
        str += m_Connections[i]->ToString();

        if (i + 1 < m_Connections.size())
            str += ",";
    }

    str += "]}";

    return str;
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomFlood(pd_room_ptr room, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
    pd_room_ptr_list roomFlood;
    pd_room_ptr_list roomPool;

    roomFlood.push_back(room);

    for (unsigned int i = 0; i < roomFlood.size(); i++)
    {
        roomPool = GetRoomUnlockedNeighbors(roomFlood[i], unlockedDoors);
        vecToSet(roomPool, roomFlood);
    }

    return roomFlood;
}

PDDungeon::pd_room_ptr_list PDDungeon::GetRoomFlood(int index, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
    return GetRoomFlood(GetRoom(index), unlockedDoors);
}

void PDDungeon::AddToRoomFlood(pd_room_ptr_list& flood, int index, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
    AddToRoomFlood(flood, GetRoom(index), unlockedDoors);
}

void PDDungeon::AddToRoomFlood(pd_room_ptr_list& flood, pd_room_ptr room, const pd_vec& unlockedDoors /* = pd_vec() */) const
{
    // If the room is already in the flood, back out because it should be new
    if (!insertUnique(flood, room))
        return;

    int num = 1;
    int startIndex = flood.size() - 1;
    pd_room_ptr_list roomPool;

    for (int i = 0; i < num; i++)
    {
        roomPool = GetRoomUnlockedNeighbors(flood[startIndex + i], unlockedDoors);
        num += vecToSet(roomPool, flood);
    }
}