#pragma once

#include <vector>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <set>

class PDRoom;
class PDConnection;
class PDLockedDoor;

class PDDungeon
{
public:
    typedef PDDungeon                                   this_type;
    typedef std::shared_ptr<PDConnection>               pd_con_ptr;
    typedef std::shared_ptr<PDRoom>                     pd_room_ptr;
    typedef std::unordered_set<int>                     pd_uset;
    typedef std::vector<int>                            pd_vec;
    typedef std::vector<pd_room_ptr>                    pd_room_ptr_list;
    typedef std::set<pd_room_ptr>                       pd_room_ptr_set;
    typedef std::vector<pd_con_ptr>                     pd_con_ptr_list;
    typedef std::unordered_map<std::string, pd_uset>    pd_con_lookup;
    typedef std::shared_ptr<PDLockedDoor>               pd_door_ptr;
    typedef std::vector<pd_door_ptr>                    pd_door_ptr_list;

private:
    pd_room_ptr_list m_Rooms;
    pd_con_ptr_list m_Connections;

    // Contains a reference (the index in m_Connections) to the connection this door is attached to
    pd_vec m_LockedDoorsLookup;

    // This lookup speeds up the retrieval of any particular connection (or connections for a room) from
    // an average of O(n) where n is the number of connections stored to an average of O(4). Worst case is
    // still O(n) but this significantly increases the average speed.
    pd_con_lookup m_ConnectionLookup;
    
    int m_Width;
    int m_Height;
    int m_NumRoomsAdded;
    int m_MaxConnections;

    int m_StartX;
    int m_StartY;

public:
    PDDungeon(int width, int height);
    ~PDDungeon();

    int AddConnection(pd_con_ptr con);
    pd_vec AddConnections(pd_con_ptr_list cons);

    void AddRoom(pd_room_ptr room, int index);
    void AddRoom(pd_room_ptr room, int gridX, int gridY);
    void AddRooms(pd_room_ptr_list rooms, pd_vec indexes);
    void AddRooms(pd_room_ptr_list rooms, std::vector<std::pair<int, int>> gridPositions);

    int AddLockedDoor(pd_con_ptr con);

    void SetStartRoom(int gridX, int gridY);
    void SetStartRoom(pd_room_ptr room);

    void GetStartRoom(int& gridX, int& gridY) const;
    pd_room_ptr GetStartRoom() const;

    // Helper functions that wrap the utils class version so they don't have to
    // know or get the size of the dungeon for conversion
    int To1DPos(int gridX, int gridY) const;
    void To2DPos(int index, int& gridX, int& gridY) const;

    bool IsValidIndex(int index) const;
	bool IsValid(int gridX, int gridY) const;

    bool HasConnection(pd_room_ptr room1, pd_room_ptr room2) const;
    
    pd_room_ptr_list GetRoomNeighbors(int index) const;
    pd_room_ptr_list GetRoomNeighbors(pd_room_ptr room) const;

	// Gets the neighbors which have a connection that is unlocked (because there is no
	// locked door). You can also specify a list of doors which are unlocked and these
	// will be regarded as connections which are unlocked
    pd_room_ptr_list GetRoomUnlockedNeighbors(int index, const pd_vec& unlockedDoors = pd_vec()) const;
    pd_room_ptr_list GetRoomUnlockedNeighbors(pd_room_ptr room, const pd_vec& unlockedDoors = pd_vec()) const;

    pd_vec GetRoomNeighborsIndexes(int index) const;
    pd_vec GetRoomNeighborsIndexes(pd_room_ptr room) const;

    int GetWidth() const;
    int GetHeight() const;
    int GetSize() const;
    int GetMaxConnections() const;
    int GetMaxLockedDoors() const;

    const pd_room_ptr_list& GetRooms() const;
    const pd_con_ptr_list& GetConnections() const;
    pd_door_ptr_list GetLockedDoors() const;
    pd_con_ptr_list GetLockedConnections() const;

    int GetNumRoomsAdded() const;
    int GetNumConnections() const;
    int GetNumLockedDoors() const;

    pd_room_ptr GetRoom(int gridX, int gridY) const;
    pd_room_ptr GetRoom(int index) const;

    pd_uset GetConnections(pd_room_ptr room) const;
    pd_con_ptr GetConnection(int index) const;
    pd_con_ptr GetConnection(pd_room_ptr room1, pd_room_ptr room2) const;

    pd_door_ptr GetLockedDoor(int index) const;
    pd_con_ptr GetLockedConnection(int index) const; // Index is the index in m_LockedDoorsLookup, not m_Connections

    pd_room_ptr_list GetRoomFlood(pd_room_ptr room, const pd_vec& unlockedDoors = pd_vec()) const;
    pd_room_ptr_list GetRoomFlood(int index, const pd_vec& unlockedDoors = pd_vec()) const;
    void AddToRoomFlood(pd_room_ptr_list& flood, pd_room_ptr room, const pd_vec& unlockedDoors = pd_vec()) const;
    void AddToRoomFlood(pd_room_ptr_list& flood, int index, const pd_vec& unlockedDoors = pd_vec()) const;

    std::string ToString() const;

private:
    pd_uset findConnections(pd_room_ptr room) const;
    void storeIndex(std::string key, int index);
	pd_room_ptr_list getNeighbors(pd_room_ptr room, bool allUnlocked, bool useVec = false, const pd_vec& unlockedDoors = pd_vec()) const;
	bool checkIfLocked(pd_room_ptr room1, pd_room_ptr room2, bool allUnlocked, bool useVec, const pd_vec& unlockedDoors) const;
    
    template<typename T>
    int vecToSet(const std::vector<T>& source, std::vector<T>& dest) const
    {
        int numAdded = 0;
        std::vector<T>::const_iterator it = source.begin();
        for (; it != source.end(); it++)
        {
            if (insertUnique(dest, *it))
                numAdded++;
        }

        return numAdded;
    }

    template<typename T>
    bool insertUnique(std::vector<T>& dest, T value) const
    {
        bool found = false;
        std::vector<T>::const_iterator it = dest.begin();
        for (; it != dest.end(); it++)
        {
            if (*it == value)
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            dest.push_back(value);
            return true;
        }

        return false;
    }
};