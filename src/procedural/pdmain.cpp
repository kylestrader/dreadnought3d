#include <iostream>
#include <ctime>
#include <procedural/pdutils.h>
#include <procedural/pdgenerator.h>
#include <3rdParty/SDL2/SDL.h>

// Entry point for procedural dungeon generation test bed
int main()
{
    srand((unsigned int)time(NULL));

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_Window *win = SDL_CreateWindow("PDTestEnv", 0, 0, 640, 480, SDL_WINDOW_SHOWN);
    if (win == nullptr){
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    SDL_Quit();
    
    system("pause");
    return 0;
}