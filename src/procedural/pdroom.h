#pragma once

#include <vector>
#include <memory>

class PDKey;

class PDRoom
{
public:
    typedef PDRoom                      this_type;
    typedef std::vector<int>            pd_vec;
    typedef std::vector<pd_vec>         pd_grid;
    typedef std::shared_ptr<PDKey>      pd_key_ptr;
    typedef std::vector<pd_key_ptr>     pd_key_ptr_list;

private:
    pd_grid m_Grid;
    int m_Width;
    int m_Height;
    float m_X;
    float m_Y;
    int m_GridX;
    int m_GridY;
    int m_Index;
    bool m_IsStart;
    pd_key_ptr_list m_Keys;

private:
    void createGrid(int width, int height);

public:
    PDRoom(int width, int height);
    ~PDRoom();

    float GetX() const;
    float GetY() const;
    int GetGridX() const;
    int GetGridY() const;
    int GetGridSize() const;
    int GetWidth() const;
    int GetHeight() const;
    int GetIndex() const;
    bool GetIsStart() const;
    std::string GetTag() const;

    this_type& SetX(float value);
    this_type& SetY(float value);
    this_type& SetGridX(int value);
    this_type& SetGridY(int value);
    this_type& SetIndex(int value);
    this_type& SetIsStart(bool value);

    std::string ToString() const;

    void AddKey(pd_key_ptr key);
    void AddKeys(pd_key_ptr_list keys);

    bool HasKey(int ID) const;

    const pd_key_ptr_list& GetKeys() const;
    pd_key_ptr GetKey(int ID) const; // Returns the key with this id (if the room has it)
    pd_key_ptr GetKeyAt(int index) const; // Returns the key at this index in the list
    int GetNumKeys() const;

    friend bool operator==(const std::shared_ptr<PDRoom>& lhs, const std::shared_ptr<PDRoom>& rhs);
    friend bool operator==(const PDRoom& lhs, const PDRoom& rhs);
};