#include "pdgenerator.h"
#include "pdroom.h"
#include "pddungeon.h"
#include "pdutils.h"
#include <set>
#include <stack>
#include "pdconnection.h"
#include "pdlockeddoor.h"
#include "pdkey.h"
#include <ctime>

// Just some random values...
int PDGenerator::m_MinRoomSize = 4;
int PDGenerator::m_MaxRoomSize = 12;
int PDGenerator::m_MinNumRooms = 10;
int PDGenerator::m_MaxNumRooms = 100;

PDGenerator::PDGenerator()
{

}

PDGenerator::~PDGenerator()
{

}

PDGenerator::pd_room_ptr PDGenerator::CreateRoom(int width /* = 0 */, int height /* = 0 */)
{
    return CreateRoom(width, width, height, height);
}

PDGenerator::pd_room_ptr PDGenerator::CreateRoom(int minWidth, int width, int minHeight, int height)
{
    if (width <= 0 || width > m_MaxRoomSize) width = PDUtils::clamp(width, m_MinRoomSize, m_MaxRoomSize);
    if (height <= 0 || height > m_MaxRoomSize) height = PDUtils::clamp(height, m_MinRoomSize, m_MaxRoomSize);
    if (minWidth <= 0 || minWidth > m_MaxRoomSize) minWidth = PDUtils::clamp(minWidth, m_MinRoomSize, m_MaxRoomSize);
    if (minHeight <= 0 || minHeight > m_MaxRoomSize) minHeight = PDUtils::clamp(minHeight, m_MinRoomSize, m_MaxRoomSize);
    
    if (minWidth != width)
        width = PDUtils::generateNumber(minWidth, width + 1);
    if (minHeight != height)
        height = PDUtils::generateNumber(minHeight, height + 1);

    pd_room_ptr room = std::make_shared<PDRoom>(width, height);
    return room;
}

PDGenerator::pd_con_ptr PDGenerator::CreateConnection(pd_room_ptr room1, pd_room_ptr room2)
{
    return std::make_shared<PDConnection>(room1, room2);
}

PDGenerator::pd_dung_ptr PDGenerator::CreateDungeon(int width /* = 0 */, int height /* = 0 */)
{
    return CreateDungeon(width, width, height, height, 0, 0, 0, 0);
}

PDGenerator::pd_dung_ptr PDGenerator::CreateDungeon(int width, int height, int roomWidth, int roomHeight)
{
    return CreateDungeon(width, width, height, height, roomWidth, roomWidth, roomHeight, roomHeight);
}

PDGenerator::pd_dung_ptr PDGenerator::CreateDungeon(int width, int height, int minRoomWidth, int roomWidth, int minRoomHeight, int roomHeight)
{
    return CreateDungeon(width, width, height, height, minRoomWidth, roomWidth, minRoomHeight, roomHeight);
}

PDGenerator::pd_dung_ptr PDGenerator::CreateDungeon(int minWidth, int width, int minHeight, int height, int minRoomWidth, int roomWidth, int minRoomHeight, int roomHeight)
{
    if (width <= 0 || width > m_MaxNumRooms) width = PDUtils::clamp(width, m_MinNumRooms, m_MaxNumRooms);
    if (height <= 0 || height > m_MaxNumRooms) height = PDUtils::clamp(height, m_MinNumRooms, m_MaxNumRooms);
    if (minWidth <= 0 || minWidth > m_MaxNumRooms) minWidth = PDUtils::clamp(minWidth, m_MinNumRooms, m_MaxNumRooms);
    if (minHeight <= 0 || minHeight > m_MaxNumRooms) minHeight = PDUtils::clamp(minHeight, m_MinNumRooms, m_MaxNumRooms);

    if (minWidth != width)
        width = PDUtils::generateNumber(minWidth, width + 1);
    if (minHeight != height)
        height = PDUtils::generateNumber(minHeight, height + 1);

    pd_dung_ptr dungeon = std::make_shared<PDDungeon>(width, height);

    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            dungeon->AddRoom(PDGenerator::CreateRoom(minRoomWidth, roomWidth, minRoomHeight, roomHeight), i, j);
        }
    }

    return dungeon;
}

PDGenerator::pd_key_ptr PDGenerator::CreateKey(int id)
{
    return std::make_shared<PDKey>(id);
}

void PDGenerator::ConnectDungeon(pd_dung_ptr dungeon, bool addRedundantCons /* = true */)
{
    typedef std::vector<int>            pd_vec_i;
    typedef std::vector<bool>           pd_vec_b;
    typedef std::stack<pd_point>        pd_stack;
    typedef std::vector<pd_room_ptr>    pd_room_ptr_list;

    // 1. Pick a start location, mark as visited, push on stack
    // 2. Pick a random direction (NOT PREVIOUS)
    // 3. If room in direction not visited
    //      - Visit
    //      - Push on stack
    //      - Create connection
    //      - Go to Step 2
    //    Else
    //      If no unvisited neighbors available
    //          - Pop stack 
    //          - Go to Step 2
    //      Else
    //          - Pick an unvisited neighbor
    //          - Go to Step 2

    pd_vec_b visited;
    pd_stack stack;
    pd_point startPos;
    pd_point curPos;
    pd_point nextPos(-1, -1);
    pd_point prevPos(-1, -1);
    bool pickDir = true;
    int roomIndex = -1;

    // Get the boundaries to pick a random position within it
    int width = dungeon->GetWidth();
    int height = dungeon->GetHeight();

    // Setup the visited lookup table
    for (int i = 0; i < dungeon->GetSize(); i++)
    {
        visited.push_back(false);
    }

    // 1. Pick a start location
    startPos.first = PDUtils::generateNumber<int>(0, width);
    startPos.second = PDUtils::generateNumber<int>(0, height);
    curPos = startPos;
    dungeon->SetStartRoom(startPos.first, startPos.second);

    // Mark as visited, push on stack
    visited[dungeon->To1DPos(startPos.first, startPos.second)] = true;
    stack.push(pd_point(startPos.first, startPos.second));

    while (stack.size() > 0)
    {
        if (pickDir)
        {
            pd_room_ptr_list neighbors = dungeon->GetRoomNeighbors(dungeon->GetRoom(curPos.first, curPos.second));
         
            // 2. Pick a random direction (NOT PREVIOUS)
            do
            {   
                int nextIndex = PDUtils::generateNumber<int>(0, neighbors.size());

                nextPos.first = neighbors[nextIndex]->GetGridX();
                nextPos.second = neighbors[nextIndex]->GetGridY();
            } while (nextPos == prevPos);
        }
        else
        {
            pickDir = true;
        }

        roomIndex = dungeon->To1DPos(nextPos.first, nextPos.second);

        if (!dungeon->IsValidIndex(roomIndex))
            continue;

        // 3. If room in direction not visited
        if (!visited[roomIndex])
        {
            visited[roomIndex] = true;
            stack.push(pd_point(nextPos.first, nextPos.second));
            pd_con_ptr con = CreateConnection(dungeon->GetRoom(curPos.first, curPos.second), dungeon->GetRoom(roomIndex));
            dungeon->AddConnection(con);
        }
        else
        {
            if (addRedundantCons)
            {
                // If the dungeon doesn't have a connection between the room we selected (which was already visited) and our current position
                // we will add a connection anyways
                if (!dungeon->HasConnection(dungeon->GetRoom(curPos.first, curPos.second), dungeon->GetRoom(roomIndex)))
                {
                    pd_con_ptr con = CreateConnection(dungeon->GetRoom(curPos.first, curPos.second), dungeon->GetRoom(roomIndex));
                    dungeon->AddConnection(con);
                }
            }

            pd_vec_i neighbors = dungeon->GetRoomNeighborsIndexes(dungeon->GetRoom(curPos.first, curPos.second));

            // Remove visited neighbors to make the list just contain unvisited neighbors
            for (unsigned int i = 0; i < neighbors.size(); i++)
            {
                if (visited[neighbors[i]])
                {
                    neighbors.erase(neighbors.begin() + i);
                    i--;
                }
            }

            // If no unvisited neighbors available, pop stack and return to top
            if (neighbors.size() <= 0)
            {
                stack.pop();

                if (stack.size() <= 0)
                    continue;

                nextPos = stack.top();
                pickDir = false;
            }
            else // Otherwise pick a random neighbor and go there next
            {
                int nextIndex = PDUtils::generateNumber<int>(0, neighbors.size());
                pd_room_ptr room = dungeon->GetRoom(neighbors[nextIndex]);

                nextPos.first = room->GetGridX();
                nextPos.second = room->GetGridY();

                pickDir = false;
                continue;
            }
        }

        // Update positions
        prevPos = curPos;
        curPos = nextPos;
    }
}

void PDGenerator::SeedDungeonConnections(pd_dung_ptr dungeon, float percent, bool enforce /* = false */)
{
    typedef std::vector<int>            pd_vec;
    typedef std::vector<pd_room_ptr>    pd_room_ptr_list;

    // While there are still connections to add
    //      - Pick a random room
    //      - Pick a random neighbor
    //      If connection exists
    //          If enforce
    //              - Continue
    //          Else
    //              - Decrement connections to add
    //              - Continue
    //      Else
    //          - Create connection
    //          - Decrement connections to add
    //          - Continue

    int newConnections = (int)(dungeon->GetSize() * percent);
    int randIndex;
    bool createCon = true;
    pd_room_ptr_list neighbors;
    pd_room_ptr room;

    // Loop until all connections are added OR we hit the max number of connections
    while (newConnections > 0 && dungeon->GetNumConnections() < dungeon->GetMaxConnections())
    {
        createCon = true;

        // Pick a random room
        randIndex = PDUtils::generateNumber<int>(0, dungeon->GetSize());
        
        if (!dungeon->IsValidIndex(randIndex))
            continue;

        // Get the neighbors
        room = dungeon->GetRoom(randIndex);
        neighbors = dungeon->GetRoomNeighbors(randIndex);

        // Pick a random neighbor
        randIndex = PDUtils::generateNumber<int>(0, neighbors.size());

        // If the connection exists
        if (dungeon->HasConnection(room, neighbors[randIndex]))
        {
            // If we want to enforce the number of connections
            if (enforce)
            {
                bool found = false;
                // Start by testing all neighbors
                for (unsigned int i = 0; i < neighbors.size(); i++)
                {
                    // Skip the one we already checked
                    if (i == randIndex)
                        continue;

                    // If we find a connection, save the index
                    if (!dungeon->HasConnection(room, neighbors[i]))
                    {
                        found = true;
                        randIndex = i;
                        break;
                    }
                }

                // If for some reason all of them fail, skip to next loop iteration without
                // Decrementing just to make sure
                if (!found)
                    continue;
            }
            else
            {
                createCon = false;
            }
        }

        if (createCon)
        {
            // If the connection doesn't exist, create it
            pd_con_ptr con = CreateConnection(room, neighbors[randIndex]);
            dungeon->AddConnection(con);
        }

        // Decrement our counter
        newConnections--;
    }
}

void PDGenerator::CreateLockedDoors(pd_dung_ptr dungeon, float percent)
{
    typedef std::vector<int>            pd_vec;
    typedef std::vector<pd_con_ptr>     pd_con_ptr_list;

    int newLocks = (int)(dungeon->GetMaxLockedDoors() * percent);
    int randIndex;

    pd_con_ptr_list cons = dungeon->GetConnections();

    // Remove the doors with locks
    for (unsigned int i = 0; i < cons.size(); i++)
    {
        if (cons[i]->IsLocked())
        {
            cons.erase(cons.begin() + i);
            i--;
        }
    }

    // Loop until all connections are added OR we hit the max number of connections
    while (newLocks > 0 && dungeon->GetNumLockedDoors() < dungeon->GetMaxLockedDoors())
    {
        // Pick a random connection
        randIndex = PDUtils::generateNumber<int>(0, cons.size());
        pd_con_ptr con = cons[randIndex];

        // Create the lock
        dungeon->AddLockedDoor(con);

        cons.erase(cons.begin() + randIndex);

        // Decrement our counter
        newLocks--;
    }
}

void PDGenerator::CreateKeys(pd_dung_ptr dungeon)
{
    typedef std::set<pd_room_ptr>    pd_room_ptr_set;
    typedef std::vector<pd_door_ptr> pd_door_ptr_list;
    typedef std::vector<int>         pd_vec;

    pd_room_ptr curRoom = dungeon->GetStartRoom();      // Grab our starting place
    pd_door_ptr_list doors = dungeon->GetLockedDoors(); // Get all the doors we need to get keys for
    pd_door_ptr_list::iterator it = doors.begin();
    pd_room_ptr_list roomPool;
    pd_vec unlockedDoors;
    int randIndex;
    
    for (; it != doors.end(); it++)
    {
        roomPool = dungeon->GetRoomFlood(curRoom, unlockedDoors);
    
        randIndex = PDUtils::generateNumber<int>(0, roomPool.size());
        roomPool[randIndex]->AddKey(CreateKey((*it)->GetID()));
    
        unlockedDoors.push_back((*it)->GetID());
    }
}

PDGenerator::pd_dung_ptr PDGenerator::BuildDungeon(const BuildDungParams& params)
{
    if (params.CustomSeed)
        srand(params.Seed);
    else
        srand((unsigned int)time(NULL));

    pd_dung_ptr dungeon = CreateDungeon(params.Width, params.Height, params.RoomMinWidth, params.RoomWidth, params.RoomMinHeight, params.RoomHeight);
    ConnectDungeon(dungeon, params.AddRedundantConnections);

    if (params.SeedDungeon)
        SeedDungeonConnections(dungeon, params.SeedPercent, params.EnforceSeed);

    CreateLockedDoors(dungeon, params.LockPercent);

    CreateKeys(dungeon);

	return dungeon;
}

int PDGenerator::GetMinRoomSize()
{
    return m_MinRoomSize;
}

int PDGenerator::GetMaxRoomSize()
{
    return m_MaxRoomSize;
}

int PDGenerator::GetMinNumRooms()
{
    return m_MinNumRooms;
}

int PDGenerator::GetMaxNumRooms()
{
    return m_MaxNumRooms;
}

void PDGenerator::SetMinRoomSize(int value)
{
    m_MinRoomSize = value;
}

void PDGenerator::SetMaxRoomSize(int value)
{
    m_MaxRoomSize = value;
}

void PDGenerator::SetMinNumRooms(int value)
{
    m_MinNumRooms = value;
}

void PDGenerator::SetMaxNumRooms(int value)
{
    m_MaxNumRooms = value;
}