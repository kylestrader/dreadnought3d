#include "pdkey.h"

PDKey::PDKey(int id)
{
    m_ID = id;
}

int PDKey::GetID() const
{
    return m_ID;
}

std::string PDKey::ToString() const
{
    std::string str = "{";
    str += "\"name\":\"Key\",";
    str += "\"ID\":\"" + std::to_string(m_ID) + "\"";
    str += "}";

    return str;
}