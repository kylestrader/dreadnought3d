#pragma once

#include <memory>
#include <vector>

class PDRoom;
class PDDungeon;
class PDConnection;
class PDLockedDoor;
class PDKey;

struct BuildDungParams
{
public:
    // Rand Seed
    bool CustomSeed;    // Whether or not to use a custom seed for the random generation
    unsigned int Seed;  // The custom seed to use

    // Dungeon Settings
    int Width;  // The number of rooms wide a dungeon is
    int Height; // The number of rooms high a dungeon is
    bool AddRedundantConnections; // Whether or not to add connections to rooms which have been visited while generating room connections.
                                  // To have a more grid like dungeon, turn this on
                                  // To have a more snake like dungeon, turn this off

    // Room Settings
    bool RoomRange;     // Whether to use room ranges or just hard values (ranges adds some randomness)
    int RoomWidth;      // How many tiles wide a room is. If using range, this is the max
    int RoomHeight;     // How many tiles high a room is. If using range, this is the max
    int RoomMinWidth;   // How many tiles wide a room is. If using range, this is the min
    int RoomMinHeight;  // How many tiles high a room is. If using range, this is the min

    // Connection Seed Settings
    bool SeedDungeon;   // Whether or not to seed the dungeon with extra connections
    float SeedPercent;  // The num of extra connections to add randomly to the dungeon [int num = (int)(dung_size * percent)]
    bool EnforceSeed;   // Whether or not to enforce the number of extra connections being added. To enforce means you are
                        // guaranteed to get the number being added each time, unless the maximum number of connections for a dungeon
                        // have been created. To not enforce means you can have up to the number being added each time (meaning it varies based on RNG).
    
    float LockPercent;  // The num of locked doors to add randomly to the dungeon [int num = (int)(num_connections * percent)]

    BuildDungParams()
    {
        CustomSeed = false;
        Seed = 0;

        Width = -1;
        Height = -1;
        AddRedundantConnections = true;

        RoomRange = false;
        RoomWidth = -1;
        RoomHeight = -1;
        RoomMinWidth = -1;
        RoomMinHeight = -1;

        SeedDungeon = true;
        SeedPercent = 0.1f;
        EnforceSeed = false;

        LockPercent = 0.35f;
    }
};

class PDGenerator
{
public:
    typedef PDGenerator                     this_type;
    typedef std::shared_ptr<PDRoom>         pd_room_ptr;
    typedef std::shared_ptr<PDDungeon>      pd_dung_ptr;
    typedef std::shared_ptr<PDConnection>   pd_con_ptr;
    typedef std::shared_ptr<PDLockedDoor>   pd_door_ptr;
    typedef std::vector<pd_room_ptr>        pd_room_ptr_list;
    typedef std::shared_ptr<PDKey>          pd_key_ptr;
    typedef std::pair<int, int>             pd_point;

private:
    static int m_MinRoomSize;
    static int m_MaxRoomSize;
    static int m_MinNumRooms;
    static int m_MaxNumRooms;

private:
    // Prevent them from making instances of this class since
    // we want them to use our static API 
    PDGenerator();
    ~PDGenerator();

public:
    // Functions for creating a dungeon step by step
    // Width and height are how many tiles wide/high the room is (in 2D space)
    static pd_room_ptr CreateRoom(int width = 0, int height = 0);
    static pd_room_ptr CreateRoom(int minWidth, int width, int minHeight, int height);

    // Width and Height mean how many rooms wide/high is the dungeon, not how wide/high room is
    static pd_dung_ptr CreateDungeon(int width = 0, int height = 0);
    static pd_dung_ptr CreateDungeon(int width, int height, int roomWidth, int roomHeight);
    static pd_dung_ptr CreateDungeon(int width, int height, int minRoomWidth, int roomWidth, int minRoomHeight, int roomHeight);
    static pd_dung_ptr CreateDungeon(int minWidth, int width, int minHeight, int height, int minRoomWidth, int roomWidth, int minRoomHeight, int roomHeight);

    static pd_con_ptr CreateConnection(pd_room_ptr room1, pd_room_ptr room2);

    static pd_key_ptr CreateKey(int id);

    // Connects the rooms in the dungeon
    static void ConnectDungeon(pd_dung_ptr dungeon, bool addRedundantCons = true);

    // Calculates what x% of the dungeon's rooms is and adds this many connections in random places
    // in the dungeon. By default, it will just not add connections if they exist, but you can
    // make the function enforce the number (it will just keep picking new locations untill all of them are placed).
    // Enforcing DOES NOT guarantee all of the connections will be added if the dungeon has hit the max
    // number of connections.
    static void SeedDungeonConnections(pd_dung_ptr dungeon, float percent, bool enforce = false);

    // Calculates what x% of the dungeon's connections is and adds this many locked doors in random
    // places in the dungeon. 
    static void CreateLockedDoors(pd_dung_ptr dungeon, float percent);

    // Calculates how many keys to place in the dungeon and where to place them based off of how many locked
    // doors there are. It will guarantee that your dungeon is always fully solveable.
    static void CreateKeys(pd_dung_ptr dungeon);

    // Build your dungeon automatically using the parameters given
    static pd_dung_ptr BuildDungeon(const BuildDungParams& params);

    // Settings
    static int GetMinRoomSize();
    static int GetMaxRoomSize();
    static int GetMinNumRooms();
    static int GetMaxNumRooms();
    static void SetMinRoomSize(int value);
    static void SetMaxRoomSize(int value);
    static void SetMinNumRooms(int value);
    static void SetMaxNumRooms(int value);
};