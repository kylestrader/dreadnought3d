#include "pdconnection.h"
#include "pdroom.h"
#include "pdlockeddoor.h"

PDConnection::PDConnection(pd_room_ptr room1, pd_room_ptr room2)
{
    m_Room1 = room1;
    m_Room2 = room2;
    m_Door = nullptr;
    m_ID = -1;
}

PDConnection::~PDConnection()
{

}

PDConnection::pd_room_ptr PDConnection::GetRoom1() const
{
    return m_Room1;
}

PDConnection::pd_room_ptr PDConnection::GetRoom2() const
{
    return m_Room2;
}

PDConnection::pd_door_ptr PDConnection::GetLockedDoor() const
{
    return m_Door;
}

int PDConnection::GetID() const
{
    return m_ID;
}

bool PDConnection::IsLocked() const
{
    return m_Door ? true : false;
}

std::string PDConnection::ToString() const
{
    std::string str = "{";
    str += "\"name\":\"Connection " + std::to_string(m_ID) + "\",";

    if (IsLocked())
        str += "\"locked_door\":" + m_Door->ToString() + ",";

    str += "\"rooms\":[";
    str += m_Room1->ToString() + ",";
    str += m_Room2->ToString();
    str += "]}";

    return str;
}

bool operator==(const std::shared_ptr<PDConnection>& lhs, const std::shared_ptr<PDRoom>& rhs)
{
    if (lhs->GetRoom1() == rhs ||
        lhs->GetRoom2() == rhs)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator==(const PDConnection& lhs, const PDConnection& rhs)
{
    if ((lhs.GetRoom1() == rhs.GetRoom1() &&
        lhs.GetRoom2() == rhs.GetRoom2()) ||
        (lhs.GetRoom1() == rhs.GetRoom2() &&
        lhs.GetRoom2() == rhs.GetRoom1()))
    {
        if (lhs.IsLocked() != rhs.IsLocked())
            return false;

        if (lhs.IsLocked() && rhs.IsLocked())
        {
            return lhs.GetLockedDoor() == rhs.GetLockedDoor();
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

bool operator==(const std::shared_ptr<PDConnection>& lhs, const std::shared_ptr<PDConnection>& rhs)
{
    if ((lhs->GetRoom1() == rhs->GetRoom1() &&
        lhs->GetRoom2() == rhs->GetRoom2()) ||
        (lhs->GetRoom1() == rhs->GetRoom2() &&
        lhs->GetRoom2() == rhs->GetRoom1()))
    {
        if (lhs->IsLocked() != rhs->IsLocked())
            return false;

        if (lhs->IsLocked() && rhs->IsLocked())
        {
            return lhs->GetLockedDoor() == rhs->GetLockedDoor();
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}