#pragma once

#include <memory>
#include <string>
class PDRoom;
class PDLockedDoor;
class PDDungeon;

class PDConnection
{
public:
    typedef PDConnection                    this_type;
    typedef std::shared_ptr<PDRoom>         pd_room_ptr;
    typedef std::shared_ptr<PDLockedDoor>   pd_door_ptr;
    typedef std::shared_ptr<PDConnection>   pd_con_ptr;

    friend class PDDungeon;

private:
    pd_room_ptr m_Room1;
    pd_room_ptr m_Room2;
    pd_door_ptr m_Door;
    int m_ID;

public:
    PDConnection(pd_room_ptr room1, pd_room_ptr room2);
    ~PDConnection();

    pd_room_ptr GetRoom1() const;
    pd_room_ptr GetRoom2() const;
    pd_door_ptr GetLockedDoor() const;
    int GetID() const;

    bool IsLocked() const;

    std::string ToString() const;

    friend bool operator==(const std::shared_ptr<PDConnection>& lhs, const std::shared_ptr<PDRoom>& rhs);
    friend bool operator==(const std::shared_ptr<PDConnection>& lhs, const std::shared_ptr<PDConnection>& rhs);
    friend bool operator==(const PDConnection& lhs, const PDConnection& rhs);
};