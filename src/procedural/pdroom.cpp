#include "pdroom.h"
#include <string>
#include <cassert>
#include "pdkey.h"

PDRoom::PDRoom(int width, int height)
{
    m_IsStart = false;
    createGrid(width, height);
}

PDRoom::~PDRoom()
{

}

void PDRoom::createGrid(int width, int height)
{
    m_Width = width;
    m_Height = height;

    m_Grid.clear();

    for (int i = 0; i < m_Width; i++)
    {
        pd_vec col;

        for (int j = 0; j < m_Height; j++)
        {
            col.push_back(0);
        }

        m_Grid.push_back(col);
    }
}

int PDRoom::GetGridSize() const
{
    int size = 0;

    for (unsigned int i = 0; i < m_Grid.size(); i++)
    {
        size += m_Grid[i].size();
    }

    return size;
}

int PDRoom::GetWidth() const
{
    return m_Width;
}

int PDRoom::GetHeight() const
{
    return m_Height;
}

float PDRoom::GetX() const
{
    return m_X;
}

float PDRoom::GetY() const
{
    return m_Y;
}

int PDRoom::GetGridX() const
{
    return m_GridX;
}

int PDRoom::GetGridY() const
{
    return m_GridY;
}

int PDRoom::GetIndex() const
{
    return m_Index;
}

std::string PDRoom::GetTag() const
{
    return std::to_string(m_GridX) + "," + std::to_string(m_GridY);
}

const PDRoom::pd_key_ptr_list& PDRoom::GetKeys() const
{
    return m_Keys;
}

int PDRoom::GetNumKeys() const
{
    return m_Keys.size();
}

PDRoom::pd_key_ptr PDRoom::GetKey(int ID) const
{
    for (unsigned int i = 0; i < m_Keys.size(); i++)
    {
        if (m_Keys[i]->GetID() == ID)
            return m_Keys[i];
    }

    return nullptr;
}

PDRoom::pd_key_ptr PDRoom::GetKeyAt(int index) const
{
    assert(index >= 0 && index < (int)m_Keys.size());
    return m_Keys[index];
}

bool PDRoom::GetIsStart() const
{
    return m_IsStart;
}

bool PDRoom::HasKey(int ID) const
{
    for (unsigned int i = 0; i < m_Keys.size(); i++)
    {
        if (m_Keys[i]->GetID() == ID)
            return true;
    }

    return false;
}

PDRoom::this_type& PDRoom::SetX(float value)
{
    m_X = value;
    return *this;
}

PDRoom::this_type& PDRoom::SetY(float value)
{
    m_Y = value;
    return *this;
}

PDRoom::this_type& PDRoom::SetGridX(int value)
{
    m_GridX = value;
    return *this;
}

PDRoom::this_type& PDRoom::SetGridY(int value)
{
    m_GridY = value;
    return *this;
}

PDRoom::this_type& PDRoom::SetIndex(int value)
{
    m_Index = value;
    return *this;
}

PDRoom::this_type& PDRoom::SetIsStart(bool value)
{
    m_IsStart = value;
    return *this;
}

std::string PDRoom::ToString() const
{
    std::string str = "{";
    str += "\"name\":\"Room " + std::to_string(m_Index) + "\",";
    if (m_IsStart) str += "\"is_start\":\"true\",";
    str += "\"x\":\"" + std::to_string(m_X) + "\",";
    str += "\"y\":\"" + std::to_string(m_Y) + "\",";
    str += "\"width\":\"" + std::to_string(m_Width) + "\",";
    str += "\"height\":\"" + std::to_string(m_Height) + "\",";
    str += "\"gridX\":\"" + std::to_string(m_GridX) + "\",";
    str += "\"gridY\":\"" + std::to_string(m_GridY) + "\"";

    if (m_Keys.size() > 0)
    {
        str += ", \"keys\":[";

        for (unsigned int i = 0; i < m_Keys.size(); i++)
        {
            str += m_Keys[i]->ToString();

            if (i + 1 < m_Keys.size())
                str += ",";
        }
        str += "]";
    }
    str +="}";

    return str;
}

void PDRoom::AddKey(pd_key_ptr key)
{
    m_Keys.push_back(key);
}

void PDRoom::AddKeys(pd_key_ptr_list keys)
{
    for (unsigned int i = 0; i < keys.size(); i++)
    {
        AddKey(keys[i]);
    }
}

bool operator==(const PDRoom& lhs, const PDRoom& rhs)
{
    if (lhs.GetGridX() == rhs.GetGridX() &&
        lhs.GetGridY() == rhs.GetGridY())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator==(const std::shared_ptr<PDRoom>& lhs, const std::shared_ptr<PDRoom>& rhs)
{
    if (lhs->GetGridX() == rhs->GetGridX() &&
        lhs->GetGridY() == rhs->GetGridY())
    {
        return true;
    }
    else
    {
        return false;
    }
}