#pragma once

#include <string>
#include <memory>

class PDLockedDoor
{
public:
    typedef PDLockedDoor    this_type;

private:
    int m_ID;

public:
    PDLockedDoor(int id);

    int GetID() const;
    std::string ToString() const;

    friend bool operator==(const std::shared_ptr<PDLockedDoor>& lhs, const std::shared_ptr<PDLockedDoor>& rhs);
    friend bool operator==(const PDLockedDoor& lhs, const PDLockedDoor& rhs);
};