#pragma once

namespace PDUtils
{
    #define PI 3.14159265358979323846

    /**
    * Converts a 2D position to a 1D position
    * @param    x The x position.
    * @param    y The y position.
    * @param    rowLength How long each row is. (AKA Height)
    * @return   The 1D position.
    */
    static int to1DPosition(int x, int y, int rowLength)
    {
        return (y * rowLength) + x;
    }

    /**
    * Converts a 1D position to a 2D position
    * @param    index The 1D position.
    * @param    rowLength How long each row is. (AKA Height)
    * @param    x The x position.
    * @param    y The y position.
    * @return   Returns the 2D position by reference.
    */
    static void to2DPosition(int index, int rowLength, int& x, int& y)
    {
        x = index % rowLength;
        y = (index - x) / rowLength;
    }

    /**
    * Calculates the length of the vector you pass.
    * @param    x The x component.
    * @param    y The y component.
    * @return   The length of the vector.
    */
    template <class T>
    static T length(T x, T y)
    {
        return sqrt((x * x) + (y * y));
    }

    /**
    * Normalizes the vector you pass.
    * @param    x The x component.
    * @param    y The y component.
    * @param    outX The x component after normalization.
    * @param    outY The y component after normalization.
    * @return   Results are returned by reference in outX and outY.
    */
    template <class T>
    static void normalize(T x, T y, T& outX, T& outY)
    {
        T len = length(x, y);
        if (len != 0)
        {
            outX = x / len;
            outY = y / len;
        }
        else
        {
            outX = x;
            outY = y;
        }
    }

    /**
    * Normalizes the vector you pass. This is a shorthand version where the variables get directly modified.
    * @param    x The x component.
    * @param    y The y component.
    * @return   Results are returned by reference in x and y.
    */
    template <class T>
    static void normalize(T& x, T& y)
    {
        T len = length(x, y);
        if (len != 0)
        {
            x = x / len;
            y = y / len;
        }
    }

    /**
    * Generates a random binomial of type T and returns it. (i.e. [-1, 1])
    * @return  Returns the random binomial of type T.
    */
    template <class T>
    static T generateRandomBinomial()
    {
        return generateRandomNumber<T>() - generateRandomNumber<T>();
    }

    /**
    * Generates a random number of type T and returns it.
    * @return  Returns the random number of type T.
    */
    template <class T>
    static T generateRandomNumber()
    {
        T r = (T)rand() / (T)RAND_MAX;
        return r;
    }

    /**
    * Generates a random number of type T between the min (Inclusive) and max (Non-Inclusive) provided.
    * @param    min The minimum value you want. (Inclusive)
    * @param    max The max value you want. (Non-Inclusive)
    * @return  Returns the random number of type T.
    */
    template <class T>
    static T generateNumber(T min, T max)
    {
        return (T)(min + (rand() % (int)max));
    }

    /**
    * Clamps the value of type T to the minimum and maximum values given.
    * @param    value The number you want to bind.
    * @param    min The minimum value that your number can be.
    * @param    max The maximum value that your number can be.
    * @return   The number you passed, but bound to the min and max provided.
    */
    template <class T>
    static T clamp(T value, T min, T max)
    {
        return (value < min ? min : (value > max ? max : value));
    }

    /**
    * This will linearly interpolate between two values of type T with a given time [0-1].
    * @param   start The value to start at with a time of 0.
    * @param   end The value to end at with a time of 1.
    * @param   time The time between 0 and 1.
    * @return  Returns the interpolated value of type T.
    */
    template<class T>
    static T lerp(T start, T end, double time)
    {
        return (T)((1 - time) * start + time * end);
    }

    /**
    * This will map the value of type T from its original set to its new provided set and return it.
    * Example: 5 mapped from [1-10] to [1-100] is 50
    * @param   value The value you want to map to a new set.
    * @param   istart The left bound of the set (inclusive).
    * @param   istart The right bound of the set (inclusive).
    * @param   ostart The left bound of the new set (inclusive).
    * @param   ostop The right bound of the new set (inclusive).
    * @return  Returns the value mapped to the new set.
    */
    template<class T>
    static T map(T value, T istart, T istop, T ostart, T ostop)
    {
        return (T)(ostart + (ostop - ostart) * ((value - istart) / (istop - istart)));
    }

    /**
    * This will calculate the distance between two points of type T and return it.
    * @param   startX The x component of the first point.
    * @param   startY The y component of the first point.
    * @param   endX The x component of the second point.
    * @param   endY The y component of the second point.
    * @return  Returns the distance in the type given.
    */
    template<class T>
    static T distance(T startX, T startY, T endX, T endY)
    {
        return (T)sqrt(((endX - startX) * (endX - startX)) + ((endY - startY) * (endY - startY)));
    }

    /**
    * Converts degrees to radians in floats.
    * @param   degrees The degrees to convert.
    * @return  Returns the degrees in radians.
    */
    static float toRadiansf(float degrees)
    {
        return degrees * (float)(PI / 180.0);
    }

    /**
    * Converts degrees to radians in doubles.
    * @param   degrees The degrees to convert.
    * @return  Returns the degrees in radians.
    */
    static double toRadians(double degrees)
    {
        return degrees * (PI / 180.0);
    }

    /**
    * Converts radians to degrees in floats.
    * @param   radians The radians to convert.
    * @return  Returns the radians in degrees.
    */
    static float toDegreesf(float radians)
    {
        return radians * (float)(180.0 / PI);
    }

    /**
    * Converts radians to degrees in doubles.
    * @param   radians The radians to convert.
    * @return  Returns the radians in degrees.
    */
    static double toDegrees(double radians)
    {
        return radians * (180.0 / PI);
    }
}