#include "pdlockeddoor.h"

PDLockedDoor::PDLockedDoor(int id)
{
    m_ID = id;
}

int PDLockedDoor::GetID() const
{
    return m_ID;
}

std::string PDLockedDoor::ToString() const
{
    std::string str = "{";
    str += "\"name\":\"Locked Door\",";
    str += "\"ID\":\"" + std::to_string(m_ID) + "\"";
    str += "}";

    return str;
}

bool operator==(const PDLockedDoor& lhs, const PDLockedDoor& rhs)
{
    if (lhs.GetID() == rhs.GetID())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator==(const std::shared_ptr<PDLockedDoor>& lhs, const std::shared_ptr<PDLockedDoor>& rhs)
{
    if (lhs->GetID() == rhs->GetID())
    {
        return true;
    }
    else
    {
        return false;
    }
}