#pragma once

#include <string>

class PDKey
{
public:
    typedef PDKey   this_type;

private:
    int m_ID;

public:
    PDKey(int id);

    int GetID() const;
    std::string ToString() const;
};