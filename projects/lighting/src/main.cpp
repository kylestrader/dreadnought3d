//Powered by Dreadnought3D && PDGenerator
#include <Dreadnought3D\D3D_CoreEngine.h>
#include <Dreadnought3D\D3D_RenderingEngine.h>
#include <Dreadnought3D\D3D_GameApp.h>
#include <Dreadnought3D\D3D_Time.h>
#include <Dreadnought3D\D3D_Utils.h>
#include <Dreadnought3D\D3D_Window.h>
#include <Dreadnought3D\D3D_Mesh.h>
#include <Dreadnought3D\D3D_Vector.h>
#include <Dreadnought3D\shader_types\D3D_Shader.h>
#include <Dreadnought3D\D3D_Vector3.h>
#include <Dreadnought3D\D3D_Vector4.h>
#include <Dreadnought3D\D3D_Matrix4.h>
#include <Dreadnought3D\D3D_Transform.h>
#include <Dreadnought3D\D3D_Texture.h>
#include <Dreadnought3D\D3D_Input.h>


#define Clampf(value, low, high) D3D_Utils::Math::Clamp<float>((float)value, (float)low, (float)high)
#define Deg2Rad(value) D3D_Utils::Math::Deg2Rad(value)
#define Rad2Deg(value) D3D_Utils::Math::Rad2Deg(value)

class MyGameApp : public D3D_GameApp
{
public:

	typedef D3D_Types::Vector3<float> Vector3f;
	typedef D3D_Types::Vector4<float> Vector4f;

	MyGameApp() {};
	~MyGameApp() {};

	void Initialize() override
	{
		Vector3f val(5.f, 6.5, 7.5);
		float scalar = 2.f;
		D3D_Time::Timer timer;

		Vector3f _v1 = Vector3f(3.5f, 4.5f, 6.5f);
		Vector3f _v2 = Vector3f(1.0f, 0.0f, 0.0f);
		float total = 0.f;
		timer.Start();
		double time = timer.GetElapsedTime_ns();
		timer.Stop();
		printf("Time: %f\n", time);

		temp = 0.0f;

		camera = std::make_shared<D3D_Camera>();

		{
			std::shared_ptr<D3D_MeshTypes::Mesh> mesh = std::make_shared<D3D_MeshTypes::Mesh>("cube.obj");
			std::shared_ptr<D3D_TextureTypes::Texture> texture = std::make_shared<D3D_TextureTypes::Texture>("bricks2.jpg");
			std::shared_ptr<D3D_Transform> transform = std::make_shared<D3D_Transform>();
			transform->SetTranslation(Vector3f(0, 0, 3));
			transform->SetEulerRotation(Vector3f(20.f, 180.f, 0.f));
			transform->SetProjection(0.1f, 1000.f, 1280, 720, Deg2Rad(90.f));
			transform->SetCamera(camera);

			{
				typedef D3D_Types::Shader Shader;

				std::shared_ptr<Shader> shader = std::make_shared<Shader>();
				m_Shaders.push_back(shader);
			}

			m_Meshes.push_back(mesh);
			m_Textures.push_back(texture);
			m_Transforms.push_back(transform);
		}
	}

	void Update() override
	{
		float movAmt = (float)(10 * D3D_Time::Helper::DELTA_T);
		float rotAmt = (float)(100 * D3D_Time::Helper::DELTA_T);

		if (D3D_Input::GetKey(D3D_Input::KEY_W))
			camera->Move(camera->GetForward(), movAmt);
		if (D3D_Input::GetKey(D3D_Input::KEY_S))
			camera->Move(camera->GetForward(), -movAmt);
		if (D3D_Input::GetKey(D3D_Input::KEY_A))
			camera->Move(camera->GetLeft(), movAmt);
		if (D3D_Input::GetKey(D3D_Input::KEY_D))
			camera->Move(camera->GetRight(), movAmt);

		if (D3D_Input::GetKey(D3D_Input::KEY_UP))
			camera->RotateX(Deg2Rad(-rotAmt));
		if (D3D_Input::GetKey(D3D_Input::KEY_DOWN))
			camera->RotateX(Deg2Rad(rotAmt));
		if (D3D_Input::GetKey(D3D_Input::KEY_LEFT))
			camera->RotateY(Deg2Rad(-rotAmt));
		if (D3D_Input::GetKey(D3D_Input::KEY_RIGHT))
			camera->RotateY(Deg2Rad(rotAmt));

		D3D_GameApp::Update();
		temp += D3D_Time::Helper::DELTA_T;
		float sinTemp = (float)sin(temp / 3);

		m_Transforms.at(0)->SetTranslation(Vector3f(sinTemp * 3, 0, 5));
		m_Transforms.at(0)->SetEulerRotation(Vector3f(sinTemp * 180, sinTemp * 180, 0));
		m_Transforms.at(0)->SetScale(Vector3f(1, 1, 1));
	}

	void Render() override
	{
		D3D_GameApp::Render();

		for (int i = 0; i < m_Meshes.size(); i++)
		{
			m_Shaders.at(i)->Bind();
			D3D_Types::Matrix4<float> mat = m_Transforms.at(i)->GetProjectedTransformation();
			m_Shaders.at(i)->SetUniform("transform", mat);
			float r_sinTemp = (float)sin(temp*2);
			float g_sinTemp = (float)sin(temp*3);
			float b_sinTemp = (float)sin(temp);
			m_Shaders.at(i)->SetUniform("color", Vector4f(r_sinTemp, g_sinTemp, b_sinTemp, 1));
			m_Textures.at(i)->Bind();
			m_Meshes.at(i)->Draw();
		}
	}

	void Cleanup() override
	{
		D3D_GameApp::Cleanup();
	}

	double temp;

	std::vector<std::shared_ptr<D3D_MeshTypes::Mesh>> m_Meshes;
	std::vector<std::shared_ptr<D3D_TextureTypes::Texture>> m_Textures;
	std::vector<std::shared_ptr<D3D_Types::Shader>> m_Shaders;
	std::vector<std::shared_ptr<D3D_Transform>> m_Transforms;

	std::shared_ptr<D3D_Camera> camera;

};

#undef main
int main()
{
	// Create a rendering engine instance and instantiate it
	std::shared_ptr<D3D_RenderingEngine> renderer = std::make_shared<D3D_RenderingEngine>(); // Create an instance of the engine

	renderer->CreateWindow(1280, 720, "My Dreadnought Engine", D3D_WindowFlags::D3D_WINDOW_OPENGL);

	renderer->InitGraphics(D3D_Types::Vector4<float>(
		0.0, 
		0.0, 
		0.0, 
		0.0
	));

	// Create an instance of your custom GameApp class and initialize it
	std::shared_ptr<MyGameApp> game = std::make_shared<MyGameApp>(); //Create game class instance

																	 // Create an instance of the core engine and instantiate it 
	std::shared_ptr<D3D_CoreEngine> core_engine = std::make_shared<D3D_CoreEngine>();
	core_engine->Initialize(game, renderer);

	game->Initialize();

	core_engine->Start(); //Start up the engine
	core_engine->Quit(); //Stop the engine

	return 0;
};