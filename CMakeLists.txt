# Dreadnought3D - Kyle Strader

cmake_minimum_required(VERSION 2.6)

#--------------------------------------------------------------------
# PROJECT DEFINES

# Project name
project(Dreadnought3D)

# Check for undefined cmake build type
if(CMAKE_BUILD_TYPE STREQUAL "")
  set(CMAKE_BUILD_TYPE Debug)
endif()

#--------------------------------------------------------------------
# INSTALL FUNCTIONS

function(install_library_source target_name target_include_directory)
	file(WRITE ${CMAKE_BINARY_DIR}/${target_name}Copy.cmake
	    "file(COPY \"${CMAKE_SOURCE_DIR}/${target_include_directory}\" 
	    DESTINATION \"${CMAKE_SOURCE_DIR}/INSTALL/include/\" 
	    FILE_PERMISSIONS OWNER_READ
	    FILES_MATCHING PATTERN \"*.h\" PATTERN \"*.cpp\" PATTERN \"*.c\" PATTERN \"*.inl\"
	    )")
	  
	add_custom_command(TARGET ${target_name} PRE_BUILD
		COMMAND ${CMAKE_COMMAND} -P "${CMAKE_BINARY_DIR}/${target_name}Copy.cmake")
endfunction()

#--------------------------------------------------------------------
# SET BINARY BUILD DIRECTORIES

foreach (OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES})
    string (TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/INSTALL/lib/${OUTPUTCONFIG})
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/INSTALL/lib/${OUTPUTCONFIG})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_SOURCE_DIR}/INSTALL/lib/${OUTPUTCONFIG})
	
endforeach (OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES})

message(STATUS "CMAKE IS NOW RESPONDING")
message(STATUS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY_Debug})

#--------------------------------------------------------------------
# INCLUDE DIRECTORIES

# OpenGL
find_package(OpenGL REQUIRED)

# Define the include DIRs
include_directories(
	${CMAKE_SOURCE_DIR}/src
	${CMAKE_SOURCE_DIR}/lib
)

#--------------------------------------------------------------------
# INCLUDE SUBDIRECTORIES

add_subdirectory(src)